function testCSSP_BBCI_III_1
	% Training : 278 trials, 64 channels and 3000 samples in 3 seconds
	% Testing  : 
	frqs=1000;
	C=64;
	T=3000;
	trainingTrials = 278;
	testTrials = 100;
	winoptions.type=1;
	winoptions.alpha=0.5;
	filterLength = 112;
	% Down-sampling the signal to 100 Hz
	hlow = wsilowpass(100*2*pi/frqs, filterLength, winoptions);
	
	% Windowing and band-pass filtering will be done on the down sapled signal
	T = T/10;
	frqs = frqs/10;
	window = hamming(T);
	bandPass = wsibandpass(6*2*pi/frqs, 34*2*pi/frqs, filterLength, winoptions);
	
	% Will load X and Y for training
  disp('Loading training data...');
  fflush(stdout);
	load '../datasets/bci/bbci.de/BCI-comp-III/1/competition_train.mat'
	
  disp('Preprocessing training data...');
  fflush(stdout);
	Xtrain = preprocess(X, hlow, 10, window, bandPass);
  clear X
	% Converts -1,1 to 1,2
	Ytrain = (3+Y)/2;
	
	% Will load X for testing
  disp('Loading testing data...');
  fflush(stdout);
	load '../datasets/bci/bbci.de/BCI-comp-III/1/competition_test.mat'
  disp('Preprocessing testing data...');
  fflush(stdout);
	Xtest = preprocess(X, hlow, 10, window, bandPass);
	clear X
	% True labels
  disp('Loading true labels...');
  fflush(stdout);
	[fid,message] = fopen('../datasets/bci/bbci.de/BCI-comp-III/1/true_labels.txt','r');
	if fid==-1
		error(message)
	end
	[trueLabels,c,message]=fscanf(fid,'%d',[size(Y,1),1]);
	fclose(fid);
	if length(message)>0
		disp (message)
	end
	% Converts -1,1 to 1,2
	trueLabels = (3+trueLabels)/2;
  
	simulation(Xtrain, Ytrain, Xtest, trueLabels)
	
end

function demoRun(Xtrain, Ytrain, Xtest, trueLabels)
  C=64;
	T=300; % After downSampling
	trainingTrials = 278;
	testTrials = 100;
  moment = cputime;
  CCModel.TYPE = 'FLDA';
  options.verbose=true;
  options.lastTau = 50;
	cssp = trainCSSP(C,T,2,@(idx) mytrialGenerator(idx, Xtrain, Ytrain), 
    trainingTrials, '', CCModel, 3, options);
	s2 = getCSSPSecondMoments(cssp.tau, C, T, [1:trainingTrials], 
    @(idx) mytrialGenerator(idx, Xtrain, Ytrain));
  D = generatePatternsFromCSP(cssp, 
    @(index) simpleTrialGenerator(index, [1:trainingTrials], s2, []),
    trainingTrials, 'training');
  clear Xtrain
  CC = train_sc(D,Ytrain,CCModel);
 	clear Ytrain
	R = testCSSP(cssp, @(idx) mytrialGenerator(idx, Xtest, nan), testTrials,
   CC, CCModel, [], trueLabels);
	disp(sprintf('Accuracy=%5.2f%%, time=%fs',100*R.ACC, cputime-moment))  
end

function simulation(Xtrain, Ytrain, Xtest, trueLabels)
  C=64;
	T=300; % After downSampling
	trainingTrials = 278;
	testTrials = 100;
  CCModel.TYPE = 'FLDA';
  allGamma=0.05:0.01:0.1;
  options.lastTau = 75;
  iterations=1;
  results=cell(1,3*numel(allGamma));
  CCModel.TYPE = 'FLDA';
  counter=0;
  for numSelectedFilters = 2:4
    accPerGamma=zeros(size(allGamma));
    for iterator = 1:iterations
      disp(sprintf('Iteration %d...',iterator));
      fflush(stdout);
      CCModel.hyperparameter.gamma=0.08;
      cssp = trainCSSP(C,T,2,@(idx) mytrialGenerator(idx, Xtrain, Ytrain), 
        trainingTrials, '', CCModel, numSelectedFilters, options);
      s2 = getCSSPSecondMoments(cssp.tau, C, T, [1:trainingTrials], 
        @(idx) mytrialGenerator(idx, Xtrain, Ytrain));  
      D = generatePatternsFromCSP(cssp, 
        @(index) simpleTrialGenerator(index, [1:trainingTrials], s2, []),
        trainingTrials, 'training');
      for t=1:numel(allGamma)
        CCModel.hyperparameter.gamma=allGamma(t);
        CC = train_sc(D,Ytrain,CCModel);
        R = testCSSP(cssp, @(idx) mytrialGenerator(idx, Xtest, nan), testTrials,
          CC, CCModel, [], trueLabels);
        accPerGamma(t)=accPerGamma(t)+R.ACC;
        disp(sprintf('Gamma=%f, acc=%5.2f%%',allGamma(t),R.ACC*100));
        %disp(accPerGamma);
        fflush(stdout);
      end   % for t
    end % iterations
    for t=1:numel(allGamma)
      r.gamma=allGamma(t);
      r.acc = accPerGamma(t)/iterations;
      r.numSelectedFilters = numSelectedFilters;
      counter=counter+1;
      results{counter}=r;
    end
  end % for numSelectedFilters
  disp(' ');
  disp('Filters     Gamma     Acc%');
  for counter=1:numel(results)
    r = results{counter};
    disp(sprintf(' %d      %f      %5.2f%%',r.numSelectedFilters, r.gamma,
      100*r.acc));
  end
  horn(5);
end

function xpp = preprocess(X, lowPass, downSamplingRate, window, bandPass)
	% X : A three dimensional matrix with L rows, C columns and T planes
	% lowPass : A vector of factors of a low pass filter
	% downSamplingRate : scaler
	% window : A vector with T/10 elements
	% bandPass : A vector of factors of a band pass filter
	% down sampling
	T = size(X,3);
	X = filter(lowPass, 1, X, [], 3);
	X = X(:,:,downSamplingRate/2:downSamplingRate:T);
	% Windowing
	X = X.*repmat(reshape(window, 1, 1, numel(window)), size(X,1), size(X,2));
	% band pass filtering
	xpp = filter(bandPass, 1, X, [], 3);
end


function [trial,label] = mytrialGenerator(idx,x,y)
trial = squeeze(x(idx,:,:));
if ~isnan(y)
	label = y(idx);
else
  label=NaN;
end
end