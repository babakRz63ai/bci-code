function [r,c,ix] = getRCIXforSimDiag(COV,alpha)
  % [r,c] = getRCforSimDiag(COV,alpha=1)
  N = size(COV,1);
  numclasses = size(COV,3);
  lamda=zeros(1,N*numclasses);
  for k=1:numclasses
    lambda((k-1)*N+1:k*N) = diag(COV(:,:,k));
  end
  
  if nargin<2
    alpha=1;
  end  
  
  % Map them
  lambda = max(lambda, alpha*(1-lambda)./(1-lambda+(numclasses-1)^2*lambda));
  [lambda, ix] = sort(lambda,'descend');
  % c is idex of each class, r is index of each eigenvalue of each class
  [r,c] = ind2sub([N,numclasses], ix);
end