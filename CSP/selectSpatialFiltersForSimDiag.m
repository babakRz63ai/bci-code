function W = selectSpatialFiltersForSimDiag(r,c,ix,N,nclasses,selectedFilters,V,verbose)
% W = selectSpatialFiltersForSimDiag(COV,N,nclasses,selectedFilters,V ,verbose=false)
% 
% r,c,ix : What getRCIXforSimDiag returns
% N : Number of channels or initial filters
% nclasses : number of classes
% selectedFilters : Desired number of selected filterts for each class
% V : Matrix of size N by N containing all initial filters
%
% Returns : An N by selectedFilters by nclasses matrix
  
  if nargin<8
    verbose = false;
  end
  
  W = zeros(N, selectedFilters, nclasses);
  selectedFiltersPerClass = zeros(1,nclasses);
  selectedFiltersFlag = false(1,N);
  k=1;
  do
    % If this class needs more filters and this filter (r(k)) has not been selected
    if selectedFiltersPerClass(c(k))<selectedFilters && ~selectedFiltersFlag(r(k))
      selectedFiltersPerClass(c(k)) = selectedFiltersPerClass(c(k))+1;
      selectedFiltersFlag(r(k)) = true;
      W(:, selectedFiltersPerClass(c(k)), c(k)) = V(:,r(k));
      if verbose
        disp(sprintf('Selects filter %d for class %d', r(k), c(k)));
        fflush(stdout);
      end
    end
    k=k+1;
  until(k>numel(ix) || all(selectedFiltersPerClass==selectedFilters));
  if any(selectedFiltersPerClass<selectedFilters)
    error 'Input signal doesn''t have enough channels'
  end
end
