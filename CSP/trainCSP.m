function cspinst = trainCSP(signal2, labels, model, numSelected, verbose)
% csp = trainCSP(signal2, labels, model='OVR', numSelected=3, verbose=false)
%
% Executes all steps for training a CSP from trials
%
% signal2 : A 3D array with one square matrix for each trial as its pages
% labels : Vector of training class labels
% model : Model of extension for multiclass case. Should be OVR, OVR.CSP0, OVR.CSP3
% or SIM
% numSelected : Number of selected spatial filters from each end of spectrum of
%               eigen values
% verbose     : Verbosity. A true value causes functions to print log messages

if nargin<2
	error "Insufficient arguments given"
end

if nargin<3
  model = 'OVR.CSP3';
elseif ~ischar(model)
  error 'Extension model must be a string'
end

if nargin<4
  numSelected = 3;
elseif numel(numSelected)~=1
  error 'numSelected must be a scalar'
elseif numSelected<=0
  error 'Invalid number of selected spatial filterts'
end

if nargin<5
	verbose=false;
end

nclasses = numel(unique(labels));

if nclasses<2
	error "Invalid number of classes. It should be at least 2"
end

if verbose
  disp(sprintf('trainCSP got nclasses=%d',nclasses))
end

C = size(signal2,1);

if nclasses==2
	cspinst = csp2(C);
else
	cspinst = cspn(C,nclasses,model);
end

% May be better to select an even number of filters. For example when
% C=5, two filters from each end of the spectrum can be selected.
cspinst.selectedFilters = min(2*fix(C/2), 2*numSelected);

%tic
cspinst = CSPaddTrial(cspinst, signal2, labels, verbose);
%toc

cspinst = solveCSP(cspinst,verbose);

end