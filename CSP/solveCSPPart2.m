function outcsp = solveCSPPart2(csp,verbose)
% outcsp = solveCSPPart2(csp)
% Solves a CSP problem by finding six eigenvectors coressponding to three eigenvalues 
% from each end of the spectrum of the eigenvalues
outcsp = csp;

% Average of each class
d = size(csp.patterns,1);
m = zeros(d, csp.nclasses);
% estimates of covariance matrices of each class
nsigma = cell(1,csp.nclasses);
for t=1:csp.nclasses
	m(:,t) = mean(csp.patterns(:,csp.classLabel==t),2);
	nsigma{t} = zeros(d,d);
	I = find(csp.classLabel==t);
	for s=1:csp.n(t)
		xfc = csp.patterns(:, I(s)) - m(:,t);
		nsigma{t} = nsigma{t} + xfc*xfc';
	end
end

if csp.nclasses==2
	SW = (nsigma{1}+nsigma{2})/(sum(csp.n)-2);
	SW_1 = inv(SW);
	outcsp.beta = SW_1*(m(:,1)-m(:,2));
	if isequal(csp.distAssumption,'normal')
		% Tests if covariance matrices are equal
		absdiff = abs(nsigma{1}-nsigma{2});
		if sum(absdiff(:))>1e-6
			warning(sprintf('Normal assumption with inequal ovariance matrices is not optimal (sum of differences is %f)\n',absdiff))
		end
		pw1 = csp.n(1); %/sum(csp.n);
		pw2 = csp.n(2); %/sum(csp.n);
		outcsp.beta0 = -0.5*(m(:,1)+m(:,2))'*outcsp.beta-log(pw2/pw1);
	else
		% find beta0 by optimization on all training data
		outcsp.beta0 = findBeta0ByMinimizingError2(outcsp.beta, outcsp.patterns, csp.classLabel, verbose);
	end
	
else 
	% SW is useful for all cases with several classes
	SW = zeros(d,d);
	for t=1:csp.nclasses
		SW = SW + nsigma{t};
	end
	SW = SW/sum(csp.n);
	SW_1 = inv(SW);
	
	if isequal(csp.extensionModel, 'OVR')
		% Performs two-class LDA for each class versus rest of classes
		% Mean of rest of classes for each class
    if verbose
      disp 'Calculating m_rest...'
      fflush(stdout);
    end
		mrest = zeros(d, csp.nclasses);
		for c=1:csp.nclasses
			mrest(:,c) = mean(csp.patterns(:,csp.classLabel~=c),2);
		end
    
    if verbose
      disp 'Calculating beta...'
      fflush(stdout);
    end
    
		outcsp.beta = zeros(csp.nclasses, csp.selectedFilters);
		for c=1:csp.nclasses
			outcsp.beta(c,:) = (SW_1*(m(:,c) - mrest(:,c)))';
		end
    
		if verbose
        disp 'Calculating beta0...'
        fflush(stdout);
    end
		outcsp.beta0 = zeros(csp.nclasses, 1);	
		if isequal(csp.distAssumption,'normal')
			% Equal covariance matrices?
			for c=2:csp.nclasses
				absdiff = abs(nsigma{1}-nsigma{c});
				if sum(absdiff(:))>1e-3
					warning(sprintf('Normal assumption with inequal ovariance matrices is not optimal (sum of differences is %f)',sum(absdiff(:))))
					break
				end
			end
      
			% a priori probablity of each class
			pw = csp.n; %/sum(csp.n);
			for c=1:csp.nclasses
				% a priori probablity of rest of classes
				pwrest = sum(csp.n([1:c-1, c+1:csp.nclasses])); %/sum(csp.n);
				outcsp.beta0(c) = -0.5*outcsp.beta(c,:)*(m(:,c)+mrest(:,c))-log(pwrest/pw(c));
			end
		else
			%find elements of beta0 by optimization on training data of each class
			for c=1:csp.nclasses
				tempLabels = zeros(size(outcsp.classLabel));
				tempLabels(outcsp.classLabel==c) = 1;
				tempLabels(outcsp.classLabel~=c) = 2;
				outcsp.beta0(c) = findBeta0ByMinimizingError2(outcsp.beta(c,:)',
					outcsp.patterns, tempLabels, verbose);
			end
      if verbose
        disp 'Values of Beta0 are :'
        disp(outcsp.beta0);
        fflush(stdout);
      end
		end
	else
		error 'Other extension models than OVR are not implemented'
	end
end

end

function beta0 = findBeta0ByMinimizingError2(beta, patterns, labels, verbose)
	% beta : A column vector by D elements
	% patterns : A matrix with D rows and N columns
	% labels : A vector with N elements 1 or 2
	% Returns: A scalar
	labels = labels(:)';
	labels(labels==2) = -1;
  % Cost of misclassification
  cost=ones(size(labels));
  cost(labels==1) = sum(labels==-1)/sum(labels==1);
  bp = beta'*patterns;
  if verbose
    %disp 'Finds optimal beta0 for beta*patterns:'
    %disp(bp);
    %disp 'and labels:'
    %disp(labels)
    figure
    subplot(2,1,1)
    plot(bp(labels==1), zeros(1,sum(labels==1)), 'bo')
    hold on
    plot(bp(labels==-1), zeros(1,sum(labels==-1)), 'r+')
  end
  % beta0 is added to bp so its median falls on the zero
	beta0 = fminbnd(@(beta0) countOfMisclassied2(beta0, bp, labels, cost), -max(bp), -min(bp));
  
  if verbose
	  allB0Values = linspace(beta0-5, beta0+5,100); % generates 100 points
	  counts = zeros(size(allB0Values));
	  for k = 1:numel(allB0Values)
		  counts(k) = countOfMisclassied2(allB0Values(k), bp, labels, cost);
	  end
	  subplot(2,1,2)
    hold on
	  plot(allB0Values, counts, 'r');
	  xlabel('beta_0')
	  ylabel('Errors count');
    title(sprintf('Optimum beta_0 = %f',beta0));
    %disp(sprintf('Optimum beta_0 = %f',beta0));
    %disp([allB0Values', counts']);
    line([beta0-5, beta0],[counts(51),counts(51)],'linestyle','--')
	end
end

function errCount = countOfMisclassied2(beta0, bp, labels, cost)
	% beta0 : A scalar
	% bp  : A row vector
	% labels : A row vector with N elements
	errCount = sum(((bp + beta0).*labels<0).*cost);
end

