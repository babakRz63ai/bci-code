function csp = cspn(c,n,model)
% csp = cspn(c,n)
% csp = cspn(c,n,model)
%
% Creates an instance of two-class CSP solver
%
% c : Number of signal channels
% t : Length of each signal section used as the training and testing sample
% n : number of classes
% model : Model of extension to more than two classes. It can be 'OVR' (the default 
% value), 'OVR.CSP0' or 'OVR.CSP3' for for One Versus Rest or 'SIM' for 
% Simultaneous diagonalization.

if nargin<2
	error "You should specify at least two arguments"
end

if nargin==2
	model='CNV.CSP3';
elseif numel(model)>=7
  model2 = model(5:end);
  if ~strcmpi(model2,'CSP0') && ~strcmpi(model2,'CSP3') && ~strcmpi(model2,'CSP4') && ...
    ~strcmpi(model2,'SIM') && ~strcmpi(model,'CNV.BIN')
		error(['Invalid extension model:',model])
  end
else
  error(['Invalid extension model:',model])
end

if numel(c)!=1 || numel(n)!=1
	error "The given arguments must be scaler"
end

csp.C = int16(c);
%csp.T = int16(t);
csp.n = zeros(1,n);		% Count of trials in each class
csp.nclasses = n;
csp.extensionModel = model;
% Each plane belongs to one class
csp.sigma = zeros(c,c,n);

% For my version of One Versus Rest model, we need covariance matrix of all classes 
% except a certain class
if strncmpi(model,'OVR',3)
	csp.restsigma = zeros(c,c,n);
end
	
end