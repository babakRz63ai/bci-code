function CC = trainBinClassifiers(cspinst, trialX2, classLabels, CCmodel, 
  verbose, restricted, classpairIndex)
  % CC = trainBinClassifiers(cspinst, trialX2, classLabels, CCmodel, verbose=false, 
  %  restricted=false, classpairIndex=1)
  % Trains a set of binary classifiers for each pair of classes in a multi-class
  % CSP, or just a classifier for a single pair of classes if restricted was true.
  
  if nargin<5
    verbose=false;
  end
  
  if nargin<6
    restricted = false;
  end
  
  if nargin<7
    classpairIndex=1;
  end
  
  D = generatePatternsFromCSP(cspinst, trialX2, classLabels);
  % A classifier for each possible pair
  
  if ~restricted
    numPairs = size(D,3);
    CC = cell(1,numPairs);
    for pair=1:numPairs
      determined = find(~isnan(classLabels(:,pair)));
      CC{pair} = train_sc(D(determined, :, pair), classLabels(determined, pair),
        CCmodel);
      % Test this classifier on the same training data
      R = test_sc(CC{pair}, D(determined, :, pair), CCmodel, 
      classLabels(determined, pair));
      
      if verbose
        disp(sprintf('Testing binary classifier %d on the training data results in accuracy=%5.2f%%',
        pair, R.ACC*100));
      end
    
      if R.ACC<0.5
        error(sprintf('This binary classifier is worse than dog shit! its accuracy is %5.2f%%',
        100*R.ACC));
      end
    end % for pair
  else  % Restricted to one pair
    determined = find(~isnan(classLabels(:,classpairIndex)));
    CC = train_sc(D(determined, :, classpairIndex), classLabels(determined, classpairIndex),
        CCmodel);
    R = test_sc(CC, D(determined, :, classpairIndex), CCmodel, 
      classLabels(determined, classpairIndex));
    if verbose
        disp(sprintf('Testing binary classifier %d on the training data results in accuracy=%5.2f%%',
        pair, R.ACC*100));
    end
    if R.ACC<0.5
        error(sprintf('This binary classifier is worse than dog shit! its accuracy is %5.2f%%',
        100*R.ACC));
    end
  end  
end
