function R = testCSP(csp, trialX2, CC, mode, trueLabels, extra)
% R = testCSP(csp, trialX2, CC, mode, trueLabels=[], extra=[])
%
% Tests a CSP on some trials and returns its accuracy
%
% csp : A trained CSP
% nTrials  : Number of all trials
% trialX2 : An array containing all second moments (auto-correlation) of a centred 
% trial (an array with csp.C rows and columns and nTrials planes)
% CC : A single classifier in case of binary problems or SIM extension, or a 
%     cell array of binary classifiers for OVR models; as returned by  train_sc 
%     from Biosig package
% mode : Classifier model, as used in train_sc
% extra : Extra info. from training. Also can contain verbose
% trueLabels : Optional, a vector of true labels , one of {1,2,3...n}. If these
%              labels are given then R will have these fields:
%       R.kappa 	Cohen's kappa coefficient
%       R.ACC   	Classification accuracy 
%       R.H     	Confusion matrix
% Also these fields if extension model is OVR:
%       R.labelPerBinClass    Predicred class label for each binary subproblem
%       R.accPerBinClass      Accuracy of prediction for each binary subproblem
%
% Returns R, a struct containing these fields:
%   R.output     	output: "signed" distance for each class. 
%		  This represents the distances between sample D and the separating hyperplane
%		  The "signed distance" is possitive if it matches the target class, 
%		  and negative if it lays on the opposite side of the separating hyperplane. 
%   R.classlabel 	class for output data


if nargin<4
	error 'Insufficient arguments given'
end

if nargin<5
  trueLabels=[];
elseif any(trueLabels<1) || any(trueLabels>csp.nclasses)
	disp(trueLabels(:)');
	error 'Invalid true labels'
end

if nargin<6
  extra.verbose=false;
end

if csp.nclasses==2
  D = generatePatternsFromCSP(csp, trialX2);
  if isempty(trueLabels)
    R = test_sc(CC, D, mode);
  else
    R = test_sc(CC, D, mode, trueLabels);
  end
elseif strncmpi(csp.extensionModel,'CNV',3)  % Conventional models
  D = generatePatternsFromCSP(csp, trialX2);
  if isempty(trueLabels)
    R = test_sc(CC, D, mode);
  else
    R = test_sc(CC, D, mode, trueLabels);
  end
elseif strncmpi(csp.extensionModel,'OVR',3)  % OVR models
  if ~iscell(CC)
    error 'CC must be a cell array for OVR models'
  end
  % Will generate a 3D matrix,each row a sample, each column a feature, 
  % and each plane for a class
  moment=cputime;
  D = generatePatternsFromCSP(csp, trialX2);
  if extra.verbose
    disp(sprintf('testCSP : Time of generating patterns=%f',cputime-moment));
  end
  
  R.output = zeros(nTrials, csp.nclasses);
  R.classlabel = zeros(nTrials,1);
  R.labelPerBinClass = zeros(nTrials, csp.nclasses);
  R.accPerBinClass = zeros(1,csp.nclasses);
  % Classfies for each class against rest of them to get a discriminant
  moment=cputime;
  for c=1:csp.nclasses
    binLabels = zeros(size(trueLabels));
    binLabels(trueLabels==c)=1;
    binLabels(trueLabels~=c)=2;
    rc = test_sc(CC{c}, squeeze(D(:,:,c)), mode, binLabels);
    R.output(:,c) = rc.output(:,1);   % Distances to the hyperplane for this class
    R.labelPerBinClas(:,c) = rc.classlabel;
    R.accPerBinClass(c) = rc.ACC;
  end
  if extra.verbose
    disp(sprintf('testCSP : Time of testing with sub-classifiers=%f',cputime-moment));
    style={'bo','r+','g^','*m','xy'};
    figure
    if (size(D,2)>=3)
      for c=1:csp.nclasses
        hold on
        plot3(D(trueLabels==c, 1, c), D(trueLabels==c, 2, c), D(trueLabels==c, 3, c), style{c});
      end
      hold off
    elseif (size(D,2)>=2)
      for c=1:csp.nclasses
        plot(D(trueLabels==c, 1, c), D(trueLabels==c, 2, c), style{c});
        hold on
      end
    else
      for c=1:csp.nclasses
        plot(D(trueLabels==c, 1, c), zeros(sum(trueLabels==c),1), style{c});
        hold on
      end
    end
  end
  if ~isempty(extra)
    R.output = (R.output-repmat(extra.meanDist,[size(R.output,1), 1]))./repmat(extra.stdDist, [size(R.output,1), 1]);
  end
  %if verbose
  %  disp 'Distances to the hyperplanes are:'
  %  disp(R.output);
  %end
  % Final classifying
  moment=cputime;

    % How many of them are positive?
    %indPositive = R.output>0;
    %nPositive = sum(indPositive,2);
    %[value, indexMax] = max(R.output,[],2);
    %[value, indexMin] = min(abs(R.output),[],2);
    %R.classlabel = ifelse(nPositive==1, findFirstInRow(indPositive),
    %  ifelse(nPositive>1, indexMax, indexMin));
      
    %if nPositive==1	% Selects the only propare class
		%		R.classlabel(t) = indPositive(1);
		%elseif nPositive>1
		  % Selects a class for which distance to the hyperplane is the largest
		%	[value, index] = max(R.output(t,:));
		%	R.classlabel(t) = index;
		%else	% Non of them are positive, so assign to a class with the smallest |g|/norm(beta)
	  %		[value, index] = min(R.output(t,:));
		%	R.classlabel(t) = index;
		%end
    
  for t=1:nTrials
    % How many of them are positive?
    indPositive = find(R.output(t,:)>0);
    nPositive = numel(indPositive);
    if nPositive==1	% Selects the only propare class
  				R.classlabel(t) = indPositive(1);
		elseif nPositive>1
		  % Selects a class for which distance to the hyperplane is the largest
			[value, index] = max(R.output(t,:));
			R.classlabel(t) = index;
		else	% Non of them are positive, so assign to a class with the smallest |g|/norm(beta)
			[value, index] = min(abs(R.output(t,:)));
			R.classlabel(t) = index;
		end
   end % for each trial
  if verbose
    disp(sprintf('testCSP : Time of final classifying=%f',cputime-moment));
  end
  
  if ~isempty(trueLabels)
    moment=cputime;
    [R.kappa,R.sd,R.H,z,R.ACC] = kappa(trueLabels, R.classlabel);
    if extra.verbose
      disp(sprintf('testCSP : Time of calculating kappa=%f',cputime-moment));
    end
  end
else
  error 'Other extension models are not implemented'  
end

end

function ind = findFirstInRow(b)
  ind = zeros(size(b,1),1);
  for col=1:size(x,2)
    ind(ind==0 & b(:,col)) = col;
  end
end
