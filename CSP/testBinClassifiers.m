function R = testBinClassifiers(cspinst, trialX2, CC, CCtype, trueLabels, restricted, classPairIndex)
  % R = testBinClassifiers(cspinst, trialX2, CC, CCtype, 
  %    trueLabels=[], restricted=false, classPairIndex=1)
  % Tests a set of binary classifiers for each pair of classes in a multi-class
  % CSP on test data. Returns structure R containing accuracy of each binary
  % classifier and predicted labels, accuracy and kappa for total multiclass 
  % problem by voting between binary classifiers.
  %
  % Retusn struct R containing:
  %   R.Rbin : A cell array of structs, or just a struct with fields output and classlabel, one for 
  % each pair of classes.
  %   R.classlabel : Predicred final labels for each trial
  % If trueLabels is given and isn't empty, the result will contain these too:
  %   R.Rbin{k}.ACC : Accuracy of each binary classifier restricted to trials
  %     belonging to one of the same two classes.
  %   R.ACC : Total accuracy
  %   R.H   : Total confusion matrix
  %   R.kappa : Total kappa score
  if nargin<6
    trueLabels=[];
  end
  
  if nargin<7
    restricted=false;
  end
  
  if nargin<8
    classPairIndex = 1;
  end
  
  
  % one row for each trial, one column for each filter and one plane for each pair
  D = generatePatternsFromCSP(cspinst, trialX2);
  % A classifier for each possible pair
  if restricted
    numPairs=1;
  else
    numPairs = size(D,3);
    R.Rbin = cell(numPairs,1);
  end
  
  switch (cspinst.nclasses)
    case 2
      pairGenerator = [1 2];
    case 3
      pairGenerator = [1 2; 1 3; 2 3];
    case 4
      pairGenerator = [1 2; 1 3; 1 4; 2 3; 2 4; 3 4];
    otherwise
      error 'More than four classes or less than two classes'
  end   
  
  if ~restricted
  votes = zeros(nTrials, numPairs);
  
  for pair=1:numPairs
    R.Rbin{pair} = pairWorker(pair, CC{pair}, CCtype, D, trueLabels, pairGenerator);
    % All target labels at once
    votes(:,pair) = pairGenerator(pair, R.Rbin{pair}.classlabel');
  end
  
  % Voting for the main problem
  
    sumvotes = zeros(nTrials, cspinst.nclasses);
    for c=1:cspinst.nclasses
      sumvotes(:,c) = sum(votes==c,2);
    end
    [v,R.classlabel] = max(sumvotes,[],2);
  
  % Total accuracy
    if ~isempty(trueLabels)
      [R.kappa, sd, R.H, z, R.ACC, sACC, MI] = kappa(trueLabels,R.classlabel);
    end
else  % restricted
  R.Rbin = pairWorker(classPairIndex, CC, CCtype, D, trueLabels, pairGenerator);
end

end

function Rbin = pairWorker(pair, CC, CCtype, D, trueLabels, pairGenerator)
  % Rbin{pair} will contain fields 'output' and 'classlabel'
    Rbin = test_sc(CC, D(:,:,pair), CCtype);
    if ~isempty(trueLabels)
      binTrueLabels = nan(1,size(D,1));
      binTrueLabels(trueLabels==pairGenerator(pair,1))=1;
      binTrueLabels(trueLabels==pairGenerator(pair,2))=2;
      idx=~isnan(binTrueLabels);
      Rbin.ACC = sum(Rbin.classlabel(idx)==binTrueLabels(idx))/sum(idx);
    end
end

