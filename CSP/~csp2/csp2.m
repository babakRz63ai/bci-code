# csp = CSP2(c,t)
#
# Creates an instance of two-class CSP solver
#
# c : Number of signal channels
# t : Length of each signal section used as the training and testing sample
function csp = csp2(c,t)
if nargin<2
	error "You should specify at least two arguments"
endif

if numel(c)!=1 || numel(t)!=1
	error "The given arguments must be scaler"
endif

csp.C = int16(c);
csp.T = int16(t);
csp.sigmaPlus = zeros(c);
csp.sigmaMinus = zeros(c);
csp.nplus = 0;
csp.nminus = 0;
csp = class(csp,"csp2");
endfunction