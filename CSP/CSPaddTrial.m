function outcsp = CSPaddTrial(csp, signal2, labels, verbose)
% outcsp = CSPaddTrialPart1(csp, signal2, labels, verbose=false)
% 	Acculumates data of a trial into an instance of CSP for the first part of
%	training the CSP. 
%
%	csp        :  CSP
% Note : in case of two classes , class (+) will be the #1 and class (-) the #2
% signal2 : set of covariance matrices of each trial as a C by C by N arrayfun
% labels : Vector of N class labels
if (nargin<3)
	error 'Insufficient parameteters given'
end

if nargin<4
  verbose=false;
end


outcsp = csp;
for k=1:size(signal2,3)
  classIndex = labels(k);
  x2 = signal2(:,:,k);
  %if verbose
	%  disp(sprintf('Training trial %d , class %d',k,classIndex))
  %  fflush(stdout);
  %end
  
	if (classIndex<1 | classIndex>csp.nclasses)
		error(sprintf('Invalid class index; It must be one of 1,2,...%d',csp.nclasses));
	end
	if all(x2(:)==0)
		warning(sprintf("X2 is all zero in trial %d",k));
		%else if any(isnan(x(:)))
		%	index=find(isnan(x(:)));
		%	[r,c] = ind2sub(size(x), index(1));
		%	error(sprintf('NaN values in X, trial index %d, row %d, column %d',
		%		k, r, c))
		%end
	end
	%TODO vectorize
	if (csp.nclasses==2)
		if (classIndex==2)
			outcsp.sigmaMinus = outcsp.sigmaMinus + x2;
		else
			outcsp.sigmaPlus = outcsp.sigmaPlus + x2;
		end
	else
		% more general case
		outcsp.sigma(:,:,classIndex) = outcsp.sigma(:,:,classIndex) + x2;
		% The rest of all other classes for each class
		if (strncmpi(csp.extensionModel, 'OVR', 3))
			for other = [1:classIndex-1, classIndex+1:csp.nclasses]
				outcsp.restsigma(:,:,other) = outcsp.restsigma(:,:,other) + x2;
			end
		end
	end
  
	% Correct for both cases
	outcsp.n(classIndex) = outcsp.n(classIndex)+1;
	
end % for
if verbose
  disp "Count of each class :"
  disp(outcsp.n)
  fflush(stdout);
end
end
		

