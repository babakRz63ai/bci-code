function [trial, label] = CSSPTrialGenerator(tau, indexSet, index, orgTrialGenerator)
% Generate an augmented signal sample as [x(t); x(t-tau)]
[x,label] = orgTrialGenerator(indexSet(index));
% disp(sprintf('trainCSSP.mTrialGenerator returns label=%d',label))
trial = [x; [zeros(size(x,1), tau), x(:,1:end-tau)]];
end