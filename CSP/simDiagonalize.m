function [V,D] = simDiagonalize(COVin,epsillon,verbose)
% [V,COVout] = simDiagonalize(COVin, epsillon, verbose=false)
% Simultaneously diagonalizes planes of COVin by a transform matrix V so that
% COVout(:,:,k) = V*COVin(:,:,k)*V'
  if nargin<4
    verbose=false;
  end
  V = simdiag_lsmopt2(COVin,epsillon,0.95,verbose);
  D = zeros(size(COVin));
  n = size(COVin,1);
  X = zeros(n,n);
  for k=1:size(COVin,3)
    D(:,:,k) = V*COVin(:,:,k)*V';
    X = X + D(:,:,k);
  end
  
  % Most probably X<>I, so we need to correct V
  V = sqrtm(inv(X))*V;
  for k=1:size(COVin,3)
    D(:,:,k) = V*COVin(:,:,k)*V';
  end
  
end