function D = generatePatternsFromCSP(cspinst, trialX2, classLabels)
% [D, classLabels] = generatePatternsFromCSP(cspinst, trialX2, classLabels=[])
%
% Generates matrix of patterns and vector of class labesl using a CSP from some
% EEG signal
% 
% cspinst : CSP instance (data structure)
% trialX2 : Trials second moments as a 3D array
%
% Returns:
%   D : training samples (each row is a sample, each column is a feature, 
%       possibly each plane for a class)
%   classLabels : A vector or matrix containing labels of each sample 

if (nargin<2)
	error "Insufficient parameteters given"
end

nTrials = size(trialX2, 3);

if cspinst.nclasses==2
  D = zeros(nTrials, cspinst.selectedFilters);
  if isOctave
    for t=1:cspinst.selectedFilters
      D(:,t) = squeeze(log(sum(sum(cspinst.W(:,t).*trialX2,1).*cspinst.W(:,t)',2)));
    end
  else
    for k=1:nTrials
    % Mapping to the feature space
    % Don't vectorize this loop
      for t=1:cspinst.selectedFilters
        D(k,t) = log(cspinst.W(:,t)'*trialX2(:,:,k)*cspinst.W(:,t));
      end  
    end % for k
  end
elseif strncmpi(cspinst.extensionModel, 'OVR', 3)
  if nargin>=3 %strcmpi(operation, 'training')
  D = zeros(nTrials, cspinst.selectedFilters);
  for k=1:nTrials
    x2 = trialX2(:,:,k);
    if isempty(classIndex)
      error 'Class labels are mandatory in the training stage'
    end
    
    % Mapping to the feature space
    % Don't vectorize this loop
    for t=1:cspinst.selectedFilters
      D(k,t) = log(cspinst.W(:,t,classIndex)'*x2*cspinst.W(:,t,classIndex));
    end
  end
  else % testing in OVR
  D = zeros(nTrials, cspinst.selectedFilters, cspinst.nclasses);
  for t=1:nTrials
    x2 = trialX2(:,:,t);
    for classIndex=1:cspinst.nclasses
      % Don't vectorize this loop
      for f=1:cspinst.selectedFilters
        D(t,f,classIndex) = log(cspinst.W(:,f,classIndex)'*x2*cspinst.W(:,f,classIndex));
      end
    end  
  end
end
elseif strcmpi(cspinst.extensionModel, 'CNV.BIN')
  numPairs = cspinst.nclasses*(cspinst.nclasses-1)/2;
  D = nan(nTrials, cspinst.selectedFilters, numPairs);
  if nargin>=3 % strcmpi(operation, 'training')
    % One label for each trial in each possible pair.
    % To get pair index for each pair
    classPairIndex = nan(cspinst.nclasses-1, cspinst.nclasses);
    k=0;
    for c1=1:cspinst.nclasses-1
      classPairIndex(c1,c1+1:end) = [1:cspinst.nclasses-c1]+k;
      k=k+cspinst.nclasses-c1;
    end
  
    for t=1:nTrials
      x2 = trialX2(:,:,t);
      classIndex = classLabels(t);
      % Extract a feature from each pair of this class with other classes
      otherclasses=[1:classIndex-1, classIndex+1:cspinst.nclasses];
      for o=1:cspinst.nclasses-1
        pairidx = classPairIndex(min(classIndex, otherclasses(o)), max(classIndex, otherclasses(o)));
        for f=1:cspinst.selectedFilters
          D(t, f, pairidx) = log(cspinst.W(:,f,pairidx)'*x2*cspinst.W(:,f,pairidx));
        end
        
        if classIndex<otherclasses(o)
          classLabels(t,pairidx) = 1;
        else
          classLabels(t,pairidx) = 2;
        end
      end % for o
    end % for t
  else  % testing with binary classifiers
    % each trial will have features in each class pair
    for t=1:nTrials
      x2 = trialX2(:,:,t);
      for pairidx=1:numPairs
        for f=1:cspinst.selectedFilters
          D(t,f,pairidx) = log(cspinst.W(:,f,pairidx)'*x2*cspinst.W(:,f,pairidx));
        end
      end
    end
  end
  
elseif strncmpi(cspinst.extensionModel, 'CNV', 3)
  D = zeros(nTrials, cspinst.selectedFilters*cspinst.nclasses);
  for k=1:nTrials
    x2 = trialX2(:,:,k);
    for classIndex=1:cspinst.nclasses
      % Don't vectorize this loop
      for t=1:cspinst.selectedFilters
        D(k,cspinst.selectedFilters*(classIndex-1)+t) = log(cspinst.W(:,t,classIndex)'*x2*cspinst.W(:,t,classIndex));
      end
    end  
  end
else
  error 'Other extension models than OVR and CNV is not supported'
end

end
