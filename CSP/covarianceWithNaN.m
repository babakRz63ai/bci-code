function x2 = covarianceWithNaN(x)
  % x2 = covarianceWithNaN(x)
	% Calculates x*x' by ignoring NaN's. if x is a signal whose rows are recorded channels, then 
	% each element of x*x' will be a covariance of two channels in the given 
	% time span
  
  idx=isfinite(x);
  if all(idx(:))
    x2 = x*x'/size(x,2);
  else
    x(~idx)=0;
    % Number of normal raw elements contributing to a covariance one
    corrected = idx*idx';
    % some elements of corrected will be zero when all original values on a row of x are NaN.
    % In these cases x*x' in those positions will be zero too.
    corrected(corrected==0) = 1;
    x2 = (x*x') ./ corrected;
end

