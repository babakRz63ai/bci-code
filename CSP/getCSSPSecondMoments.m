function [s2,allClassLabels] = getCSSPSecondMoments(tau, C, T, indexSet, orgTrialGenerator, checkNaNs)
  % [s2,allClassLabels] = getCSSPSecondMoments(tau, C, T, indexSet, orgTrialGenerator, checkNaNs=false)
  % Calculates second moments (autocorrelations) of signals of trials in a CSSP
  % problem.
  %
  % tau : Time shift of duplicated signals
  % C   : Number of original signal channels
  % T   : Time points in each trial
  % indexSet: A set of indices to be used instead of the linear sequence.This is
  % a vector with N elements.
  % orgTrialGenerator : Original trial generator function
  %
  % Returns:
  % s2 : A three dimensional array with C rows, C columns and N plnes
  %  allClassLabels : A vector containing all class labels
  
  if nargin<6
    checkNaNs=false;
  end
    
  allClassLabels=zeros(numel(indexSet),1);
  % Storage for second moments from signals
  s2 = zeros(2*C,2*C,numel(indexSet));
  central = double(eye(T)-ones(T)/T);
  if checkNaNs
    for t=1:numel(indexSet)
      [X,y] = CSSPTrialGenerator(tau, indexSet, t, orgTrialGenerator);
      s2(:,:,t) = covarianceWithNaN(X*central);
      allClassLabels(t) = y;
    end
  else
    for t=1:numel(indexSet)
      [X,y] = CSSPTrialGenerator(tau, indexSet, t, orgTrialGenerator);
      X = X*central;
      s2(:,:,t) = X*X';
      allClassLabels(t) = y;
    end
end