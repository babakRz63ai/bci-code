function outcsp = CSPaddTrialPart2(csp, nTrials, trialGenerator, verbose)
% outcsp = CSPaddTrialPart2(csp, nTrials, trialGenerator)
% 	Acculumates data of a trial into an instance of CSP for the first part of
%	training the CSP. 
%
%	csp        :  CSP
%	nTrials    :  Count of trials
%	trialGenerator: Trial generator function handle
if (nargin<3)
	error "Insufficient parameteters given"
end



%[cx,tx] = size(x);
%if cx!=csp.C || tx!=csp.T
%	error(sprintf("Invalid dimensions of the data matrix. It must be %dx%d", csp.C,csp.T));
%end

%if size(csp.W,1)!=csp.C
%	error(sprintf("Invalid dimensions of spacial filters. They should have %d elements each",
%	csp.C));
%end

outcsp = csp;
central = eye(csp.T)-ones(csp.T);
if csp.nclasses==2
	for k=1:nTrials
		[x,classIndex] = trialGenerator(k);
		% centering the signal
		x = x*central;
		% Mapping to the feature space
		outcsp.patterns(:,k) = log(diag(outcsp.W'*covarianceWithNaN(x)*outcsp.W));
		outcsp.classLabel(k) = classIndex;
	end	% for k
else if isequal(csp.extensionModel, 'OVR')
	% Patterns of each class are generated from features extracted by spatial filters of the same class
	for k=1:nTrials
    if verbose
      disp(sprintf('Training on trial %d',k));
      fflush(stdout);
    end
    
		[x,classIndex] = trialGenerator(k);
		x = x*central;
		outcsp.patterns(:,k) = log(diag(outcsp.W(:,:,classIndex)'*covarianceWithNaN(x)*outcsp.W(:,:,classIndex)));
		outcsp.classLabel(k) = classIndex;
	end % for k
	end	% if extension model is OVR
end
end
		

