function cspdemo2

Up = [-0.93000   0.34000; 0.34000   0.93000];
Lp = [0.250000   0.00000; 0.00000   2.50000];
Um = [-1, 1; 1 1];
Lm = [0.25000   0.00000; 0.00000   1.50000];

% Generate training samples randomly
Xp = Up*Lp*randn(2,50);
Xm = Um*Lm*randn(2,50);
% Generate test samples randomly
XpTest = Up*Lp*randn(2,500);
XmTest = Um*Lm*randn(2,500);

analyze(Xp, Xm, XpTest, XmTest, 'original data')
% An outlier
Xm(:,1) = [4; -4];
analyze(Xp, Xm, XpTest, XmTest, 'data with outlier')


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [w,w0] = FisherLDA(C1,C2)
% C1 and C2 : two matrices with p rows for dimensions and n_i columns for samples
p = size(C1,1);
n1 = size(C1,2);
n2 = size(C2,2);
S1 = zeros(p,p);
S2 = zeros(p,p);
% Covariance matrices
for t=1:n1
	S1 = S1+C1(:,t)*C1(:,t)';
end

for t=1:n2
	S2 = S2+C2(:,t)*C2(:,t)';
end

% the pooled within-class sample covariance matrix
Sw = (S1+S2)/(n1+n2-2);

% Averages
m1 = mean(C1,2);
m2 = mean(C2,2);

% Results
w = inv(Sw)*(m1-m2);
w0 = -0.5*(m1+m2)'*inv(Sw)*(m1-m2)-log(n2/n1);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function analyze(Xp, Xm, XpTest, XmTest, titleAffix)
% Estimates of covariance matrices
Sp=zeros(2,2);
for k=1:50
	Sp=Sp+Xp(:,k)*Xp(:,k)';
end
Sp=Sp/50;

Sm=zeros(2,2);
for k=1:50
	Sm=Sm+Xm(:,k)*Xm(:,k)';
end
Sm=Sm/50;

% Calculate spatial filters
[W,Lambda] = eig(Sp,Sm);

% Map points by spetial fiters
Yp = W'*Xp;
Ym = W'*Xm;

% Map to the Phi space (log-power)
phi_p = log(Yp.^2);
phi_m = log(Ym.^2);

% Perform Fisehr LDA to obtain a discriminant function
[beta,beta0] = FisherLDA(phi_p, phi_m);


% Plots
figure
plot(Xp(1,:),Xp(2,:),'r+');
hold on
plot(Xm(1,:),Xm(2,:),'bo');
title(['Raw data of ',titleAffix])
xlabel('x_1')
ylabel('x_2')

figure
plot(Yp(1,:),Yp(2,:),'r+');
hold on
plot(Ym(1,:),Ym(2,:),'bo');
title(['After CSP filtering for ',titleAffix])
xlabel('w_1^T X')
ylabel('w_2^T X')

figure
plot(phi_p(1,:),phi_p(2,:),'r+');
hold on
plot(phi_m(1,:),phi_m(2,:),'bo');
% Decision boundary
if abs(beta(2))>abs(beta(1))
	phi1l = min([phi_p(1,:), phi_m(1,:)]);
	phi1h = max([phi_p(1,:), phi_m(1,:)]);
	phi2l = (-beta0-beta(1)*phi1l)/beta(2);
	phi2h = (-beta0-beta(1)*phi1h)/beta(2);
else
	phi2l = min([phi_p(2,:), phi_m(2,:)]);
	phi2h = max([phi_p(2,:), phi_m(2,:)]);
	phi1l = (-beta0-beta(2)*phi2l)/beta(1);
	phi1h = (-beta0-beta(2)*phi2h)/beta(1);
end
line([phi1l, phi1h],[phi2l, phi2h])
title(['Phi space (log-power) for ',titleAffix])
xlabel('log(w_1^T X X^T w_1)')
ylabel('log(w_2^T X X^T w_2)')

% Generating some test points and classifing them
Y = W'*XpTest;
phi = log(Y.^2);
Ipp = beta(1)*phi(1,:) + beta(2)*phi(2,:) + beta0 > 0;

Y = W'*XmTest;
phi = log(Y.^2);
Imm = beta(1)*phi(1,:) + beta(2)*phi(2,:) + beta0 < 0;

disp(sprintf('For %s : True positive = %d\nFalse negative = %d\nTrue negative = %d\nFalse positive = %d', titleAffix, sum(Ipp), size(XpTest,2)-sum(Ipp), sum(Imm), size(XmTest,2)-sum(Imm)))

end