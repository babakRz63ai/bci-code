function outcsp = solveCSP(csp,verbose)
% outcsp = solveCSPPart1(csp)
% Solves a CSP problem by finding six eigenvectors coressponding to three eigenvalues 
% from each end of the spectrum of the eigenvalues
outcsp = csp;
selectedFiltersFromEachEnd = csp.selectedFilters/2;
if csp.nclasses==2
  
  outcsp.W = solveBinaryCSP(csp.sigmaPlus/csp.n(1), csp.sigmaMinus/csp.n(2),
    csp.selectedFilters, verbose);
  
	
else
  COV = covarianceMatrices(csp);
  outcsp.W = zeros(csp.C, outcsp.selectedFilters, csp.nclasses);
	%outcsp.A = zeros(csp.C, outcsp.selectedFilters, csp.nclasses);
  if strcmpi(csp.extensionModel, 'OVR.CSP4') % Several classes
	for c=1:csp.nclasses
    if verbose
      disp(sprintf('solveCSPPart1 Calculates OVR items for class %d...',c))
      fflush(stdout);
    end
		restsigma = csp.restsigma(:,:,c)/sum(csp.n([1:c-1,c+1:end]));
		[W,lambda] = eig(COV(:,:,c), COV(:,:,c)+restsigma);
		%A = inv(W)';
		if csp.C~=csp.selectedFilters		% Selects some eigenvectors with the largest or smallest eigenvalues
			[S,I] = sort(diag(lambda));
			%disp([lambda, S, I, lambda(I)])
			outcsp.W(:,:,c) = W(:,I([1:selectedFiltersFromEachEnd,end-selectedFiltersFromEachEnd+1:end]));
			%outcsp.A(:,:,c) = A(:,I([1:selectedFiltersFromEachEnd,end-selectedFiltersFromEachEnd+1:end]));
			if verbose
        disp "Selected eigen values:"
        lambda = diag(lambda);
			  disp(lambda(I([1:selectedFiltersFromEachEnd,end-selectedFiltersFromEachEnd+1:end]))');
      end
      
		else
			outcsp.W(:,:,c) = W;
			%outcsp.A(:,:,c) = A;
		end
	end % for c
	%disp "W is:"
	%disp(outcsp.W)
elseif strcmpi(csp.extensionModel(5:end), 'CSP0')    % common diagonalization 
  [P,D] = eig(squeeze(sum(COV,3)));
  P = diag(sqrt(1./diag(D)))*P';
  for c=1:csp.nclasses
    C = P * squeeze(COV(:,:,c)) * P';
  	[R,d1]  = eig((C+C')/2);
    [d1,ix] = sort(diag(d1));
    outcsp.W(:,:,c) = P'*R(:,ix([1:selectedFiltersFromEachEnd,end-selectedFiltersFromEachEnd+1:end]));
  end
elseif strcmpi(csp.extensionModel(5:end), 'CSP3')
  	%% do actual CSP calculation as generalized eigenvalues
	%% R = permute(COV,[2,3,1]);
	for c = 1:csp.nclasses
		[W,D] = eig(COV(:,:,c),sum(COV,3));
		outcsp.W(:,:,c) = W(:,[1:selectedFiltersFromEachEnd,end-selectedFiltersFromEachEnd+1:end]);
	end;
elseif strcmpi(csp.extensionModel(5:end), 'SIM')  % Simultaneous diagonalization
  if verbose
    disp 'Simultaneous diagonalization...'
    fflush(stdout);
  end
   % Reset selected filters, because we select less than specified
  outcsp.selectedFilters = selectedFiltersFromEachEnd;
  [V,COV] = simDiagonalize(COV,0.1,verbose);
  % Now spatial filters are stored in V and eigenvalues for each class are in diagonals of COV(:,:,k)
  
  N = size(COV,1);
  % Will be there enough filters?
  if N<outcsp.selectedFilters*outcsp.nclasses
    error 'Input signal doesn''t have enough channels'
  end
  
 [r,c,ix] = getRCIXforSimDiag(COV);
  
  outcsp.W = selectSpatialFiltersForSimDiag(r,c,ix,N,outcsp.nclasses,outcsp.selectedFilters,V,verbose);

elseif strncmpi(csp.extensionModel, 'CNV.BIN', 7) % Binary classifiers for each pair of classes
  % Each plane belongs to one pair
  outcsp.W = zeros(csp.C, outcsp.selectedFilters, csp.nclasses*(csp.nclasses-1)/2);
  k=1;
  for c1=1:csp.nclasses-1
    for c2=c1+1:csp.nclasses
      outcsp.W(:,:,k) = solveBinaryCSP(COV(:,:,c1), COV(:,:,c2), csp.selectedFilters, verbose);
      k=k+1;
    end % for c2
  end % for c1
else
		error 'Other extenssion models than OVR are not implemented'
end
end   % else number of classes is greater then two

end

