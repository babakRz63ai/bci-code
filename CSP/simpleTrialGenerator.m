function [X,label] = simpleTrialGenerator(index, indexSet, s, labels)
  % [X,label] = simpleTrialGenerator(index, indexSet, s, labels)
  % Generate trials stored in a three dimensional array.
  %
  % index : a scalar index
  % indexSet : A vector
  % s : A three dimensional array with C rows, C columns and N planes
  % labels : a vector of class labels or an empty array
  %
  % Returns:
  %   X : A trial signal as a matrix with C rows and T columns
  %   label : An integer scalar
  
  X = s(:,:,indexSet(index));
  if ~isempty(labels)
    label = labels(indexSet(index));
  else
    label=[];
  end
end
