% csp = csp2(c)
%
% Creates an instance of two-class CSP solver
%
% c : Number of signal channels
function csp = csp2(c)
if nargin<1
	error "You should specify at least one argument"
end

if numel(c)!=1
	error "The given arguments must be scaler"
end

csp.C = int16(c);
csp.sigmaPlus = zeros(c);
csp.sigmaMinus = zeros(c);
csp.n = zeros(1,2);		% Count of trials in each class
csp.nclasses = 2;
end