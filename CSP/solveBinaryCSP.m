function W = solveBinaryCSP(sigma1, sigma2, selectedFilters, verbose)
  % W = solveBinaryCSP(sigma1, sigma2, selectedFilters, verbose=false)
  % solves a binary CSP and returns set of spatial filters
  % sigma1, sigma2 : two covariance matrices
  % selectedFilters : number of selected filters
  % verbose : controls verbosity
  if nargin<4
    verbose=false;
  end
  
  % lambda will be a diagonal matrix
	[W,lambda] = eig(sigma1, sigma1+sigma2);
  % Sanity check
  L1 = W'*sigma1*W;
  L2 = W'*sigma2*W;
  channels = size(sigma1,1);
  diff=eye(channels)-(L1+L2);
  sumdiff = sum(abs(diff(:)));
  %if verbose
  %  disp(sprintf('solveBinaryCSP checks wether diff=%f is less than 1e-7', 
  %    sumdiff));
  %  fflush(stdout);
  %end
  
  if sumdiff>1e-7
    
    sigma1p = trimToNDigits(sigma1,4);
    sigma2p = trimToNDigits(sigma2,4);
    [W,lambda] = eig(sigma1p, sigma1p+sigma2p);
    L1 = W'*sigma1p*W;
    L2 = W'*sigma2p*W;
    diff=eye(channels)-(L1+L2);
    %orgsumdiff = sumdiff;
    sumdiff = sum(abs(diff(:)));
    %if verbose
    %  disp(sprintf('Correcting covariance matrices, changes sumdiff from %f to %f',
    %  orgsumdiff, sumdiff));
    %  fflush(stdout);
    %end
  end

  %assert(sumdiff<1e-5*size(sigma1,1)^2);
  if (sumdiff>=1e-5*size(sigma1,1)^2)
    W=[];
    if verbose
      warning('sumdiff is %f while was expected to be less than %f',sumdiff,1e-5*size(sigma1,1)^2);
    end
    return
  end
  
  
	%A = inv(W)';
	% Selects some of them
	if channels>selectedFilters
		[S,I] = sort(abs(diag(lambda)));
    selectedFiltersFromEachEnd = selectedFilters/2;
		W = W(:,I([1:selectedFiltersFromEachEnd,end-selectedFiltersFromEachEnd+1:end]));
		%A = A(:,I([1:selectedFiltersFromEachEnd,end-selectedFiltersFromEachEnd+1:end]));
    %if verbose
    %  lambda = abs(diag(lambda));
    %  disp 'Selected eigen values:'
    %  disp(lambda(I([1:selectedFiltersFromEachEnd,end-selectedFiltersFromEachEnd+1:end])));
    %end
  end
end
%------------------------
function y = trimToNDigits(x,n)
  p = 10^(n-fix(log10(max(abs(x(:))))));
  y = fix(x*p)/p;
end
