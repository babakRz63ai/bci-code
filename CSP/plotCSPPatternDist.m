function plotCSPPatternDist(csp)
% plotCSPPatternDist(cps)
%
% Displays some plots of distribution of extracted patterns in a CSP

format={'b-','r--','g-.','k:'};

% LDA method maps patterns on a stright line, so the final plot is a histogram
figure
hold on
for c=1:csp.nclasses
	[nn,xx]=hist(csp.beta' * csp.patterns(:,csp.classLabel==c));
	plot(xx,nn/sum(csp.n),format{c})
end
title 'Histograms of mapped patterns'
if csp.nclasses==2
	legend('Class (+)','Class (-)')
else
	names=cell(1,csp.nclasses);
	for c=1:csp.nclasses
		names{c}=sprintf('Class %d',c);
	end
	legend(names)
end
line([-csp.beta0, -csp.beta0],[0,1])
hold off
end	