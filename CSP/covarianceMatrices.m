function COV = covarianceMatrices(cspinst)
  COV = zeros(size(cspinst.sigma)); % All covariance matrices
  for c=1:cspinst.nclasses
    COV(:,:,c) = cspinst.sigma(:,:,c)/cspinst.n(c);
  end
end
