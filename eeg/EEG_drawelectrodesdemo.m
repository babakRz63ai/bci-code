function EEG_drawelectrodesdemo(setName, drawNames)
% drawelectrodesdemo(setName, drawNames)
% Draws a blank plot of the sclap with a set of electrodes
%
% setName	:	One of '10-10-Cap33', '10-10-Cap47', '10-20-Cap19', '10-20-Cap25' or '10-20-Cap81'
% drawNames :	Boolean, shows if names of electrodes should be displayed

[R,Theta,Names] = EEG_loadstdlocs(setName);

%disp 'R ='
%disp(R)
%disp 'Theta ='
%disp(Theta)
%disp 'names are'
%disp(names)

if EEG_isOctave
	prevGraphicsToolkit=graphics_toolkit;
	graphics_toolkit('gnuplot')
end
figure
hold on

Theta=(90-Theta)*pi/180;
xcap=1.5652*R.*cos(Theta);
ycap=1.5652*R.*sin(Theta);


% A random topography
left=min(xcap);
right=max(xcap);
top=max(ycap);
bottom=min(ycap);
z=cos(pi*xcap.*ycap)+10*randn(size(xcap));
zi=griddata(xcap,ycap,z, linspace(left,right), linspace(bottom,top)');
%disp([min(zi(:)), max(zi(:))])
colormap('jet')

%convert zi to integer
zmin=min(zi(:));
zmax=max(zi(:));


image([left,right], [bottom,top], (zi-zmin)*64/(zmax-zmin));
set(gca(),'clim',[zmin,zmax])
colorbar('SouthOutside')

% The frame and caps
x=linspace(-1,1);
plot(x,sqrt(1-x.*x),'k')
plot(x,-sqrt(1-x.*x),'k')
plot([-0.125, 0, 0.125],[0.99216, 1.2, 0.99216],'k')
plot(xcap,ycap,'marker','+',
	'markeredgecolor','black','linestyle','none')
if drawNames
	text(xcap+0.05,ycap-0.05,Names)
end

hold off

if EEG_isOctave
	graphics_toolkit(prevGraphicsToolkit)
end

end
