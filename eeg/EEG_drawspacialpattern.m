function EEG_drawspatialpattern(capsNames, capsValues, displayNames)
% EEG_drawspatialpattern(capsNames, capsValues, displayNames)
%
% Draws a topographical plot of a special pattern on the sclap
%
% capsNames    : A cell array of channel names
% capsValues   : A vector of values in each cap position
% displayNames : Boolean, whether name of caps should be displayed

if nargin<3
	error 'Insufficient arguments given'
end

if ~iscell(capsNames)
	error 'The first argument mmust be a cell array'
end

[allR,allTheta,allNames] = EEG_loadstdlocs('10-20-Cap81');

if EEG_isOctave
	prevGraphicsToolkit=graphics_toolkit;
	graphics_toolkit('gnuplot')
end
figure; hold on

R=zeros(1,numel(capsValues));
Theta=zeros(1,numel(capsValues));
for t=1:numel(capsNames)
	ind=find(strcmp(capsNames{t}, allNames));
	R(t)=allR(ind(1));
	Theta(t)=allTheta(ind(1));
end
Theta=(90-Theta)*pi/180;

xcap=1.5652*R.*cos(Theta);
ycap=1.5652*R.*sin(Theta);

left=min(xcap);
right=max(xcap);
top=max(ycap);
bottom=min(ycap);
zi=griddata(xcap,ycap,capsValues, linspace(left,right,20), linspace(bottom,top,20)');
%disp([min(zi(:)), max(zi(:))])

colormap('jet')
%convert zi to integer
zmin=min(zi(:));
zmax=max(zi(:));
set(gca(),'clim',[zmin,zmax])
image([left,right], [bottom,top], (zi-zmin)*64/(zmax-zmin));
colorbar('SouthOutside')

% The frame and caps
x=linspace(-1,1);
plot(x,sqrt(1-x.*x),'k')
plot(x,-sqrt(1-x.*x),'k')
plot([-0.125, 0, 0.125],[0.99216, 1.2, 0.99216],'k')

plot(xcap,ycap,'marker','+',
	'markeredgecolor','black','linestyle','none')
if displayNames
	text(xcap+0.05,ycap-0.05,capsNames)
end

hold off
if EEG_isOctave
	graphics_toolkit(prevGraphicsToolkit)
end


end