function [R,Theta,Names] = EEG_loadstdlocs(setName)
% [R,Theta,Names] = loadstdlocs(setName)
% Reads a file containing names and positions of standard EEG electrodes
if nargin<1
	error 'Insufficien arguments given'
end
name=sprintf('Standard-%s.locs',setName);
[fid,msg]=fopen(name);
if fid==-1
	error([msg,' name=',name])
end

R=[];
Theta=[];
Names={};
do
	[idx,theta,r,name,count]=fscanf(fid,'%d%f%f%s','C');
	if count>0
		R(idx)=r;
		Theta(idx)=theta;
		Names{idx}=name;
	end
until count==0;
fclose(fid);
end