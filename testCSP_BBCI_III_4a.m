function testCSP_BBCI_III_4a
  subjects={'aa','al','av','aw','ay'};
  for k=2
    disp(['Subejct ',subjects{k},'...']);
    fflush(stdout);
    [data,testLabels] = loadSubjectData(subjects{k});
    demo(data,testLabels);
  end
end

function [data,testLabels] = loadSubjectData(subject)
  % data.signal : A matrix with channels as rows and time samples as columns
  % data.marker : Markers metadata
  load(sprintf('../datasets/bci/bbci.de/BCI-comp-III/4/IVa/100Hz/%s.mat',subject));
  data.signal = cnt';
  data.marker = mrk;
  load(sprintf('../datasets/bci/bbci.de/BCI-comp-III/4/IVa/true_labels_4a_%s.mat',subject));
  testLabels = true_y(test_idx);
end
%-----------------------------------
function demo(data,testLabels)
  ppparams.firstMoment = 0;
  ppparams.lastMoment = 3.5;
  ppparams.lowFrq = 8;
  ppparams.highFrq = 30;
  ppparams.filterLength = 16;
  C = 118;
  disp 'Preprocessing....'
  fflush(stdout);
  s2 = preprocessWithParams(data.signal, data.marker, ppparams, 100);
  
  disp 'Training CSP....'
  fflush(stdout);
  [trainIndex, testIndex] = calcIndices(data.marker);
  cspinst = trainCSP(C, 2, @(idx) simpleTrialGenerator(idx, trainIndex, s2,
    data.marker.y(trainIndex)), numel(trainIndex), '', 3, true);
  disp 'Training classifiers....'
  fflush(stdout);
  D = generatePatternsFromCSP(cspinst, @(idx) simpleTrialGenerator(idx, 
    trainIndex, s2, data.marker.y(trainIndex)), numel(trainIndex), 'training');
  CCmodel.TYPE = 'FLDA';  
  CC = train_sc(D, data.marker.y(trainIndex)(:), CCmodel);
  disp 'Testing....'
  fflush(stdout);
  R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, testIndex, s2, []),
    numel(testIndex), CC, CCmodel, [], testLabels);
  disp 'Confusion matrix:'
  disp(R.H);
  disp(sprintf('Accuracy is %5.2f%%', R.ACC*100));
end
%-----------------------------------
function simulation1(data,trueLabels)
  % Finds the best valus for preprocessing parameters by validating on the test set
  ppparams.lastMoment = 3.5;
  ppparams.lowFrq = 8;
  ppparams.highFrq = 30;
  ppparams.filterLength = 16;
  frqs = 100;
  % first moment
  ppparams.firstMoment = findParameterByValidation(data, frqs, 0:0.125:3,
    'firstMoment', ppparams, trueLabels);
  disp ' '
  % last moment
  ppparams.lastMoment = findParameterByValidation(data, frqs,
    ppparams.firstMoment+0.125:0.125:3.5, 'lastMoment', ppparams, trueLabels);
  disp ' '
  % lowFrq
  ppparams.lowFrq = findParameterByValidation(data, frqs, 6:15, 'lowFrq', ppparams, 
    trueLabels);
  disp ' '
  % highFrq
  ppparams.highFrq = findParameterByValidation(data, frqs, ppparams.lowFrq+1:40,
    'highFrq', ppparams, trueLabels);
  disp ' '
  %filterLength
  ppparams.filterLength = findParameterByValidation(data, frqs, 8:2:40,
    'filterLength', ppparams, trueLabels);
  disp ' '  
  disp(sprintf('selected firstMoment=%f',ppparams.firstMoment));
  disp(sprintf('selected lastMoment=%f',ppparams.lastMoment));
  disp(sprintf('selected lowFrq=%f',ppparams.lowFrq));
  disp(sprintf('selected highFrq=%f',ppparams.highFrq));
  disp(sprintf('selected filterLength=%f',ppparams.filterLength));
end
%-----------------------------------
function parameter = findParameterByValidation(data, frqs, paramValues, name,
  ppparams, trueLabels, verbose)
  % data : A struct with fields signal and marker
  % paramValues : range of values of the parameter to be found
  % name : name of the parameter in ppparams structure
  % trueLabels : vector of true labels of test trials
  % verbose (optional boolean)
  if nargin<7
    verbose=true;
  end
  [trainIndex, testIndex] = calcIndices(data.marker);
  C = 118;
  CCmodel.TYPE = 'FLDA'; 
  allacc = zeros(size(paramValues));
  for k=1:numel(paramValues)
    ppparams = setfield(ppparams,name,paramValues(k));
    s2 = preprocessWithParams(data.signal, data.marker, ppparams, frqs);
    cspinst = trainCSP(C, 2, @(idx) simpleTrialGenerator(idx, trainIndex, s2,
      data.marker.y(trainIndex)), numel(trainIndex), '', 3);
    D = generatePatternsFromCSP(cspinst, @(idx) simpleTrialGenerator(idx, 
      trainIndex, s2, data.marker.y(trainIndex)), numel(trainIndex), 'training');
    CC = train_sc(D, data.marker.y(trainIndex)(:), CCmodel);
    R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, testIndex, s2, []),
      numel(testIndex), CC, CCmodel, [], trueLabels);
    allacc(k) = R.ACC;
    if verbose
      disp(sprintf('%s=%f , accuracy=%5.2f%%', name, paramValues(k), 100*R.ACC));
      fflush(stdout);
    end
  end
  [a,ix] = max(allacc);
  parameter = paramValues(ix);
end  
%-----------------------------------
function signal2 = preprocessWithParams(cntSignal, marker, params, frqs)
  % data.signal : A matrix with channels as rows and time samples as columns
  % data.marker : Markers metadata
  % params : struct of parameters
  % frqs : Sampling frequency
  % Returns : A matrix with C times C times N elements where C is number of 
  % channels and N is number of trials
  C = size(cntSignal,1);
  N = numel(marker.pos);
  signal2 = zeros(C,C,N);
  % Relative to the moment of the cue appearing
  firstSample = fix(frqs*params.firstMoment);
  lastSample = fix(frqs*params.lastMoment);
  T = lastSample-firstSample+1;
  % Impulse response of the band-pass filter
  winoptions.type=1;
	winoptions.alpha=0.5;
  h = wsibandpass(params.lowFrq*2*pi/frqs, params.highFrq*2*pi/frqs, 
    params.filterLength, winoptions);
  % The sampling window  
  window = repmat(hamming(T)', C, 1);
  % Scaling and centralizing
  central = eye(T)-ones(T)/T;
  
  for k=1:N
    % Windowing, filtering and centralizing
    X = filter(h, 1,
      cntSignal(:, marker.pos(k)+firstSample:marker.pos(k)+lastSample).*window, 
      [], 2)*central;
    signal2(:,:,k) = X*X';
  end  
end

function [trainIndex, testIndex] = calcIndices(marker)
    trainIndex = find(~isnan(marker.y));
    testIndex = find(isnan(marker.y));
end



