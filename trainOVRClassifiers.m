function [CC,extra] = trainOVRClassifiers(D, classLabels, ccmodel, ovrmodel)
% [CC,extra] = trainOVRClassifiers(D, classLabels, ccmodel, ovrmodel)
% Trains N binary One Versusu Rest classifiers
%
% D            : Patterns (rows=patterns, columns=features, planes=classes)
% classLabels  : Vector of all class labels, should have nTrials elements
% ccmodel      : Model parameters of the generated classifiers
% ovrmodel     : Options for OVR strategy.This can have these field(s):
%                   fairTrain : A boolean indicating fair training should be 
%                   used, which means samples of classes will be used in equal
%                   numbers and samples of a class may be copied
%
% Returns : CC, A cell array of N classifiers
%           extra, a struct with fields:
%             meanDist ,stdDist : Mean and standard deviation of distance from 
%             decision boundary for each original class.

if nargin<3
  error 'Too few arguments are given'
end

if isempty(classLabels) || any(isnan(classLabels)) || any(classLabels<1)
  error 'Invalid class labels'
end

if numel(size(D))~=2
  error 'D must have 2 dimensions'
end

nclasses=numel(unique(classLabels));

CC = cell(1,nclasses);
if nargout>1
  extra.meanDist = zeros(1,nclasses);
  extra.stdDist = zeros(1,nclasses);
end

fairD = D;

for classIndex = 1:nclasses
      binLabels=[];
      ixpos = find(classLabels==classIndex);
      ixneg = find(classLabels~=classIndex);
      if nargin>=4 && ovrmodel.fairTrain
        nc = numel(ixpos);
        rc = numel(ixneg);
        assert(rc+nc, numel(classLabels))
        maxc = max(nc,rc);
        binLabels = zeros(2*maxc,1);
        fairD = zeros(2*maxc, size(D,2));
        % The first patterns are copied from D
        binLabels(ixpos) = 1;
        binLabels(ixneg) = 2;
        fairD(1:numel(classLabels), :) = D(1:numel(classLabels), :);
        % Repeats samples of the smaller class
        if rc>nc
          q = fix(rc/nc);
          for t=1:q-1
            fairD(rc+t*nc+1:rc+(t+1)*nc, :) = D(ixpos, :);
          end
          reminder = mod(rc,nc);
          if reminder>0
            fairD(rc+q*nc+1:end, :) = D(ixpos(1:reminder), :);
          end
          binLabels(rc+1:end) = 1;
        elseif nc>rc
          q = fix(nc/rc);
          for t=1:q-1
            fairD(nc+t*rc+1:nc+(t+1)*rc, :) = D(ixneg, :);
          end
          reminder = mod(nc,rc);
          if reminder>0
            fairD(nc+q*rc+1:end, :) = D(ixneg(1:reminder), :);
          end
          binLabels(nc+1:end) = 2;
        end
      else
        binLabels = zeros(numel(classLabels),1);
        binLabels(ixpos)=1;
        binLabels(ixneg)=2;
      end
      cci = train_sc(fairD, binLabels, ccmodel);
      CC{classIndex} = cci;
      if nargout>1
        R = test_sc(CC{classIndex}, fairD(ixpos,:), ccmodel);
        extra.meanDist(classIndex) = mean(R.output(:,1));
        extra.stdDist(classIndex) = std(R.output(:,1));
      end
end

end