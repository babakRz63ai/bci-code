t0=cputime;
N=10000;
fprintf(stdout,'   ');
for n=1:N
  fprintf(stdout,'%c%c%c%c%4d',8,8,8,8,n);
  fflush(stdout);
  x=rand(128,128);
  x(fix(rand(100,1)*1024)+1) = nan;
  covarianceWithNaN(x);
end

disp(sprintf('\nAverage CPU time passed is %f',(cputime-t0)/N));
clear t0 x n