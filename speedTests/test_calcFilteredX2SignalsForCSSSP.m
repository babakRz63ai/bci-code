function test_calcFilteredX2SignalsForCSSSP(operation)
  channels = 64;
  T = 1000;
  nClasses = 2;
  numTrials = 300;
  bulkSignal = rand(channels, T, numTrials);
  index = [2:2:numTrials, 1:2:numTrials];
  if isequal(operation,'accuracy')
    b = rand(64, 1);
    s2a = calcInOctave(channels, b, bulkSignal);
    s2b = nativeFilteredX2SignalForCSSSP(b, bulkSignal);
    disp(sprintf('Difference is %f',sum(abs(s2a(:)-s2b(:)))));
  else
    disp('Filter length   In octave   Native');
    nRepeats = 20;
    for SpectFltrLength = 8:8:96
      t1=cputime;
      %for n=1:nRepeats
      %  s2 = calcInOctave(channels, rand(SpectFltrLength, 1), bulkSignal);
      %end
      t2 = cputime;
      for n=1:nRepeats
        s2 = nativeFilteredX2SignalForCSSSP(rand(SpectFltrLength, 1), bulkSignal);
      end
      t3 = cputime;
      disp(sprintf('%4d             %f        %f',SpectFltrLength,(t2-t1)/nRepeats,
        (t3-t2)/nRepeats));
      fflush(stdout);
    end
  end
end
%---------------------------------
function s2 = calcInOctave(channels, b, signals)
  s2 = zeros(channels, channels, size(signals,3));
  for t=1:size(signals,3)
    s = signals(:,:,t);
    for tau=1:numel(b)-1
      s += b(tau+1)*[zeros(channels, tau), signals(:,1:end-tau,t)];
    end
    % The averaging is neccessary because covarianceWithNaN averages too
    s2(:,:,t) = s*s'/columns(signals);
  end
end