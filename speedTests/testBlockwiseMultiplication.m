function testBlockwiseMultiplication(operation)
  channels = 64;
  T = 300;
  nTrials = 250;
  A=randn(channels, T, nTrials);
  %B = randn(T, channels, nTrials);
  classLabel = fix(rand(nTrials,1)+0.5)+1;  % A set of ones and twos
  SpectFltrLength = 40;
  if isequal(operation,'accuracy')
    sigmaTau1 = calcTrainingItemsOctave(A, classLabel, 2, SpectFltrLength);
    sigmaTau3 = nativeCalcSigmaTauForCSSSP(A, classLabel, SpectFltrLength);
    disp(sprintf('Sum of differences is %f',sum(abs(sigmaTau1(:)-sigmaTau3(:)))));
  else
    N = 200;
    t0=cputime;
    fprintf(stdout, ' ');
    for n=1:N
      fprintf(stdout, '%c%c%c%2d',8,8,8,n);
      fflush(stdout);
      nativeCalcSigmaTauForCSSSP(A, classLabel, SpectFltrLength);
    end
    disp ' '
    disp(sprintf('Averaged CPU time is %f',(cputime-t0)/N));
  end
end

function calcMethod1(A,B)
  [channels, T, nTrials] = size(A);
  C = zeros(channels, channels, nTrials);
  for k=1:nTrials
    C(:,:,k) = A(:,:,k)*B(:,:,k);
  end
end

%--------------------------------
function preCalculatedSigmaTau = calcTrainingItemsOctave(signal, classLabel, nClasses, SpectFltrLength)
  % signal : Centralized signals of all channels ant rials
  % classLabel : All class labels
  [channels, T, nTrials] = size(signal);
  lastTau = min(SpectFltrLength-1, T-1);
  preCalculatedSigmaTau = zeros(channels, channels, lastTau+1, nClasses);
  
  for t=1:nTrials
    for tau=0:lastTau
      preCalculatedSigmaTau(:,:,tau+1,classLabel(t)) += signal(:,1:end-tau,t)*signal(:,tau+1:end,t)';
    end % for tau
  end % for each trial
  
  % Generalized transpose of each plane
  preCalculatedSigmaTau += permute(preCalculatedSigmaTau, [2,1,3,4]);
  
  % Averaging
  for y=1:nClasses
    preCalculatedSigmaTau(:,:,:,y) /= nnz(classLabel==y);
  end
  
end
%-------------------------------------------
function preCalculatedSigmaTau = calcTrainingItemsOctave2(signal, classLabel, nClasses, SpectFltrLength)
  [channels, T, nTrials] = size(signal);
  lastTau = min(SpectFltrLength-1, T-1);
  preCalculatedSigmaTau = zeros(channels, channels, nClasses, lastTau+1);
  st = permute(signal,[2 1 3]);
  for k=1:nClasses
    idx = classLabel==k;
    for tau=0:lastTau
      preCalculatedSigmaTau(:,:,k,tau+1) = multiplyAndSumSlices(signal(:,1:end-tau,idx), st(tau+1:end,:,idx));
    end
  end
  
  % Generalized transpose of each plane
  preCalculatedSigmaTau += permute(preCalculatedSigmaTau, [2,1,3,4]);
  
  % Averaging
  for y=1:nClasses
    preCalculatedSigmaTau(:,:,y,:) /= nnz(classLabel==y);
  end
  
  preCalculatedSigmaTau = permute(preCalculatedSigmaTau,[1,2,4,3]);
  
end
%---------------------------

function calcMethod2(A,B)
  % This method is not optimal for large values of T.It is only useful when
  % channels and T are less then 5 and nTrials is very big
  [channels, T, nTrials] = size(A);
  C = zeros(channels, channels, nTrials);
  for j=1:T
    C += bsxfun(@times,A(:,j,:),B(j,:,:));
  end
end

