function testCSP_BBCI_II_3

	% Loading data
  disp 'Loading data...'
  fflush(stdout);
	load '../datasets/bci/bbci.de/BCI-comp-II/set3/dataset.mat'
	% True labels as y_test vector
	load '../datasets/bci/bbci.de/BCI-comp-II/set3/test_labels.mat'
	
  simulation1(x_train, y_train, x_test, y_test);
	
end

function demo(x_train, y_train, x_test, y_test)
  disp 'Preprocessing...'
  fflush(stdout);
  frqs=128;
	winoptions.type=1;
	winoptions.alpha=0.5;
	h = wsibandpass(6*2*pi/frqs, 34*2*pi/frqs, 112, winoptions);
	C=3;
	T=1152;
	window = hamming(T);
  x_train = preprocess(x_train, window, h);
  x_test = preprocess(x_test, window, h);
  
	% Timing starts from here
	timedt=cputime;
	disp 'Training CSP'
  fflush(stdout);
	csp = trainCSP(C, 2,
		@(idx) simpleTrialGenerator(idx, 1:140, x_train, y_train), 140,'', 3, true);
  
  disp 'Training classifier...'
  fflush(stdout);
  D = generatePatternsFromCSP(csp, @(idx) simpleTrialGenerator(idx, 1:140, x_train, y_train), 140);
  mode.TYPE = 'FLDA';
  CC = train_sc(D, y_train, mode);
	% end of timing
  timedt = cputime-timedt;
  
	% Testing CSP
  disp 'Testing CSP and classifier'
  fflush(stdout);
	R=testCSP(csp, @(idx) simpleTrialGenerator(idx, 1:numel(y_test), x_test, []),
		size(x_test, 3), CC, mode, [], y_test(:), true);
	disp(sprintf("Accuracy = %5.2f%%, duration=%fs",R.ACC*100, timedt));
  disp 'Confusion matrix:'
  disp(R.H);
end
%-----------------
function simulation1(x_train, y_train, x_test, y_test)
  frqs=128;
  C=3;
	T=1152;
  % Default values for parameters of preprocessing
  parameters.lowFrq = 6;
  parameters.highFrq = 34;
  parameters.filterLength = 112;
  parameters.firstMoment = 0;
  parameters.lastMoment = 9;
  parameters.winoptions.type=1;
	parameters.winoptions.alpha=0.5;
  % Search for the best value of each parameter
  parameters.firstMoment = simulation1worker(x_train, y_train, x_test, y_test,
    parameters,'firstMoment',[0:0.125:4.5]);
  
  parameters.lastMoment = simulation1worker(x_train, y_train, x_test, y_test,
    parameters,'lastMoment',[parameters.firstMoment+0.125:0.125:9]);
    
  parameters.lowFrq = simulation1worker(x_train, y_train, x_test, y_test,
    parameters,'lowFrq',[1:15]);
  
  parameters.highFrq = simulation1worker(x_train, y_train, x_test, y_test,
    parameters,'highFrq',[parameters.lowFrq+1:40]);
  
  parameters.filterLength = simulation1worker(x_train, y_train, x_test, y_test,
    parameters,'filterLength',[2:2:64]);
  
  % Final testing
  xpp_train = preprocessWithParams(x_train, parameters);
  xpp_test = preprocessWithParams(x_test, parameters);
  csp = trainCSP(C, 2,
	  @(idx) simpleTrialGenerator(idx, 1:140, xpp_train, y_train), 140,'', 3, true);
  D = generatePatternsFromCSP(csp, @(idx) simpleTrialGenerator(idx, 1:140, xpp_train, y_train), 140);
  mode.TYPE = 'FLDA';
  CC = train_sc(D, y_train, mode);
  R=testCSP(csp, @(idx) simpleTrialGenerator(idx, 1:numel(y_test), xpp_test, []),
	  size(xpp_test, 3), CC, mode, [], y_test(:));
  disp 'Best value for parameters:'
  disp(parameters);
  disp 'Confusion matrix:'
  disp(R.H);  
  disp(sprintf("Accuracy = %5.2f%%",R.ACC*100));
end
%-----------------
function paramValue = simulation1worker(x_train, y_train, x_test, y_test, 
  parameters ,parameterName, paramValues)
  % parameters : A struct of determined values for all parameters
  % parameter : A string, name of a parameter to be validated
  % paramValues : A vector of test validation values
  % Returns: The best value for this variable parameter
  allAcc = zeros(size(paramValues));
  C = 3;
  for k=1:numel(paramValues)
    parameters = setfield(parameters, parameterName, paramValues(k));
    xpp_train = preprocessWithParams(x_train, parameters);
    xpp_test = preprocessWithParams(x_test, parameters);
    csp = trainCSP(C, 2,
		@(idx) simpleTrialGenerator(idx, 1:140, xpp_train, y_train), 140, '', 3);
    D = generatePatternsFromCSP(csp, @(idx) simpleTrialGenerator(idx, 1:140, xpp_train, y_train), 140);
    mode.TYPE = 'FLDA';
    CC = train_sc(D, y_train, mode);
    R=testCSP(csp, @(idx) simpleTrialGenerator(idx, 1:numel(y_test), xpp_test, []),
		  size(xpp_test, 3), CC, mode, [], y_test(:));
    allAcc(k) = R.ACC;
    disp(sprintf('%s = %f , Accuracy = %5.2f%%', parameterName, paramValues(k),
      R.ACC*100));
    fflush(stdout);  
   end
   [x,ix] = max(allAcc);
   paramValue = paramValues(ix);
end

%-----------------
function xpp = preprocess(x,window,h)
  C=3;
	T=1152;
  repwin = repmat(window(:), 1, size(x,2));
  xpp = zeros(C,C,size(x,3));
  central=eye(T)-ones(T)/T;
  for idx = 1:size(x,3)
    rawx = filtfilt(h,1,repwin.*x(:,:,idx))'*central; % C by T
    xpp(:,:,idx) = rawx*rawx'/T;  % each plane is C by C
  end
end
%------------------
function xpp = preprocessWithParams(x, params)
% params should be a structure with fields: 'firstMoment','lastMoment','lowFrq',
% 'highFrq' ,'filterLength' and 'winoptions'
  C=3;
  frqs=128;
  % Impulse response of the band-pass filter
  h = wsibandpass(2*pi*params.lowFrq/frqs, 2*pi*params.highFrq/frqs, 
    params.filterLength, params.winoptions);
  % Window
  offset1 = max(1,fix(frqs*params.firstMoment));
  offset2 = fix(frqs*params.lastMoment);
  T = offset2-offset1+1;
  window = hamming(T);
  repwin = repmat(window(:), 1, size(x,2));
  central=eye(T)-ones(T)/T;
  xpp = zeros(C,C,size(x,3));
  for idx = 1:size(x,3)
    rawx = filter(h,1,repwin.*x(offset1:offset2,:,idx))'*central; % C by T
    xpp(:,:,idx) = rawx*rawx'/T;  % each plane is C by C
  end
end
