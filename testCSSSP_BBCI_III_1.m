function testCSSSP_BBCI_III_1
	% Training : 278 trials, 64 channels and 3000 samples in 3 seconds
	% Testing  : 
	frqs=1000;
	C=64;
	T=3000;
	trainingTrials = 278;
	testTrials = 100;
	
	
  disp 'Loading training data...'
  fflush(stdout);
  
	% Will load X and Y for training
	load '../datasets/bci/bbci.de/BCI-comp-III/1/competition_train.mat'
	
  disp 'Preprocessing training data...'
  fflush(stdout);
	Xtrain = downSample(X);
	% Converts -1,1 to 1,2
	Ytrain = (3+Y)/2;
	
	% Will load X for testing
  disp 'Loading testing data...'
  fflush(stdout);
	load '../datasets/bci/bbci.de/BCI-comp-III/1/competition_test.mat'
  disp 'Preprocessing testing data...'
  fflush(stdout);
	Xtest = downSample(X);
	
	% True labels
	[fid,message] = fopen('../datasets/bci/bbci.de/BCI-comp-III/1/true_labels.txt','r');
	if fid==-1
		error(message)
	end
	[trueLabels,c,message]=fscanf(fid,'%d',[size(Y,1),1]);
	fclose(fid);
	if length(message)>0
		disp (message)
	end
	% Converts -1,1 to 1,2
	Ytest = (3+trueLabels)/2;
	
  demo(Xtrain, Ytrain, Xtest, Ytest);
  
end
%---------------------------------------
function XDS = downSample(X)
  frqs=1000;  % Fmax is 500Hz
  T=3000;
  winoptions.type=1;
	winoptions.alpha=0.5;
  downSamplingRate = 10;  % new Fmax will be 50Hz
  % Down-sampling the signal to 100 Hz
	hlow = wsilowpass(pi/downSamplingRate, 112, winoptions);
  XDS = filter(hlow, 1, X, [], 3);
  XDS = XDS(:,:,downSamplingRate/2:downSamplingRate:T);
end

%---------------------------------------
function timePassed = demo(Xtrain, Ytrain, Xtest, Ytest)
  params.firstMoment = 0;
  params.lastMoment = 3;
  Xtrain = preprocess(Xtrain, params);
  Xtest = preprocess(Xtest, params);
  C = 64;
  %time1 = cputime;
  disp 'Training CSSSP'
  fflush(stdout);
  model.SpectFltrLength = 30;
  model.CCModel.TYPE='FLDA';
  model.CCModel.hyperparameter.gamma=0.075;
  model.problemName = 'BBCI-III-1';
  tic
  time1=cputime;
  cssspOptions.verbose=true;
  cssspOptions.parallel=true;
  cssspOptions.optimizationMethod='nonlin_min';
  csssp = trainCSSSP(Xtrain, Ytrain, model, 3, cssspOptions);
  timePassed = cputime-time1;
  disp(sprintf('Total CPU time spent is %f seconds',timePassed));
  toc
  disp 'Training classifier...'
  fflush(stdout);
  D = generatePatternsFromCSSSP(csssp, Xtrain);
  mode.TYPE = 'FLDA';
  
  CC = train_sc(D, Ytrain, mode);
  % Elapsed time is 208.725 seconds.
  % 
  disp 'Testing CSSSP and classifier...'
  fflush(stdout);
  D = generatePatternsFromCSSSP(csssp, Xtest);
  R = test_sc(CC, D, mode, Ytest);
  disp 'Confusion matrix:'
  disp(R.H);
  disp(sprintf('Accuracy is %5.2f%%',R.ACC*100));
  end
%--------------------------------------
function simulation1(Xtrain, Ytrain, Xtest, Ytest)
  
  params.firstMoment = 0;
  params.lastMoment = 3;
  
  params.SpectFltrLength = findBestParameterValue(Xtrain, Ytrain, Xtest, Ytest,
    params, 'SpectFltrLength', 1:1:40, false);
  disp(' ');
  fflush(stdout);
  
  %params.SpectFltrLength = 12;
  
  params.firstMoment = findBestParameterValue(Xtrain, Ytrain, Xtest, Ytest,
    params, 'firstMoment', 1:0.1:2.9, true);
  disp(' ');
  fflush(stdout);  
  
  params.lastMoment = findBestParameterValue(Xtrain, Ytrain, Xtest, Ytest,
    params, 'lastMoment', params.firstMoment+0.2:0.1:3, true);
  disp(' ');
  fflush(stdout);
  
 %horn(7.5); 
end
%---------------------------------------
function bestValue = findBestParameterValue(x_train_org, y_train, x_test_org, 
  y_test, params, name, paramValues, shouldPreprocessEachTime)
  C = 64;
  if ~shouldPreprocessEachTime
    x_train = preprocess(x_train_org, params);
    x_test = preprocess(x_test_org, params);
  end
  accracy = zeros(size(paramValues));
  
  CCModel.TYPE = 'FLDA';
  model.CCModel = CCModel;
  cssspOptions.verbose=false;
  cssspOptions.parallel=true;
  cssspOptions.optimizationMethod='fminunc';
  for k=1:numel(paramValues)
    params = setfield(params, name, paramValues(k));
    model.SpectFltrLength = params.SpectFltrLength;
    if shouldPreprocessEachTime
      x_train = preprocess(x_train_org, params);
      x_test = preprocess(x_test_org, params);
    end
    % training CSSSP
    
    csssp = trainCSSSP(x_train, y_train, model, 3, cssspOptions);
    if ~isempty(csssp.cspinst.W)  
      D = generatePatternsFromCSSSP(csssp, x_train);
      CC = train_sc(D, y_train, model.CCModel);
	    % Testing CSP
	    D = generatePatternsFromCSSSP(csssp, x_test);
      R = test_sc(CC, D, CCModel, y_test);
      accracy(k) = R.ACC;
      disp(sprintf('%s=%f will result in accuracy=%5.2f%%', name, paramValues(k),
        100*R.ACC));
      fflush(stdout);
    end  
  end
  [v,ix] = max(accracy);
  bestValue = paramValues(ix);
end  
%---------------------------------------
function xpp = preprocess(X, params)
	% X : A three dimensional matrix with L rows, C columns and T planes
	% params : a struct with fields firstMoment ann lastMoment
	C = size(X,2);
  frqs = 100; % After down sampling
	% Windowing
  firstSample = max(fix(params.firstMoment*frqs), 1);
  lastSample = min(fix(params.lastMoment*frqs), size(X,3));
  T = lastSample-firstSample+1;
  window = hamming(T);
	if isOctave
    % Uses broadcasting to ignore repeating values
    X = X(:,:,firstSample:lastSample) .* reshape(window, [1, 1, numel(window)]);
  else
    X = X(:,:,firstSample:lastSample) .* repmat(reshape(window, [1, 1, numel(window)]), size(X,1), size(X,2));
	end
  % centering and taking the second moment
  xpp = zeros(C, T, size(X,1));
  
  if isOctave
    xpp = permute(X,[2,3,1]);
    xpp -= mean(xpp,2);
  else
    for trial=1:size(X,1)
      xpp(:, :, trial) = squeeze(X(trial,:,:));
    end
    xpp = xpp - repmat(mean(xpp,2),[1,T,1]);
  end
end
%-------------------