function testCSP_BBCI_III_3b
  frqs=125;	% Hz
	C=2;		% Channels
	T=5*frqs;		% Time samples after the visual cue
	subject={'O3VR','S4b','X11b'};
	
  headers = cell(1,3);
  signals = cell(1,3);
  trueLabels = cell(1,3);
  for k=1:3
	% Will load HDR and s for training and testing for each subject
  disp(sprintf('Loading data for subject %d : %s',k,subject{k}));
  fflush(stdout);
	load(sprintf('../datasets/bci/bbci.de/BCI-comp-III/3/b/%s.mat', subject{k}))
  
	
  % True labels
	[fid,message] = fopen(sprintf('../datasets/bci/bbci.de/BCI-comp-III/3/b/true_labels_%s.txt',subject{k}),'r');
	if fid==-1
		error(message)
	end
  testTrials = sum(isnan(HDR.Classlabel));
	[trueLabels{k},c,message]=fscanf(fid,'%d',[testTrials,1]);
	fclose(fid);
	if length(message)>0
		disp (message)
	end
  
	signals{k} = s;
  headers{k}=HDR;
end   % for k

simulation1(headers, signals, trueLabels);

end

%---------------------------------------------
function demo1(HDR, signals, trueLabels)
  % Tests with voting binary classifiers for each pair of classes
  % HDR : Cell array of eaders
  % signals : Cell array of autocorrelations of signals
  % trueLabels : Cell array containing sets of true labels
  C=2;		% Channels
  allacc=zeros(1,3);		% One for each subject
  subject={'O3VR','S4b','X11b'};
  opts.numSelectedFilters = 2;
  CCmodel.TYPE = 'FLDA';
  for k=1:3
    disp(['Subject ',subject{k}]);
    
    %s = preprocess(signals{k}, HDR{k}.TRIG);
    s = preprocessWithParams(signals{k}, HDR{k}.TRIG, 1.5, 5.5, 0.5, 8, 30, 32);
    
    [trainingIndex, trainingTrials, testingIndex, testTrials, nonzeroTestLabels] = calcIndices(s, HDR{k});
    
    countOfTestTrials(k) = testTrials;
    disp 'Training CSP...'
    fflush(stdout);
    
	  cspinst = trainCSP(C,2,@(idx) simpleTrialGenerator(idx, trainingIndex, s, 
      HDR{k}.Classlabel), trainingTrials, 'conventional', 
      opts.numSelectedFilters, false);
    
    disp 'Training classifiers...'
    fflush(stdout);
    %TODO from here
    D = generatePatternsFromCSP(cspinst, @(idx) simpleTrialGenerator(idx, 
      trainingIndex, s, HDR{k}.Classlabel), trainingTrials, 'training');
    CC = train_sc(D, HDR{k}.Classlabel(trainingIndex), CCmodel);
    
    disp 'Testing CSP and classifiers...'
    fflush(stdout);
    R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, testingIndex,
      s, HDR{k}.Classlabel), testTrials, CC, CCmodel, [], 
      trueLabels{k}(nonzeroTestLabels), true);
    
    disp ' '
    disp 'Total confusion matrix:'
    disp(R.H);
    disp(sprintf('Total accuracy for this subject is %5.2f%%',R.ACC*100));
    allacc(k) = R.ACC;
  end
  disp(sprintf('Average accuracy = %5.2f%%',mean(allacc)*100));
end
%---------------------------------------------
function [trainingIndex, trainingTrials, testingIndex, testTrials, nonzeroTestLabels] = calcIndices(s2, HDR)
  % Indices of labels of test trials with non-zero covariance
  [nonzeroTestLabels, allzero] = findNonzeroTestLabels(s2, HDR.Classlabel);
  % Other indices
  [trainingIndex, trainingTrials, testingIndex, testTrials] = calcIndicesPart2(HDR, allzero);
    
end
%---------------------------------------------
function [trainingIndex, trainingTrials, testingIndex, testTrials] = calcIndicesPart2(HDR,allzero)
  trainingIndex = find(~isnan(HDR.Classlabel) & ~HDR.ArtifactSelection & ~allzero);
	testingIndex = find(isnan(HDR.Classlabel) & ~allzero);
	trainingTrials = numel(trainingIndex);
	testTrials = numel(testingIndex);
end
%---------------------------------------------
function [nonzeroTestLabels, allzero] = findNonzeroTestLabels(s2, classLabels)
  % Ignore trials with all-zero covariance
  allzero=false(size(classLabels));
  for t=1:numel(classLabels)
    COV = s2(:,:,t);
    allzero(t)=all(COV(:)==0);
  end
  nonzeroTestLabels = ~allzero(isnan(classLabels));
end

%---------------------------------------------
function simulation1(HDR, signals, trueLabels)
  subject={'O3VR','S4b','X11b'};
  allacc=zeros(1,3);		% One for each subject
  for k=1:3
    disp(['Subject ',subject{k}]);
    fflush(stdout);
    allacc(k) = simulation1sub(HDR{k}, signals{k}, trueLabels{k}, true);
  end
  disp(sprintf('Average accuracy = %5.2f%%',mean(allacc)*100));
end
%---------------------------------------------
function acc = simulation1sub(HDR, signals, trueLabels, verbose)
  % TODO Find best value for parametters of preprocessing and training CSP independently
  % HDR : headers for a subject
  % signals : raw signal for a subject
  % trueLabels : Vector of true labels for this subject
  
  if nargin<4
    verbose = false;
  end
  
  %Default values for parametters
  ppparams.lastMoment = 5.5;
  ppparams.winoptions_alpha = 0.5;
  ppparams.lowFrq = 8;
  ppparams.highFrq = 30;
  ppparams.filterLength = 32;
  
  CCmodel.TYPE = 'FLDA';
    
  % first moment
  ppparams.firstMoment = findParameterByValidation(HDR, signals, 0.25:0.125:3,
    'firstMoment', ppparams, trueLabels, verbose);
      
  % last moment
  ppparams.lastMoment = findParameterByValidation(HDR, signals, 
    ppparams.firstMoment+0.125:0.125:6.5, 'lastMoment', ppparams, trueLabels, verbose);
  
  % alpha for window signal
  ppparams.winoptions_alpha = findParameterByValidation(HDR, signals, 0.48:0.01:0.54,
    'winoptions_alpha', ppparams, trueLabels, verbose);
 
  % lowFrq
  ppparams.lowFrq = findParameterByValidation(HDR, signals, 6:15,
    'lowFrq', ppparams, trueLabels, verbose);
  
  % highFrq
  ppparams.highFrq = findParameterByValidation(HDR, signals, ppparams.lowFrq+1:38,
    'highFrq', ppparams, trueLabels, verbose);
  
  %filterLength
  ppparams.filterLength = findParameterByValidation(HDR, signals, 8:2:40,
    'filterLength', ppparams, trueLabels, verbose);
  
  % Final testing
  if verbose
    disp(sprintf('selected firstMoment=%f',ppparams.firstMoment));
    disp(sprintf('selected lastMoment=%f',ppparams.lastMoment));
    disp(sprintf('selected alpha=%f',ppparams.winoptions_alpha));
    disp(sprintf('selected lowFrq=%f',ppparams.lowFrq));
    disp(sprintf('selected highFrq=%f',ppparams.highFrq));
    disp(sprintf('selected filterLength=%f',ppparams.filterLength));
    fflush(stdout);
  end
  
  if verbose
    acc = preprocessAndTest(HDR, signals, trueLabels, ppparams);
    disp(sprintf('Final accuracy on test trials is %5.2f%%',acc*100));
  end
  
end
%---------------------------------------------
function parameter = findParameterByValidation(HDR, signals, paramValues,
    name, ppparams, trueLabels, verbose)
  validationAcc = zeros(size(paramValues));
  for k=1:numel(paramValues)
    % validation for this value of firstMoment and default value for other parameters
    ppparams = setfield(ppparams, name, paramValues(k));
    validationAcc(k) = preprocessAndTest(HDR, signals, trueLabels, ppparams);
    if verbose
      disp(sprintf('Validation for %s=%f, acc=%5.2f%%', name, paramValues(k),
      validationAcc(k)*100));
      fflush(stdout);
    end
  end
  [v,ix] = max(validationAcc);
  parameter = paramValues(ix);
  if verbose
    disp ' '
  end
end  
%---------------------------------------------
function acc = preprocessAndValidate(HDR, signals,
  firstMoment, lastMoment, winoptions_alpha, lowFrq, highFrq, filterLength)
  
  % Preprocess with the given parameters
  s2 = preprocessWithParams(signals, HDR.TRIG, 
  firstMoment, lastMoment, winoptions_alpha, lowFrq, highFrq, filterLength);
  
  % Indices 
  [trainingIndex, orgTrainingTrials] = calcIndices(s2, HDR);
  
  % Divide training data to pure training and validation sets
  divisionRate = 3;
  validationSize = fix(orgTrainingTrials/divisionRate);
  trainingSize = orgTrainingTrials-validationSize;
  sumacc = 0;
  CCmodel.TYPE = 'FLDA';
  for d=1:divisionRate
    validationSubset = trainingIndex((d-1)*validationSize+1:d*validationSize);
    %if d==1
    %  trainingSubset = trainingIndex(validationSize+1:end);
    %elseif d==divisionRate
    %  trainingSubset = trainingIndex(1:end-validationSize);
    %else
    trainingSubset = trainingIndex([1:(d-1)*validationSize, d*validationSize+1:orgTrainingTrials]);
    % For each division, train on the training subset and test on the validation 
    % subset and average all accuracies
    cspinst = trainCSP(2,2,@(idx) simpleTrialGenerator(idx, trainingSubset, s2, 
      HDR.Classlabel), numel(trainingSubset), 'conventional', 
      2, false);
    
    D = generatePatternsFromCSP(cspinst, @(idx) simpleTrialGenerator(idx, 
      trainingSubset, s2, HDR.Classlabel), numel(trainingSubset), 'training');
    
    CC = train_sc(D, HDR.Classlabel(trainingSubset), CCmodel);
    
    R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, validationSubset,
      s2, HDR.Classlabel), numel(validationSubset), CC, CCmodel, [], 
      HDR.Classlabel(validationSubset));
    
    sumacc = sumacc+R.ACC;
  end % for d
  acc = sumacc/divisionRate;
end

function acc = preprocessAndTest(HDR, signals, trueLabels, ppparams)
  
  % Preprocess with the given parameters
  s2 = preprocessWithParams(signals, HDR.TRIG, 
  ppparams.firstMoment, ppparams.lastMoment, ppparams.winoptions_alpha, 
  ppparams.lowFrq, ppparams.highFrq, ppparams.filterLength);
  
  % Indices 
  [trainingIndex, trainingTrials, testingIndex, testTrials, nonzeroTestLabels] = calcIndices(s2, HDR);
  
  CCmodel.TYPE = 'FLDA';
  cspinst = trainCSP(2,2,@(idx) simpleTrialGenerator(idx, trainingIndex, s2, 
      HDR.Classlabel), trainingTrials, 'conventional', 
      2, false);
    
    D = generatePatternsFromCSP(cspinst, @(idx) simpleTrialGenerator(idx, 
      trainingIndex, s2, HDR.Classlabel), trainingTrials, 'training');
    
    CC = train_sc(D, HDR.Classlabel(trainingIndex), CCmodel);
    
    R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, testingIndex,
      s2, HDR.Classlabel), testTrials, CC, CCmodel, [], 
      trueLabels(nonzeroTestLabels));
      
    acc = R.ACC;
end
%---------------------------------------------
function signal2 = preprocess(signal1, trigger)
  % Returns a 3D array with C times C times N elements
	%Torg = 1000;  % Original T
  C=2;		% Channels
  frqs=125;	% Hz
  % From 1 to 3 seconds after the cue, or 2 to 5 seconds after the trigger
  firstSample = fix(frqs*1.5);
  lastSample = fix(frqs*5.5);  
  T = lastSample-firstSample+1;
  %h = butter(5, 2*[8,20]/frqs);
  winoptions.type=1;
	winoptions.alpha=0.5;
	h = wsibandpass(8*2*pi/frqs, 30*2*pi/frqs, 32, winoptions);
	window = repmat(hamming(T), 1, C);
	signal2 = zeros(C,C,numel(trigger));
  central = eye(T)-ones(T)/T;
	for n=1:numel(trigger)
    startRow = trigger(n)+firstSample-1;
    temp = covarianceWithNaN(filter(h, 1, signal1(startRow:startRow+T-1,:).*window)' * central);
    %if all(temp(:)==0)
    %  error 'All zero covariance!'
    %end
    signal2(:,:,n) = temp;
	end
	if all(signal2(:)==0)
		error "Signals after preprocessing are all zero"
	%else
	%	disp "Healthy preprocessed signal:"
	%	disp(signal(1:20,1:8))
	end
end

function signal2 = preprocessWithParams(signal1, trigger, firstMoment, lastMoment,
    winoptions_alpha, lowFrq, highFrq, filterLength)
  % Returns a 3D array with C times C times N elements
	%Torg = 1000;  % Original T
  C=2;		% Channels
  frqs=125;	% Hz
  % From 1 to 3 seconds after the cue, or 2 to 5 seconds after the trigger
  firstSample = fix(frqs*firstMoment);
  lastSample = fix(frqs*lastMoment);
  T = lastSample-firstSample+1;
  %h = butter(5, 2*[8,20]/frqs);
  winoptions.type=1;
	winoptions.alpha=winoptions_alpha;
	h = wsibandpass(lowFrq*2*pi/frqs, highFrq*2*pi/frqs, filterLength, winoptions);
	window = repmat(hamming(T), 1, C);
	signal2 = zeros(C,C,numel(trigger));
  central = eye(T)-ones(T)/T;
	for n=1:numel(trigger)
    startRow = trigger(n)+firstSample-1;
    temp = covarianceWithNaN(filter(h, 1, signal1(startRow:startRow+T-1,:).*window)' * central);
    %if all(temp(:)==0)
    %  error 'All zero covariance!'
    %end
    signal2(:,:,n) = temp;
	end
	if all(signal2(:)==0)
		error "Signals after preprocessing are all zero"
	%else
	%	disp "Healthy preprocessed signal:"
	%	disp(signal(1:20,1:8))
	end
end


