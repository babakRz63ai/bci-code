function testCSP_BBCI_III_3a
  frqs=250;	% Hz
	C=60;		% Channels
	T=1000;		% Time samples after the visual cue
	subject={'k3b','k6b','l1b'};
	
  headers = cell(1,3);
  signals = cell(1,3);
  trueLabels = cell(1,3);
  for k=1:3
	% Will load HDR and s for training and testing for each subject
  disp(sprintf('Loading data for subject %d : %s',k,subject{k}));
  fflush(stdout);
	load(sprintf('../datasets/bci/bbci.de/BCI-comp-III/3/a/%s.mat', subject{k}))
  
	
  % True labels
	[fid,message] = fopen(sprintf('../datasets/bci/bbci.de/BCI-comp-III/3/a/true_labels_%s.txt',subject{k}),'r');
	if fid==-1
		error(message)
	end
  testTrials = sum(isnan(HDR.Classlabel));
	[trueLabels{k},c,message]=fscanf(fid,'%d',[testTrials,1]);
	fclose(fid);
	if length(message)>0
		disp (message)
	end
  
  %disp 'Preprocessing signal...'
  fflush(stdout);
	signals{k} = s; % for preprocessing with single configuration: signals{k} = preprocess(s, HDR.TRIG);
  headers{k}=HDR;
end   % for k

%We have performaed a simulation and some best values for parameters are known 
% for each subject
ppp = cell(1,3);
opts.firstMoment = 1;
opts.lastMoment = 3.125;
opts.filterOrder = 16;
ppp{1} = opts;

opts.firstMoment = 1.375;
opts.lastMoment = 2.5;
opts.filterOrder = 16;
ppp{2} = opts;

opts.firstMoment = 2.375;
opts.lastMoment = 2.5;
opts.filterOrder = 14;
ppp{3} = opts;

  
demo2(headers, signals, trueLabels);
%for k=3
%  for classPairIndex=2:6
%    disp(sprintf('Subject %s, pair index:%d', subject{k}, classPairIndex));
%    fflush(stdout);
%    variantPreprocessBin1(headers{k}, signals{k}, trueLabels{k}, classPairIndex);
%  end
%end
end

%---------------------------------------------
function worker2sim(HDR, signals, trueLabels, verbose)
   % Uses subfunctions of CSP to speedup the simulations
   C=60;		% Channels
	 T=1000;		% Time samples after the visual cue
   results = cell(1,32);
   resPerSubject = cell(3,32);
   for k=1:3
     numResults = 0;
     cspinst = cspn(C,T,4,'CNV.SIM');
     trainingIndex = find(~isnan(HDR{k}.Classlabel) & ~HDR{k}.ArtifactSelection);
     testingIndex = find(isnan(HDR{k}.Classlabel));
	   trainingTrials = numel(trainingIndex);
	   testTrials = numel(testingIndex);
     countOfTestTrials(k) = testTrials;
     disp 'Training CSP...'
     fflush(stdout);
     cspinst = CSPaddTrial(cspinst, trainingTrials, @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
      HDR{k}.Classlabel));
     numResults=0;
     disp 'Simultaneous diagonalization...'
     fflush(stdout);
     [V,COV] = simDiagonalize(covarianceMatrices(cspinst),1e-6,verbose);
     if verbose
       disp(sprintf('Quality of diagonalization is %f',diagonalQuality(COV)));
       fflush(stdout);
     end
     
     for alpha=1
     [r,c,ix] = getRCIXforSimDiag(COV,alpha);
     %calculate all possible spatial filters and then select some of them each time
     for selectedFilters=2:10
       cspinst.W = selectSpatialFiltersForSimDiag(r,c,ix,C,4,selectedFilters,V,verbose);
       cspinst.selectedFilters = selectedFilters;
       model.TYPE = 'FLDA';
       for g=0.05:0.01:0.1
       model.hyperparameter.gamma=g; % TODO change it and test
       disp(sprintf('Training classifiers with gamma=%f ...',g))
       fflush(stdout);
       CC = train_sc(generatePatternsFromCSP(cspinst, 
        @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
        HDR{k}.Classlabel), 
        trainingTrials, 'training'), HDR{k}.Classlabel(trainingIndex), model);
       disp 'Testing...'
       fflush(stdout);
       %R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
       % HDR{k}.Classlabel),
       % numel(trueLabels{k}), CC, model, [], trueLabels{k});
       R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, testingIndex, signals{k},[]),
        testTrials, CC, model, [], trueLabels{k}); % It is not verbose 
       % Saving results 
       numResults=numResults+1;
       res.acc = R.ACC;
       res.kappa = R.kappa;
       res.selectedFilters = selectedFilters;
       res.alpha = alpha;
       res.gamma = g;
       resPerSubject{k,numResults} = res;
     end  % for g
     end  % for selectedFilters
     end % for alpha (parameter for sorting eigenvalues of diagonalized covariances)
   end  % for k
   % Averaging
   for n=1:numResults
     sumACC=0;
     sumKappa=0;
     res = resPerSubject{1,n};
     for k=1:3
       sumACC = sumACC+resPerSubject{k,n}.acc*countOfTestTrials(k);
       sumKappa = sumKappa + resPerSubject{k,n}.kappa*countOfTestTrials(k);;
     end  %for k
     res.acc = sumACC/sum(countOfTestTrials);
     res.kappa = sumKappa/sum(countOfTestTrials);
     results{n} = res;
   end  %for n
  
  % Results of the simulation
  disp 'Alpha  Filters   Gamma   Acc%    kappa';                       
  for k2=1:numResults
  disp(sprintf('%4.2f  %5d   %8.3f  %7.2f %9.4f',
    results{k2}.alpha, results{k2}.selectedFilters, results{k2}.gamma,
    results{k2}.acc*100, results{k2}.kappa));
  end

end

%---------------------------------------------
function [results,numResults] = worker1(headers, signals, trueLabels)
    % Calls the worker in a set of nested loops
extModels={'CNV.BIN','OVR.CSP4','CNV.CSP0','CNV.CSP3'};
results = cell(1,32);
k=1;
opts.gamma = 0.075;
for modelIndex=1
  opts.extensionModel = extModels{modelIndex};
  for numSelected = 2:9
    opts.numSelectedFilters=numSelected;
    for balanced=0:0
      opts.balancedTraining = balanced==1;
      for normalized=0:0
        opts.normalize = normalized==1;
        [opts.avgacc,opts.avgkappa] = worker1sub(headers, signals, trueLabels, opts);
        results{k} = opts;
        k=k+1;
      end
    end
  end
end
numResults = k-1
% Results of the simulation
disp '--Model-  filters  balanced  normalized  Acc%   kappa';                           
for k2=1:numResults
  disp(sprintf('%8s%5d%10d%12d%10.2f%9.4f',
    results{k2}.extensionModel, results{k2}.numSelectedFilters, results{k2}.balancedTraining,
    results{k2}.normalize, results{k2}.avgacc, results{k2}.avgkappa));
end

end
%---------------------------------------------
function [avgacc,avgkappa] = worker1sub(HDR, signals, trueLabels, opts)
  % worker(HDR, signal, trueLabels, opts)
  %  HDR : A cell array of HDR structs
  %  signals : A cell array of signals of all subjects
  % trueLabels : A Cell array of true labels for each subject
  %  opts : A struct containing these fields:
  %   extensionModel : One of 'OVR','OVR.CSP0', 'OVR.CSP3' or 'SIM'
  %   numSelectedFilters : number of selected spatial filters for each class
  %   gamma          : A scalar used for training classifiers
  %   balancedTraining : boolean
  %   normalize        : boolean, indicates testing should be done using normalized discriminants
  
  frqs=250;	% Hz
	C=60;		% Channels
	T=1000;		% Time samples after the visual cue
  subject={'k3b','k6b','l1b'};
  
	estimatedKappa=zeros(1,3);		% One for each subject
  allacc=zeros(1,3);		% One for each subject
  countOfTestTrials=zeros(1,3);
	totalTime=0;
	
	for k=1:3
	moment = cputime;
	
  trainingIndex = find(~isnan(HDR{k}.Classlabel) & ~HDR{k}.ArtifactSelection);
	testingIndex = find(isnan(HDR{k}.Classlabel));
	trainingTrials = numel(trainingIndex);
	testTrials = numel(testingIndex);
  countOfTestTrials(k) = testTrials;
  disp 'Training CSP...'
  fflush(stdout);
  
	%csp = trainCSP(C,T,4,@(idx) mytrialGenerator(idx, signals{k}, trainingIndex, HDR{k}.TRIG, HDR{k}.Classlabel), 
  %trainingTrials, opts.extensionModel, opts.numSelectedFilters, true);
  csp = trainCSP(C,T,4,@(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
      HDR{k}.Classlabel), trainingTrials, opts.extensionModel, 
      opts.numSelectedFilters, true);
  disp 'Training classifiers...'
  fflush(stdout);
  model.TYPE = 'FLDA';
  model.hyperparameter.gamma=opts.gamma;
  if strcmpi(opts.extensionModel,'CNV.BIN')
    % TODO
  elseif strncmpi(opts.extensionModel,'CNV',3)  % Conventional multi class mode
    CC = train_sc(generatePatternsFromCSP(csp, 
      @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, HDR{k}.Classlabel), 
      trainingTrials, 'training'), HDR{k}.Classlabel(trainingIndex), model);
  elseif strncmpi(opts.extensionModel,'OVR',3)
    ovrmodel.fairTrain = opts.balancedTraining;
    [CC,extra] = trainOVRClassifiers(generatePatternsFromCSP(csp, 
      @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, HDR{k}.Classlabel), 
      trainingTrials, 'training'),
    HDR{k}.Classlabel(trainingIndex), model, ovrmodel);
    %disp 'extra is:'
    %disp(extra)
  else
    error 'Other extension models are not implemented'
  end
  timePassed = cputime-moment;
  
  disp 'Testing...'
  fflush(stdout);
  if ~opts.normalize
    extra=[];
  end
	R = testCSP(csp, @(idx) simpleTrialGenerator(idx, testingIndex, signals{k},[]),
    testTrials, CC, model, [], trueLabels{k}); % It is not verbose
	
  % A matrix with 4 columns and one row for each time step
  estimatedKappa(k) = R.kappa;
  allacc(k) = R.ACC;
	totalTime = totalTime+timePassed;
	disp(sprintf('Accuracy for subject %s is %f%%, kappa=%f, time=%fs',
  subject{k}, 100*allacc(k), estimatedKappa(k), timePassed));
  %disp 'Confusion matrix is:'
  %disp(R.H);
  %disp 'Accuracy per class is:'
  %disp(X.sACC)
  %fflush(stdout);
end 	% for k
avgacc = 100*sum(allacc.*countOfTestTrials)/sum(countOfTestTrials);
avgkappa = sum(estimatedKappa.*countOfTestTrials)/sum(countOfTestTrials);
	disp(sprintf('Average Accuracy=%f%%, average kappa is %f, total time=%fs',
  avgacc,avgkappa, totalTime));
fflush(stdout);  
end

%---------------------------------------------
function demo1(HDR, signals, trueLabels)
  % HDR : Cell array of eaders
  % signals : Cell array of autocorrelations of signals
  % trueLabels : Cell array containing sets of true labels
  C=60;		% Channels
	T=1000;		% Time samples after the visual cue
  estimatedKappa=zeros(1,3);		% One for each subject
  allacc=zeros(1,3);		% One for each subject
  countOfTestTrials=zeros(1,3);
  subject={'k3b','k6b','l1b'};
  opts.numSelectedFilters = 2;
  opts.extensionModel = 'CNV.SIM';
  for k=1:3
    disp(['Subject ',subject{k}]);
    trainingIndex = find(~isnan(HDR{k}.Classlabel) & ~HDR{k}.ArtifactSelection);
	  testingIndex = find(isnan(HDR{k}.Classlabel));
	  trainingTrials = numel(trainingIndex);
	  testTrials = numel(testingIndex);
    countOfTestTrials(k) = testTrials;
    disp 'Training CSP...'
    fflush(stdout);
	  csp = trainCSP(C,T,4,@(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
      HDR{k}.Classlabel), trainingTrials, opts.extensionModel, 
      opts.numSelectedFilters, true);
  
    disp 'Training classifiers...'
    fflush(stdout);
    ovrmodel.fairTrain = false;
    model.TYPE = 'FLDA';
    %[CC,extra] = trainOVRClassifiers(generatePatternsFromCSP(csp, 
    %  @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, HDR{k}.Classlabel), 
    %  trainingTrials, 'training'), HDR{k}.Classlabel(trainingIndex), model, 
    %  ovrmodel);
    CC = train_sc(generatePatternsFromCSP(csp, 
      @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, HDR{k}.Classlabel), 
      trainingTrials, 'training'), HDR{k}.Classlabel(trainingIndex), model);
    disp 'Testing...'
    fflush(stdout);
    R = testCSP(csp, @(idx) simpleTrialGenerator(idx, testingIndex, signals{k},[]),
    testTrials, CC, model, [], trueLabels{k}, true);
    disp 'Confusion matrix is'
    disp(R.H);
    disp ' '
    %disp 'Binary classifier       Accuracy%'
    %for c=1:4
    %  disp(sprintf('  %d                 %5.2f%%',c,100*R.accPerBinClass(c)));
    %end
    
    disp(sprintf('Accuracy is %5.2f%% , kappa is %f', R.ACC*100, R.kappa));
    allacc(k) = R.ACC;
    estimatedKappa(k) = R.kappa;
    countOfTestTrials(k) = testTrials;
  end
  disp(sprintf('Average accuracy is %5.1f%% , average kappa is %f',
    100*sum(allacc.*countOfTestTrials)/sum(countOfTestTrials) , 
    sum(estimatedKappa.*countOfTestTrials)/sum(countOfTestTrials)));
end

%---------------------------------------------
function demo2(HDR, signals, trueLabels)
  % Tests with voting binary classifiers for each pair of classes
  % HDR : Cell array of eaders
  % signals : Cell array of autocorrelations of signals
  % trueLabels : Cell array containing sets of true labels
  C=60;		% Channels
  estimatedKappa=zeros(1,3);		% One for each subject
  allacc=zeros(1,3);		% One for each subject
  countOfTestTrials=zeros(1,3);
  subject={'k3b','k6b','l1b'};
  opts.numSelectedFilters = 3;
  opts.extensionModel = 'CNV.BIN';
  CCmodel.TYPE = 'FLDA';
  for k=1:3
    disp(['Subject ',subject{k}]);
    [trainingIndex, trainingTrials, testingIndex, testTrials] = calcIndices(HDR{k});
    countOfTestTrials(k) = testTrials;
    disp 'Training CSP...'
    fflush(stdout);
	  cspinst = trainCSP(C,4,@(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
      HDR{k}.Classlabel), trainingTrials, opts.extensionModel, 
      opts.numSelectedFilters, false);
    
    disp 'Training classifiers...'
    fflush(stdout);
    CC = trainBinClassifiers(cspinst, @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
      HDR{k}.Classlabel), trainingTrials, CCmodel, true);
    
    disp 'Testing CSP and classifiers...'
    fflush(stdout);
    R = testBinClassifiers(cspinst, @(idx) simpleTrialGenerator(idx, 
      testingIndex, signals{k}, HDR{k}.Classlabel), testTrials, CC, CCmodel,
      trueLabels{k});
    disp ' '
    disp 'Pair      Accuracy'
    pairGenerator = [1 2; 1 3; 1 4; 2 3; 2 4; 3 4];
    for pair=1:6
      disp(sprintf('(%d,%d)      %5.2f%%', pairGenerator(pair,1),
      pairGenerator(pair,2), R.Rbin{pair}.ACC*100));
    end
    disp ' '
    disp 'Total confusion matrix:'
    disp(R.H);
    disp(sprintf('Total accuracy for this subject is %5.2f%%',R.ACC*100));
    disp(sprintf('Total kappa for this subject is %f',R.kappa));
    estimatedKappa(k) = R.kappa;
  end
  disp(sprintf('Total kappa is %f',mean(estimatedKappa)));
end
%---------------------------------------------
% TODO use classPairIndex to restrict indices
function [trainingIndex, trainingTrials, testingIndex, testTrials] = calcIndices(header)
  trainingIndex = find(~isnan(header.Classlabel) & ~header.ArtifactSelection);  
  testingIndex = find(isnan(header.Classlabel));
  trainingTrials = numel(trainingIndex);
	testTrials = numel(testingIndex);
end
%---------------------------------------------
function variantPreprocessBin1(HDR, rawSignals, trueLabels, classPairIndex)
  % Training and testing binary classifiers for classes 1,2 with preprocessing
  % optimized for one subject
  C=60;		% Channels
  estimatedKappa=zeros(1,3);		% One for each subject
  allacc=zeros(1,3);		% One for each subject
  countOfTestTrials=zeros(1,3);
  subject={'k3b','k6b','l1b'};
  CSPopts.numSelectedFilters = 3;
  CSPopts.extensionModel = 'CNV.BIN';
  CCmodel.TYPE = 'FLDA';
  % Default values
  parameters.firstMoment = 0.5;
  parameters.lastMoment = 2.5;
  parameters.filterOrder = 16;
  parameters.lowFrq = 8;
  parameters.highFrq = 30;
  
  % FIXME use classPairIndex to restrict indices
  [trainingIndex, trainingTrials, testingIndex, testTrials] = calcIndices(HDR);
  
  % first moment
  parameters.firstMoment = variantPreprocessBin1worker(HDR, rawSignals, 
    trueLabels, parameters, 'firstMoment', 0:0.125:2.375, trainingIndex, 
    trainingTrials, testingIndex, testTrials, classPairIndex, true);
  disp ' '
  
  parameters.lastMoment = variantPreprocessBin1worker(HDR, rawSignals, 
    trueLabels, parameters, 'lastMoment', parameters.firstMoment+0.125:0.125:4, 
    trainingIndex, trainingTrials, testingIndex, testTrials, classPairIndex, true);
  disp ' '
  
  parameters.filterOrder = variantPreprocessBin1worker(HDR, rawSignals, 
    trueLabels, parameters, 'filterOrder', 4:2:40, 
    trainingIndex, trainingTrials, testingIndex, testTrials, classPairIndex, true);
  disp ' '
  
  % Limits of the frequency band
  parameters.lowFrq = variantPreprocessBin1worker(HDR, rawSignals, 
    trueLabels, parameters, 'lowFrq', 1:20, 
    trainingIndex, trainingTrials, testingIndex, testTrials, classPairIndex, true);
  disp ' '
  
  parameters.highFrq = variantPreprocessBin1worker(HDR, rawSignals, 
    trueLabels, parameters, 'highFrq', parameters.lowFrq+1:40, 
    trainingIndex, trainingTrials, testingIndex, testTrials, classPairIndex, true);
  disp ' '
  
  % Final test  
  acc = preprocessAndValidateBin1(HDR, rawSignals, parameters, trueLabels,
    CSPopts, trainingIndex, trainingTrials, testingIndex, testTrials, classPairIndex);
  
  %if verbose
    disp ' '
    disp 'Best parameters for this subject:'
    disp(sprintf('firstMoment=%f',parameters.firstMoment));
    disp(sprintf('lastMoment=%f',parameters.lastMoment));
    disp(sprintf('filterOrder=%d',parameters.filterOrder));
    disp(sprintf('lowFrq=%d',parameters.lowFrq));
    disp(sprintf('highFrq=%d',parameters.highFrq));
    disp(sprintf('Final accuracy is %5.2f%%', acc*100));
  %end
  
end
%---------------------------------------------
function parameter = variantPreprocessBin1worker(HDR, rawSignals, trueLabels, 
  parameters, name, paramValues, trainingIndex, trainingTrials, testingIndex, 
  testTrials, classPairIndex, verbose)
% HDR : Headers of data of a subject
% rawSignals : raw signals of a subject
% trueLabels : vactor of true labels
% parameters : A struct containing fields 'firstMoment', 'lastMoment',
% 'filterOrder', 'lowFrq', 'highFrq'
% name : String, name of the variable parameter
% paramValues : Vector of possible values
% trainingIndex, trainingTrials, testingIndex, testTrials : indices and their counts
% verbose : boolean, controls verbosity
  opts.numSelectedFilters = 3;
  opts.extensionModel = 'CNV.BIN';
  CCmodel.TYPE = 'FLDA';
  validationAcc = zeros(size(paramValues));
  for k=1:numel(paramValues)
    % validation for this value of the parameter and default value for other parameters
    parameters = setfield(parameters,name,paramValues(k));
    validationAcc(k) = preprocessAndValidateBin1(HDR, rawSignals, parameters, 
    trueLabels, opts, trainingIndex, trainingTrials, testingIndex, testTrials,
    classPairIndex);
    if verbose
      disp(sprintf('Validation for %s=%d, acc=%5.2f%%', name, paramValues(k),
      validationAcc(k)*100));
      fflush(stdout);
    end
  end
  [acc,ix] = max(validationAcc);
  parameter = paramValues(ix);
end  
%---------------------------------------------
function variantPreprocessBin2(HDR, rawSignals, trueLabels, ppp, verbose)
  % Training and testing binary classifiers for classes 1,2 with preprocessing
  % optimized for one subject
  C=60;		% Channels
  
  countOfTestTrials=zeros(1,3);
  opts.numSelectedFilters = 3;
  opts.extensionModel = 'CNV.BIN';
  CCmodel.TYPE = 'FLDA';
  
  [trainingIndex, trainingTrials, testingIndex, testTrials] = calcIndices(HDR);
   
  % Limits of the frequency band
  numIters = (20-5+1)*35-sum(5:20);
  validationAcc = zeros(numIters,1);
  paramValues = zeros(numIters, 2);
  k=1;
  for lowFrq = 5:20
    paramValues(k,1) = lowFrq;
    for highFrq = lowFrq+1:35
      paramValues(k,2) = highFrq;
      % validation for this value of firstMoment and default value for other parameters
      validationAcc(k) = preprocessAndValidateBin1(HDR, rawSignals, ppp.firstMoment,
      ppp.lastMoment, ppp.filterOrder, lowFrq, highFrq, trueLabels, opts,
      trainingIndex, trainingTrials, testingIndex, testTrials);
      if verbose
        disp(sprintf('Validation for lowFrq=%d, highFrq=%d acc=%5.2f%%',lowFrq,
        highFrq,validationAcc(k)*100));
        fflush(stdout);
      end
      k=k+1;
    end
  end
  
  [acc,ix] = max(validationAcc);
  lowFrq = paramValues(ix,1);
  highFrq = paramValues(ix,2);
  
  if verbose
    disp ' '
    disp 'Best parameters for this subject:'
    disp(sprintf('lowFrq=%d',lowFrq));
    disp(sprintf('highFrq=%d',highFrq));
    disp(sprintf('Final accuracy is %5.2f%%', acc*100));
  end
  
end
%---------------------------------------------
function acc = preprocessAndValidateBin1(HDR, rawSignals, parameters, trueLabels, 
    opts, trainingIndex, trainingTrials, testingIndex, testTrials, classPairIndex)
    
    C=60;		% Channels
    
    filterOptions.filterOrder = parameters.filterOrder;
    filterOptions.lowFrq = parameters.lowFrq;
    filterOptions.highFrq = parameters.highFrq;
    filterOptions.alpha = 0.5;
    filterOptions.type = 'wsibandpass';
    signal2 = preprocessWithParams1(rawSignals, HDR.TRIG, parameters.firstMoment,
      parameters.lastMoment, filterOptions);
    
    % FIXME use classPairIndex to restrict training on this sub prob
    cspinst = trainCSP(C,4,@(idx) simpleTrialGenerator(idx, trainingIndex, signal2, 
      HDR.Classlabel), trainingTrials, opts.extensionModel, 
      opts.numSelectedFilters, false); 
    
    CCmodel.TYPE = 'FLDA';  
    CC = trainBinClassifiers(cspinst, @(idx) simpleTrialGenerator(idx, trainingIndex, 
      signal2, HDR.Classlabel), trainingTrials, CCmodel, false, true, classPairIndex);
    
    R = testBinClassifiers(cspinst, @(idx) simpleTrialGenerator(idx,
      testingIndex, signal2, HDR.Classlabel), testTrials, CC, CCmodel,
      trueLabels, true, classPairIndex);
    
    acc = R.Rbin.ACC;
end
%---------------------------------------------
function signal2 = preprocessWithParams1(signal1, trigger, firstMoment,
      lastMoment, filterOptions)
  C=60;		% Channels
  frqs=250;	% Hz
  firstSample = fix(frqs*firstMoment);
  lastSample = fix(frqs*lastMoment);  
  T = lastSample-firstSample+1;
  if strcmpi(filterOptions.type, 'butter')
    h = butter(filterOptions.filterOrder, 2*[filterOptions.lowFrq,filterOptions.highFrq]/frqs);
  elseif strcmpi(filterOptions.type, 'wsibandpass')
    winoptions.type=1;
	  winoptions.alpha=filterOptions.alpha;
	  h = wsibandpass(filterOptions.lowFrq*2*pi/frqs, 
      filterOptions.highFrq*2*pi/frqs, filterOptions.filterOrder, winoptions);
  end
  window = repmat(hamming(T), 1, C);
	signal2 = zeros(C,C,numel(trigger));
  central = eye(T)-ones(T)/T;
	for n=1:numel(trigger)
    startRow = trigger(n)+firstSample-1;
    signal2(:,:,n) = covarianceWithNaN(filter(h, 1, signal1(startRow:startRow+T-1,:).*window)' * central);
	end
	if all(signal2(:)==0)
		error "Signals after preprocessing are all zero"
	%else
	%	disp "Healthy preprocessed signal:"
	%	disp(signal(1:20,1:8))
	end
end
%---------------------------------------------
function signal2 = preprocess(signal1, trigger)
  % Returns a 3D array with C times C times N elements
	%Torg = 1000;  % Original T
  C=60;		% Channels
  frqs=250;	% Hz
  % From 0.5 to 2.5 seconds after the cue
  firstSample = fix(frqs*0.5);
  lastSample = fix(frqs*2.5);  
  T = lastSample-firstSample+1;
  h = butter(7, 2*[8,30]/frqs);
  %winoptions.type=1;
	%winoptions.alpha=0.5;
	%h = wsibandpass(8*2*pi/frqs, 30*2*pi/frqs, 16, winoptions);
	window = repmat(hamming(T), 1, C);
	signal2 = zeros(C,C,numel(trigger));
  central = eye(T)-ones(T)/T;
	for n=1:numel(trigger)
    startRow = trigger(n)+firstSample-1;
    signal2(:,:,n) = covarianceWithNaN(filter(h, 1, signal1(startRow:startRow+T-1,:).*window)' * central);
	end
	if all(signal2(:)==0)
		error "Signals after preprocessing are all zero"
	%else
	%	disp "Healthy preprocessed signal:"
	%	disp(signal(1:20,1:8))
	end
end
