function xopt = optFullyExplore(f, a, b, dx, plotResult)
  % x=optFullyExplore(f,x0,a,b,dx,plotResult=false)
% Maximizes a univariate function using full exploration method
%
% f : A function handle
% a,b : Interval of legal values
% dx : step size
%
% Returens a maximum of f

if dx<=0
  error 'dx must be positive'
end

if b<=a
  error 'value ''a'' must be less then value ''b''' 
end

if nargin<5
  plotResult=false;
end


points=a:dx:b;
fvals=zeros(size(points));
fvalmax=-Inf;
for t=1:numel(points)
  x=points(t);
  y = f(x);
  fvals(t)=y;
  if y>fvalmax
    xopt = x;
    fvalmax = y;
  end
end

if plotResult
  figure
  plot(points, fvals)
  xlabel('x')
  ylabel('y')
  title('Objective function')
end
end