function [x, objf, numEvals] = optHillClimbing(f, domain, step, options)
  % [x, objf, numEvals] = optHillClimbing(f, domain, dx, options)
  % Maximizes an objective function by Hill Climbing method with random starts, 
  % sideway moves and possible restarts.
  %
  % f : function handle for the objective function
  % domain : A matrix with two columns. Each row defines an interval. 
  % Descartesian product of all these intervals defines the domain
  % step : length of each step
  % options : struct with fields:
  % - successorsStrategy : one of 'steepest', 'stochastic' or 'first-choice'
  % - sidewaysLimit : Maximum number of sideways steps allowed before a restart
  % - restartsLimit : Maximum number of restarts allowed
  objf = -inf;
  x=[];
  ndims = size(domain,1);
  numEvals=0;
  backspaces=char(8*ones(1,9));
  switch options.successorsStrategy
    case 'steepest'
      successorFunc = 'steepestSuccessor';
    case 'stochastic'
      successorFunc = 'stochasticSuccessor';
    case 'first-choice'
      successorFunc = 'firstChoiceSuccessor';
    otherwise
      error 'Unknown successor generation strategy'
  end
  % May return from within this loop
  for restart=1:options.restartsLimit
    % A random start
    if options.verbose
      disp 'HillClimbing starts over'
      fprintf(stdout,'Current height = 12.345678');
      fflush(stdout);
    end
    current = rand(ndims,1).*(domain(:,2)-domain(:,1)) + domain(:,1);
    currentFVal = f(current);
    if objf<currentFVal
      x = current;
      objf = currentFVal;
    end
    numSideways=0;
    while numSideways<options.sidewaysLimit
      % a successor of current state
      [nextPoint,nextFVal,numEvalsLocal] = feval(successorFunc, f, current, currentFVal, step, domain);
      numEvals = numEvals+numEvalsLocal;
      if ~isempty(nextPoint)
        if currentFVal==nextFVal
          numSideways = numSideways+1;
        else
          numSideways=0;
        end
        current = nextPoint;
        currentFVal = nextFVal;
        
        if options.verbose
          fprintf(stdout, sprintf('%s%9.6f',backspaces,currentFVal));
          fflush(stdout);
        end
        
        if objf<nextFVal
          x = nextPoint;
          objf = nextFVal;
        end
      else % We are on a local maximum
        return;
      end
    end % while
  end % for
end
%---------------------
function [nextPoint,nextFVal,numFCalls] = steepestSuccessor(f, currentPoint, currentFVal, step, domain)
  % May return empty array of all successors are below the current point
  points = allNeighbours(currentPoint, step, domain);
  fvals=zeros(1,size(points,2));
  numFCalls = size(points,2);
  for t=1:size(points,2)
    fvals(t) = f(points(:,t));
  end
  [maxfval,idx] = max(fvals);
  if maxfval>=currentFVal
    nextPoint = points(:,idx);
    nextFVal = maxfval;
  else
    nextPoint=[];
    nextFVal=nan;
  end
end
%--------------------
function [nextPoint,nextFVal,numFCalls] = firstChoiceSuccessor(f, currentPoint, currentFVal, step, domain)
  points = allNeighbours(currentPoint, step, domain);
  fvals=zeros(1,size(points,2));
  numFCalls = size(points,2);
  for t=1:size(points,2)
    fvals(t) = f(points(:,t));
  end
  % Randomly choose one of ascending moves
  ascends = find(fvals>=currentFVal);
  if numel(ascends)>0
    idx = fix(rand*numel(ascends))+1;
    nextPoint = points(:,ascends(idx));
    nextFVal = fvals(ascends(idx));
  else
    nextPoint=[];
    nextFVal=nan;
  end
end
%--------------------
function [nextPoint,nextFVal,numFCalls] = stochasticSuccessor(f, currentPoint, currentFVal, step, domain)
  points = allNeighbours(currentPoint, step, domain);
  fvals=zeros(1,size(points,2));
  numFCalls = size(points,2);
  for t=1:size(points,2)
    fvals(t) = f(points(:,t));
  end
  % Choose one of uphill moves by probablity proportionate to its height
  ascends = find(fvals>=currentFVal);
  if numel(ascends)>0
    fvals = fvals(ascends);
    probablity = [0,cumsum(fvals)/sum(fvals)];
    ball = rand;
    idx = find(probablity(1:end-1)<=ball & ball<probablity(2:end));
    nextPoint = points(:,ascends(idx));
    nextFVal = fvals(idx);
  else
    nextPoint=[];
    nextFVal=nan;
  end
end
%--------------------
function points = allNeighbours(current, step, domain)
  % Returns a matrix whose columns are points in the search space
  points = zeros(size(domain,1), 2*size(domain,2));
  numActualPoints=0;
  for d=1:size(domain,1)
    if current(d)>domain(d,1)
      numActualPoints = numActualPoints+1;
      points(:,numActualPoints) = current;
      points(d,numActualPoints) = max(current(d)-step, domain(d,1));
    end
    if current(d)<domain(d,2)
      numActualPoints = numActualPoints+1;
      points(:,numActualPoints) = current;
      points(d,numActualPoints) = min(current(d)+step, domain(d,2));
    end
  end
  points = points(:,1:numActualPoints);
end