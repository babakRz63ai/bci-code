function [x, objf, numEvals] = optSimAnnealingND(f, domain, x0, dx, options)
% [x, objf, numEvals] = optSimAnnealingND(f, domain, x0, dx, options)
% Maximizes a given function by searching in an N-dimensional domain
%
% f : The objective function as a function handle
% domain : A matrix defining domain of the objective function. The domain is a
% Descartesian product of intervals that are given as a pair of values in one 
% row of the matrix 'domain'.
% x0 : The starting point as a column vector
% dx : A positive scalar; amount if movement along each dimension
% options : If present, must be a struct with fields below:
%    numIterations : Maximum number of iterations (default inf)
%    verbose : boolean
%
% Returns :
%   x        : The optimal value
%   objf     : Final value of the objective function
%   numEvals : Number of times the objective function has been called

if dx<=0
  error 'dx must be positive'
end

if nargin<5
  numEvals = pord(fix((domain(:,2)-domain(:,1))/dx));
else
  numEvals = options.numIterations;
end

current = x0;
bestFval=-inf;
x = x0;
ndims = numel(x0);
current=x0;
valueCurrent = f(x0);
T=1000; % The simulated temprature
if options.verbose
  disp(sprintf('SimAnnealing : total steps is %d',numEvals));
  fprintf(stdout,'123456  12.345678');
  backspace=char(8*ones(1,17));
end  
for t=1:numEvals
  next = current;
  chosenDim = fix(rand*ndims)+1;
  if rand>=0.5
    next(chosenDim) = min(next(chosenDim) + dx, domain(chosenDim,2));
  else
    next(chosenDim) = max(next(chosenDim) - dx, domain(chosenDim, 1));
  end
  valueNext = f(next);
  dE = valueNext-valueCurrent;
  if dE>0 || rand<exp(dE/T)
    current = next;
    valueCurrent = valueNext;
    
    if options.verbose
      fprintf(stdout, sprintf('%s%6d  %9.6f',backspace,t,valueCurrent));
      fflush(stdout);
    end
    
    if bestFval<valueCurrent
      bestFval = valueCurrent;
      x = current;
    end
  T = T*0.92;
end % for t

objf = bestFval;

end