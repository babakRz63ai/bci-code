function x=optSimAnnealing(f,x0,a,b,dx)
% x=optSimAnnealing(f,x0,a,b,dx)
% Maximizes a univariate function using simulated annealing method
%
% f : A function handle
% x0 : The starting point
% a,b : Interval of legal values
% dx : step size
%
% Returens a maximum of f

if dx<=0
  error 'dx must be positive'
end

if b<=a
  error 'value ''a'' must be less then value ''b''' 
end

nPoints = fix((b-a)/dx+1);
savedFVals = nan(1,nPoints);
current = x0;
currentIdx = fix((x0-a)/dx+1);
valueCurrent = f(x0);
savedFVals(currentIdx) = valueCurrent;
T=1000;
maxIterations=500;
for t=1:maxIterations
  found=false;
  do
    r = rand;
    if r<0.5 && a+dx<=current
      next = current-dx;
      nextIndex = currentIdx-1;
      found=true;
    elseif r>=0.5 && current<=b-dx
      next = current+dx;
      nextIndex = currentIdx+1;
      found = true;
    end
  until(found);
  
  % Only calculate if needed
  if isnan(savedFVals(nextIndex))
    valueNext = f(next);
    savedFVals(nextIndex) = valueNext;
  else
    valueNext = savedFVals(nextIndex);
  end
  
  dE = valueNext-valueCurrent;
  
  %disp(sprintf('Simulated annealing : dE=%f , T=%f, current=%f, f(current)=%f',
  %  dE,T,current,valueCurrent));
  %fflush(stdout);
  
  if dE>0 || rand<exp(dE/T)
    current = next;
    valueCurrent = valueNext;
    currentIdx = nextIndex;
  end
  T = T*0.92;
  if t>1
    fprintf(stdout,char(8*ones(1,5)));
  end
  fprintf(stdout,sprintf('%4.1f%%%%',100*t/maxIterations));
  fflush(stdout);
end
% Return a point with the most elevation
[v,ix] = max(savedFVals);
x = a+(ix-1)*dx;
n = sum(~isnan(savedFVals));
disp(sprintf('Objective function is evaluated in %d points (%5.2f%%)',n,100*n/numel(savedFVals)));
fflush(stdout);
end