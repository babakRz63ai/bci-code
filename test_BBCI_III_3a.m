function test_BBCI_III_3a(method, operation, subject, classPairs)
  %test_BBCI_III_3a(method, operation, subject, classPairs)
  % method : One of 'CSP' or 'CSSSP'
  % operation : One of 'demo','invest1', 'invest2' or 'full'
  % subject : 1...3, a subject whome you want to run simulations for
  % A subset of 1:6
  if nargin<2
    error 'Usage : test_BBCI_III_3a(method, operation)'
  end
  if ~ischar(method) || ~ischar(operation)
    error 'Usage : test_BBCI_III_3a(method, operation)'
  end
  
  [headers, signals, trueLabels, ppp] = initAll(subject);
  
  if isequal(operation,'demo') || isequal(operation,'full')
    disp 'Preprocessing signals...'
    fflush(stdout);
    %for k=1:3
      [signals{1}, frqSampling] = downSample(signals{1}, 2);
    %end
  end
  
  if isequal(method,'CSP')
    switch(operation)
      case 'demo'
        demoBinaryCSP(headers, signals, trueLabels, ppp, frqSampling);
      
      case 'full'
        variantPreprocessBin1(headers, signals, trueLabels, frqSampling);
      
      case 'invest1'
        investigate1CSPBin('k3b', headers{1}, signals{1}, ppp{1}, 1);
        investigate1CSPBin('k3b', headers{1}, signals{1}, ppp{1}, 2);
        investigate1CSPBin('k6b', headers{2}, signals{2}, ppp{2}, 1);
        investigate1CSPBin('k6b', headers{2}, signals{2}, ppp{2}, 2);
        investigate1CSPBin('l1b', headers{3}, signals{3}, ppp{3}, 1);
        investigate1CSPBin('l1b', headers{3}, signals{3}, ppp{3}, 2);
        
      case 'invest2'
        investigate2CSPBin('k3b', headers{1}, signals{1}, ppp{1}, 1);
        investigate2CSPBin('k3b', headers{1}, signals{1}, ppp{1}, 2);
        investigate2CSPBin('k6b', headers{2}, signals{2}, ppp{2}, 1);
        investigate2CSPBin('k6b', headers{2}, signals{2}, ppp{2}, 2);
        investigate2CSPBin('l1b', headers{3}, signals{3}, ppp{3}, 1);
        investigate2CSPBin('l1b', headers{3}, signals{3}, ppp{3}, 2);
        
      otherwise
        error(['Unknown operation:',operation])
    end
  elseif isequal(operation,'demo')
    demoBinCSSSP(headers, signals, trueLabels, ppp, frqSampling);
  elseif isequal(operation,'full')
    simulationCSSSP(headers, signals, trueLabels, ppp, frqSampling, subject, classPairs);
  else
    error('This operation (%s) is not recognized', operation);
  end
    
end
%---------------------------------------------
function [headers, signals, trueLabels, ppp] = initAll(subjectIndex)
  frqs=250;	% Hz
	C=60;		% Channels
	T=1000;		% Time samples after the visual cue
	subject={'k3b','k6b','l1b'};
	
  headers = cell(1,1);
  signals = cell(1,1);
  trueLabels = cell(1,1);
  %for k=1:3
	% Will load HDR and s for training and testing for each subject
  disp(sprintf('Loading data for subject %d : %s',subjectIndex,subject{subjectIndex}));
  fflush(stdout);
	load(sprintf('../datasets/bci/bbci.de/BCI-comp-III/3/a/%s.mat', subject{subjectIndex}))
  
	
  % True labels
	[fid,message] = fopen(sprintf('../datasets/bci/bbci.de/BCI-comp-III/3/a/true_labels_%s.txt',subject{subjectIndex}),'r');
	if fid==-1
		error(message)
	end
  testTrials = nnz(isnan(HDR.Classlabel));
	[trueLabels{1},c,message]=fscanf(fid,'%d',[testTrials,1]);
	fclose(fid);
	if length(message)>0
		disp (message)
	end
  signals{1} = reshapeAndWindow(s, HDR.TRIG);
  headers{1}=HDR;
	% end for k

%We have performaed a simulation and some best values for parameters are known 
% for each subject. It is just for demo
ppp = cell(1,1);
switch subjectIndex
  case 1
    opts.firstMoment = 0;
    opts.lastMoment = 4;
    opts.filterOrder = 72;
    opts.withNaNs = true;
  case 2
    opts.firstMoment = 2.7;
    opts.lastMoment = 4;
    opts.filterOrder = 80;
    opts.withNaNs = true;
  case 3
    opts.firstMoment = 2.375; % or 0
    opts.lastMoment = 4;  % or 4
    opts.filterOrder = 14;  % or 8
    opts.withNaNs = false;
endswitch

ppp{1} = opts;

end
%---------------------------------------------
%function worker2sim(HDR, signals, trueLabels, verbose)
   % Uses subfunctions of CSP to speedup the simulations
%   C=60;		% Channels
%	 T=1000;		% Time samples after the visual cue
%   results = cell(1,32);
%   resPerSubject = cell(3,32);
%   for k=1:3
%     numResults = 0;
%     cspinst = cspn(C,T,4,'CNV.SIM');
%     trainingIndex = find(~isnan(HDR{k}.Classlabel) & ~HDR{k}.ArtifactSelection);
%     testingIndex = find(isnan(HDR{k}.Classlabel));
%	   trainingTrials = numel(trainingIndex);
%	   testTrials = numel(testingIndex);
%     countOfTestTrials(k) = testTrials;
%     disp 'Training CSP...'
%     fflush(stdout);
%     cspinst = CSPaddTrial(cspinst, trainingTrials, @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
%      HDR{k}.Classlabel));
%     numResults=0;
%     disp 'Simultaneous diagonalization...'
%     fflush(stdout);
%     [V,COV] = simDiagonalize(covarianceMatrices(cspinst),1e-6,verbose);
%     if verbose
%       disp(sprintf('Quality of diagonalization is %f',diagonalQuality(COV)));
%       fflush(stdout);
%     end
%     
%     for alpha=1
%     [r,c,ix] = getRCIXforSimDiag(COV,alpha);
%     %calculate all possible spatial filters and then select some of them each time
%     for selectedFilters=2:10
%       cspinst.W = selectSpatialFiltersForSimDiag(r,c,ix,C,4,selectedFilters,V,verbose);
%       cspinst.selectedFilters = selectedFilters;
%       model.TYPE = 'FLDA';
%       for g=0.05:0.01:0.1
%       model.hyperparameter.gamma=g; % TODO change it and test
%       disp(sprintf('Training classifiers with gamma=%f ...',g))
%       fflush(stdout);
%       CC = train_sc(generatePatternsFromCSP(cspinst, 
%        @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
%        HDR{k}.Classlabel), 
%        trainingTrials, 'training'), HDR{k}.Classlabel(trainingIndex), model);
%       disp 'Testing...'
%       fflush(stdout);
%       %R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
       % HDR{k}.Classlabel),
       % numel(trueLabels{k}), CC, model, [], trueLabels{k});
%       R = testCSP(cspinst, @(idx) simpleTrialGenerator(idx, testingIndex, signals{k},[]),
%        testTrials, CC, model, [], trueLabels{k}); % It is not verbose 
%       % Saving results 
%       numResults=numResults+1;
%       res.acc = R.ACC;
%       res.kappa = R.kappa;
%       res.selectedFilters = selectedFilters;
%       res.alpha = alpha;
%       res.gamma = g;
%       resPerSubject{k,numResults} = res;
%     end  % for g
%     end  % for selectedFilters
%     end % for alpha (parameter for sorting eigenvalues of diagonalized covariances)
%   end  % for k
%   % Averaging
%   for n=1:numResults
%     sumACC=0;
%     sumKappa=0;
%     res = resPerSubject{1,n};
%     for k=1:3
%       sumACC = sumACC+resPerSubject{k,n}.acc*countOfTestTrials(k);
%       sumKappa = sumKappa + resPerSubject{k,n}.kappa*countOfTestTrials(k);;
%     end  %for k
%     res.acc = sumACC/sum(countOfTestTrials);
%     res.kappa = sumKappa/sum(countOfTestTrials);
%     results{n} = res;
%   end  %for n
  
  % Results of the simulation
%  disp 'Alpha  Filters   Gamma   Acc%    kappa';                       
%  for k2=1:numResults
%  disp(sprintf('%4.2f  %5d   %8.3f  %7.2f %9.4f',
%    results{k2}.alpha, results{k2}.selectedFilters, results{k2}.gamma,
%    results{k2}.acc*100, results{k2}.kappa));
%  end

%end

%---------------------------------------------
%function [results,numResults] = worker1(headers, signals, trueLabels)
    % Calls the worker in a set of nested loops
%extModels={'CNV.BIN','OVR.CSP4','CNV.CSP0','CNV.CSP3'};
%results = cell(1,32);
%k=1;
%opts.gamma = 0.075;
%for modelIndex=1
%  opts.extensionModel = extModels{modelIndex};
%  for numSelected = 2:9
%    opts.numSelectedFilters=numSelected;
%    for balanced=0:0
%      opts.balancedTraining = balanced==1;
%      for normalized=0:0
%        opts.normalize = normalized==1;
%        [opts.avgacc,opts.avgkappa] = worker1sub(headers, signals, trueLabels, opts);
%        results{k} = opts;
%        k=k+1;
%      end
%    end
%  end
%end
%numResults = k-1
% Results of the simulation
%disp '--Model-  filters  balanced  normalized  Acc%   kappa';                           
%for k2=1:numResults
%  disp(sprintf('%8s%5d%10d%12d%10.2f%9.4f',
%    results{k2}.extensionModel, results{k2}.numSelectedFilters, results{k2}.balancedTraining,
%    results{k2}.normalize, results{k2}.avgacc, results{k2}.avgkappa));
%end

%end
%---------------------------------------------
%function [avgacc,avgkappa] = worker1sub(HDR, signals, trueLabels, opts)
  % worker(HDR, signal, trueLabels, opts)
  %  HDR : A cell array of HDR structs
  %  signals : A cell array of signals of all subjects
  % trueLabels : A Cell array of true labels for each subject
  %  opts : A struct containing these fields:
  %   extensionModel : One of 'OVR','OVR.CSP0', 'OVR.CSP3' or 'SIM'
  %   numSelectedFilters : number of selected spatial filters for each class
  %   gamma          : A scalar used for training classifiers
  %   balancedTraining : boolean
  %   normalize        : boolean, indicates testing should be done using normalized discriminants
  
%  frqs=250;	% Hz
%	C=60;		% Channels
%	T=1000;		% Time samples after the visual cue
%  subject={'k3b','k6b','l1b'};
  
%	estimatedKappa=zeros(1,3);		% One for each subject
%  allacc=zeros(1,3);		% One for each subject
%  countOfTestTrials=zeros(1,3);
%	totalTime=0;
	
%	for k=1:3
%	moment = cputime;
	
%  trainingIndex = find(~isnan(HDR{k}.Classlabel) & ~HDR{k}.ArtifactSelection);
%	testingIndex = find(isnan(HDR{k}.Classlabel));
%	trainingTrials = numel(trainingIndex);
%	testTrials = numel(testingIndex);
%  countOfTestTrials(k) = testTrials;
%  disp 'Training CSP...'
%  fflush(stdout);
  
	%csp = trainCSP(C,T,4,@(idx) mytrialGenerator(idx, signals{k}, trainingIndex, HDR{k}.TRIG, HDR{k}.Classlabel), 
  %trainingTrials, opts.extensionModel, opts.numSelectedFilters, true);
%  csp = trainCSP(C,T,4,@(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
%      HDR{k}.Classlabel), trainingTrials, opts.extensionModel, 
%      opts.numSelectedFilters, true);
%  disp 'Training classifiers...'
%  fflush(stdout);
%  model.TYPE = 'FLDA';
%  model.hyperparameter.gamma=opts.gamma;
%  if strcmpi(opts.extensionModel,'CNV.BIN')
%    % TODO
%  elseif strncmpi(opts.extensionModel,'CNV',3)  % Conventional multi class mode
%    CC = train_sc(generatePatternsFromCSP(csp, 
%      @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, HDR{k}.Classlabel), 
%      trainingTrials, 'training'), HDR{k}.Classlabel(trainingIndex), model);
%  elseif strncmpi(opts.extensionModel,'OVR',3)
%    ovrmodel.fairTrain = opts.balancedTraining;
%    [CC,extra] = trainOVRClassifiers(generatePatternsFromCSP(csp, 
%      @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, HDR{k}.Classlabel), 
%      trainingTrials, 'training'),
%    HDR{k}.Classlabel(trainingIndex), model, ovrmodel);
%    %disp 'extra is:'
    %disp(extra)
%  else
%    error 'Other extension models are not implemented'
%  end
%  timePassed = cputime-moment;
  
%  disp 'Testing...'
%  fflush(stdout);
%  if ~opts.normalize
%    extra=[];
%  end
%	R = testCSP(csp, @(idx) simpleTrialGenerator(idx, testingIndex, signals{k},[]),
%    testTrials, CC, model, [], trueLabels{k}); % It is not verbose
	
  % A matrix with 4 columns and one row for each time step
%  estimatedKappa(k) = R.kappa;
%  allacc(k) = R.ACC;
%	totalTime = totalTime+timePassed;
%	disp(sprintf('Accuracy for subject %s is %f%%, kappa=%f, time=%fs',
%  subject{k}, 100*allacc(k), estimatedKappa(k), timePassed));
  %disp 'Confusion matrix is:'
  %disp(R.H);
  %disp 'Accuracy per class is:'
  %disp(X.sACC)
  %fflush(stdout);
%end 	% for k
%avgacc = 100*sum(allacc.*countOfTestTrials)/sum(countOfTestTrials);
%avgkappa = sum(estimatedKappa.*countOfTestTrials)/sum(countOfTestTrials);
%	disp(sprintf('Average Accuracy=%f%%, average kappa is %f, total time=%fs',
%  avgacc,avgkappa, totalTime));
%fflush(stdout);  
%end

%---------------------------------------------
%function demo1(HDR, signals, trueLabels)
  % HDR : Cell array of eaders
  % signals : Cell array of autocorrelations of signals
  % trueLabels : Cell array containing sets of true labels
%  C=60;		% Channels
%	T=1000;		% Time samples after the visual cue
%  estimatedKappa=zeros(1,3);		% One for each subject
%  allacc=zeros(1,3);		% One for each subject
%  countOfTestTrials=zeros(1,3);
%  subject={'k3b','k6b','l1b'};
%  opts.numSelectedFilters = 2;
%  opts.extensionModel = 'CNV.SIM';
%  for k=1:3
%    disp(['Subject ',subject{k}]);
%    trainingIndex = find(~isnan(HDR{k}.Classlabel) & ~HDR{k}.ArtifactSelection);
%	  testingIndex = find(isnan(HDR{k}.Classlabel));
%	  trainingTrials = numel(trainingIndex);
%	  testTrials = numel(testingIndex);
%    countOfTestTrials(k) = testTrials;
%    disp 'Training CSP...'
%    fflush(stdout);
%	  csp = trainCSP(C,T,4,@(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
%     HDR{k}.Classlabel), trainingTrials, opts.extensionModel, 
%      opts.numSelectedFilters, true);
%  
%    disp 'Training classifiers...'
%    fflush(stdout);
%    ovrmodel.fairTrain = false;
%    model.TYPE = 'FLDA';
%    %[CC,extra] = trainOVRClassifiers(generatePatternsFromCSP(csp, 
%    %  @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, HDR{k}.Classlabel), 
%    %  trainingTrials, 'training'), HDR{k}.Classlabel(trainingIndex), model, 
%    %  ovrmodel);
%    CC = train_sc(generatePatternsFromCSP(csp, 
%      @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, HDR{k}.Classlabel), 
%      trainingTrials, 'training'), HDR{k}.Classlabel(trainingIndex), model);
%    disp 'Testing...'
%    fflush(stdout);
%    R = testCSP(csp, @(idx) simpleTrialGenerator(idx, testingIndex, signals{k},[]),
%    testTrials, CC, model, [], trueLabels{k}, true);
%    disp 'Confusion matrix is'
%    disp(R.H);
%    disp ' '
%    %disp 'Binary classifier       Accuracy%'
%    %for c=1:4
%    %  disp(sprintf('  %d                 %5.2f%%',c,100*R.accPerBinClass(c)));
%    %end
    
%    disp(sprintf('Accuracy is %5.2f%% , kappa is %f', R.ACC*100, R.kappa));
%    allacc(k) = R.ACC;
%    estimatedKappa(k) = R.kappa;
%    countOfTestTrials(k) = testTrials;
%  end
%  disp(sprintf('Average accuracy is %5.1f%% , average kappa is %f',
%    100*sum(allacc.*countOfTestTrials)/sum(countOfTestTrials) , 
%    sum(estimatedKappa.*countOfTestTrials)/sum(countOfTestTrials)));
%end

%---------------------------------------------
%function demo2(HDR, signals, trueLabels)
%  % Tests with voting binary classifiers for each pair of classes
%  % HDR : Cell array of eaders
%  % signals : Cell array of autocorrelations of signals
%  % trueLabels : Cell array containing sets of true labels
%  C=60;		% Channels
%  estimatedKappa=zeros(1,3);		% One for each subject
%  allacc=zeros(1,3);		% One for each subject
%  countOfTestTrials=zeros(1,3);
%  subject={'k3b','k6b','l1b'};
%  opts.numSelectedFilters = 3;
%  opts.extensionModel = 'CNV.BIN';
%  CCmodel.TYPE = 'FLDA';
%  for k=1:3
%    disp(['Subject ',subject{k}]);
%    [trainingIndex, trainingTrials, testingIndex, testTrials] = calcIndices(HDR{k});
%    countOfTestTrials(k) = testTrials;
%    disp 'Training CSP...'
%    fflush(stdout);
%	  cspinst = trainCSP(C,4,@(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
%      HDR{k}.Classlabel), trainingTrials, opts.extensionModel, 
%      opts.numSelectedFilters, false);
%    
%    disp 'Training classifiers...'
%    fflush(stdout);
%    CC = trainBinClassifiers(cspinst, @(idx) simpleTrialGenerator(idx, trainingIndex, signals{k}, 
%      HDR{k}.Classlabel), trainingTrials, CCmodel, true);
%    
%    disp 'Testing CSP and classifiers...'
%    fflush(stdout);
%    R = testBinClassifiers(cspinst, @(idx) simpleTrialGenerator(idx, 
%      testingIndex, signals{k}, HDR{k}.Classlabel), testTrials, CC, CCmodel,
%      trueLabels{k});
%    disp ' '
%    disp 'Pair      Accuracy'
%    pairGenerator = [1 2; 1 3; 1 4; 2 3; 2 4; 3 4];
%    for pair=1:6
%      disp(sprintf('(%d,%d)      %5.2f%%', pairGenerator(pair,1),
%      pairGenerator(pair,2), R.Rbin{pair}.ACC*100));
%    end
%    disp ' '
%    disp 'Total confusion matrix:'
%    disp(R.H);
%    disp(sprintf('Total accuracy for this subject is %5.2f%%',R.ACC*100));
%    disp(sprintf('Total kappa for this subject is %f',R.kappa));
%    estimatedKappa(k) = R.kappa;
%  end
%  disp(sprintf('Total kappa is %f',mean(estimatedKappa)));
%end
%---------------------------------------------
function demoBinaryCSP(HDR, signals, trueLabels, ppp, frqSampling)
  subjectName={'k3b','k6b','l1b'};
  CCmodel.TYPE = 'FLDA';
  filterOptions.lowFrq = 6;
  filterOptions.hiFrq = 35;
  filterOptions.filterOrder=16;
  % Preprocess signals to get an array of covariance matrices
  for k=1:3
    s2 = preprocessWithParamsForCSP(signals{k}, ppp{k}.firstMoment, 
        ppp{k}.lastMoment, frqSampling, filterOptions);
    for classPairIndex=1:6
      disp(sprintf('Subject %s , class pair %d',subjectName{k}, classPairIndex));
      fflush(stdout);
      [trainingIndex, testingIndex, testingIndex2] = calcIndices(HDR{k},
        classPairIndex, trueLabels{k});
      restrictedSignal=s2(:,:,trainingIndex);
      restrictedLabels = getRestrictedLabels(HDR{k}.Classlabel(trainingIndex), classPairIndex);
      % training CSP
      cspinst = trainCSP(restrictedSignal, restrictedLabels, '', 3, true);
      % training linear classifier
      CC = train_sc(generatePatternsFromCSP(cspinst, restrictedSignal, 
        restrictedLabels), restrictedLabels, CCmodel);
      % Testing
      R = testCSP(cspinst, s2(:,:,testingIndex), CC, CCmodel, 
        getRestrictedLabels(trueLabels{k}(testingIndex2), classPairIndex));
      disp 'Confusion matrix:'
      disp(R.H)  
      disp(sprintf('Final accuracy : %5.2f%%',R.ACC*100));
      fflush(stdout);
    end
  end
end
%---------------------------------------------
function investigate1CSPBin(subjectName, HDR, signals, ppp, downsampleRate)
  % Investigates effect of downsampling with on separability of four 
  % classes for a subject
  
  CCmodel.TYPE = 'FLDA';
  filterOptions.lowFrq = 6; % Hz
  filterOptions.hiFrq = 35; % Hz
  filterOptions.filterOrder=16;
  % Estimates separability using possible downsampling
  if downsampleRate>1
    [signals, frqSampling] = downSample(signals, downsampleRate);
  else
    frqSampling = 250; % Hz
  end
  
  s2 = preprocessWithParamsForCSP(signals, ppp.firstMoment, 
        ppp.lastMoment, frqSampling, filterOptions);
  plotStyle={'r+','bo','g^','m.'};
  classDesc=[1,2;1,3;1,4;2,3;2,4;3,4];
  if downsampleRate==1
    disp(sprintf('\n-------- Subject %s --------',subjectName));
  else
    disp(sprintf('\n-------- Subject %s with downsampling rate %d --------',
      subjectName, downsampleRate));
  end
  disp 'First class    Second Class    Avg. distance    Accuracy'
  disp '-----------    ------------    -------------    --------'
  fflush(stdout);
  for classPairIndex=1:6    
    trainingIndex = calcIndices(HDR, classPairIndex);
    restrictedSignal=s2(:,:,trainingIndex);
    restrictedLabels = getRestrictedLabels(HDR.Classlabel(trainingIndex), classPairIndex);
    % training CSP
    cspinst = trainCSP(restrictedSignal, restrictedLabels, '', 3);
    D = generatePatternsFromCSP(cspinst, restrictedSignal, restrictedLabels);
    %figure
    %hold on
    %plot3(D(restrictedLabels==1,1),D(restrictedLabels==1,2),
    %   D(restrictedLabels==1,3),plotStyle{classDesc(classPairIndex,1)});
    %plot3(D(restrictedLabels==2,4),D(restrictedLabels==2,5),
    %   D(restrictedLabels==2,6),plotStyle{classDesc(classPairIndex,2)});
    %xlabel('x');
    %ylabel('y');
    %zlabel('z');
    %if downsampleRate==1
    %  title(sprintf('Mapping samples of classes %d and %d using original signals for %s',
    %    classDesc(classPairIndex,1),classDesc(classPairIndex,2),subjectName));
    %else
    %  title(sprintf('Mapping samples of classes %d and %d by downsampling signals in rate %d for %s',
    %    classDesc(classPairIndex,1), classDesc(classPairIndex,2), downsampleRate,
    %    subjectName));
    %end    
    % A measure of distance of two distributions is mean of distances of points
    % of each class from the decision hyperplane
    CC = train_sc(D, restrictedLabels, CCmodel);
    R = test_sc(CC, D, CCmodel, restrictedLabels);
    dist = mean(R.output(restrictedLabels==1,1)) + mean(R.output(restrictedLabels==2,2));
    % prints a tidy and compact table including accuracy on the training samples
    disp(sprintf('%6d %13d %23f %10.2f%%', classDesc(classPairIndex,1), 
      classDesc(classPairIndex,2), dist, 100*R.ACC));
    fflush(stdout);
  end
end
%---------------------------------------------
function investigate2CSPBin(subjectName, HDR, signals, ppp, downsampleRate)
  % Investigates effect of downsampling on entropy of processed signal of 
  % training trials
  
  filterOptions.lowFrq = 6; % Hz
  filterOptions.hiFrq = 35; % Hz
  filterOptions.filterOrder=16;
  % Estimates separability using possible downsampling
  if downsampleRate>1
    [signals, frqSampling] = downSample(signals, downsampleRate);
  else
    frqSampling = 250; % Hz
  end
  
  s2 = preprocessWithParamsForCSP(signals, ppp.firstMoment, 
        ppp.lastMoment, frqSampling, filterOptions);
  classDesc=[1,2;1,3;1,4;2,3;2,4;3,4];
  if downsampleRate==1
    disp(sprintf('\n-------- Subject %s --------',subjectName));
  else
    disp(sprintf('\n-------- Subject %s with downsampling rate %d --------',
      subjectName, downsampleRate));
  end
  disp 'First class    Second Class    First part       Second part        Total'
  disp '                                 entropy          entropy          entopy' 
  disp '-----------    ------------    ------------    -------------    -------------'
  fflush(stdout);

  % We don't need to calculate entropy of samples of a single class repeatedly.
  % This array will save these values after they have been calculated for the
 % first time 
  singleClassEnt = nan(1,4);
  
  % use more proper or relevant channels : 28,34 for C3,C4 or 28,31,34 for 
  %C3,Cz,C4 or 28,31,34,38,41,44 
  selectedChannels = [28,34]; 

  for classPairIndex=1:6    
    trainingIndex = calcIndices(HDR, classPairIndex);
    restrictedLabels = getRestrictedLabels(HDR.Classlabel(trainingIndex), classPairIndex);
    % Entropy of signals in each class
    for t=1:2
      if isnan(singleClassEnt(classDesc(classPairIndex,t)))
        singleClassEnt(classDesc(classPairIndex,t)) = entropyOfSignal(s2(selectedChannels, :, trainingIndex(restrictedLabels==t)), 5);
      end
    end
    
    % Entropy of signals in both classes together
    enttotal = entropyOfSignal(s2(selectedChannels, :, trainingIndex), 5);
    
    disp(sprintf('%6d %15d %19.3f %16.3f %16.3f', classDesc(classPairIndex,1), 
      classDesc(classPairIndex,2), 
      singleClassEnt(classDesc(classPairIndex,1)),
      singleClassEnt(classDesc(classPairIndex,2)),
      enttotal));
    fflush(stdout);
  end
end

%---------------------------------------------
function [trainingIndex, testingIndex, testingIndex2] = calcIndices(header,classPairIndex,trueLabels)
  %[trainingIndex, testingIndex, testingIndex2] = calcIndices(header,classPairIndex,trueLabels)
  % Calculates indices for training or testing
  % Returns:
  % trainingIndex : Used as the third index of signal array to generate training trials
  % testingIndex  : Used as the third index of signal array to generate testing trials
  % testingIndex2 : Used as index of true labels vector
  if nargin<2 % Use all classes
    trainingIndex = find(~isnan(header.Classlabel) & ~header.ArtifactSelection);
    testingIndex = find(isnan(header.Classlabel));
  else
    pairs=[1 2; 1 3; 1 4; 2 3; 2 4; 3 4];
    trainingIndex = find((header.Classlabel==pairs(classPairIndex,1) | header.Classlabel==pairs(classPairIndex,2)) & ~header.ArtifactSelection);
    tempix = find(isnan(header.Classlabel));
    if nargin>=3
      testingIndex2 = find(trueLabels==pairs(classPairIndex,1) | trueLabels==pairs(classPairIndex,2));
      testingIndex = tempix(testingIndex2);
    end
  end
end
%---------------------------------------------
function rst = getRestrictedLabels(generalLabels, pairIndex)
  % Translates general labels (1 to 4) to a restricted form of (1,2)
  % generalLabels : all class labels of training trials for a subject 
  pairDesc=[1,2; 1,3; 1,4; 2,3; 2,4; 3,4];
  rst = zeros(size(generalLabels));
  for i=1:2
    rst(generalLabels==pairDesc(pairIndex,i)) = i;
  end
end
%---------------------------------------------
function variantPreprocessBin1(HDR, rawSignals, trueLabels, frqSampling)
  % Training and testing binary classifiers for classes 1&2 with preprocessing
  % optimized for one subject
  C=60;		% Channels
  estimatedKappa=zeros(1,3);		% One for each subject
  allacc=zeros(1,3);		% One for each subject
  countOfTestTrials=zeros(1,3);
  subject={'k3b','k6b','l1b'};
  CSPopts.numSelectedFilters = 3;
  CSPopts.extensionModel = 'CNV.BIN';
  CCmodel.TYPE = 'FLDA';
  % Controls verbosity of sub methods
  verboseSubMethod = false;
  
  for classPairIndex=1:6
    disp(sprintf('------------------------------\n\nClass pair %d:',classPairIndex));
    fflush(stdout);
    for subjectIndex = 1:3
      
      [trainingIndex, testingIndex, testingIndex2] = calcIndices(HDR{subjectIndex},
        classPairIndex, trueLabels{subjectIndex});
      trainingLabels = getRestrictedLabels(HDR{subjectIndex}.Classlabel(trainingIndex),
        classPairIndex);
      testingLabels = getRestrictedLabels(trueLabels{subjectIndex}(testingIndex2),
        classPairIndex);
      % Default values
      parameters.firstMoment = 0.5;
      parameters.lastMoment = 2.5;
      parameters.filterOrder = 16;
      parameters.lowFrq = 8;
      parameters.highFrq = 30;  
    % first moment
      parameters.firstMoment = variantPreprocessBin1worker(parameters, 
        'firstMoment', 0:0.125:2.375,  rawSignals{subjectIndex}, 
        trainingIndex, testingIndex, trainingLabels, testingLabels,
        frqSampling, verboseSubMethod);
  
      parameters.lastMoment = variantPreprocessBin1worker(parameters, 
      'lastMoment', parameters.firstMoment+0.125:0.125:4, 
        rawSignals{subjectIndex}, 
        trainingIndex, testingIndex, trainingLabels, testingLabels,
        frqSampling, verboseSubMethod);
  
      parameters.filterOrder = variantPreprocessBin1worker(parameters, 
        'filterOrder', 4:2:40, 
        rawSignals{subjectIndex}, 
        trainingIndex, testingIndex, trainingLabels, testingLabels,
        frqSampling, verboseSubMethod);
  
      % Limits of the frequency band
      parameters.lowFrq = variantPreprocessBin1worker(parameters, 'lowFrq', 1:20, 
        rawSignals{subjectIndex}, 
        trainingIndex, testingIndex, trainingLabels, testingLabels,
        frqSampling, verboseSubMethod);
    
      parameters.highFrq = variantPreprocessBin1worker(parameters, 'highFrq', 
        parameters.lowFrq+1:40, 
        rawSignals{subjectIndex}, 
        trainingIndex, testingIndex, trainingLabels, testingLabels,
        frqSampling, verboseSubMethod);
  
      % Final test  
      acc = preprocessAndValidateBin1(rawSignals{subjectIndex}, parameters,
        frqSampling, 
        trainingIndex, testingIndex, trainingLabels, testingLabels, CSPopts);
  
      %if verbose
      disp ' '
      disp(['Best parameters for subject ',subject{subjectIndex}]);
      disp(sprintf('firstMoment=%f',parameters.firstMoment));
      disp(sprintf('lastMoment=%f',parameters.lastMoment));
      disp(sprintf('filterOrder=%d',parameters.filterOrder));
      disp(sprintf('lowFrq=%d',parameters.lowFrq));
      disp(sprintf('highFrq=%d',parameters.highFrq));
      disp(sprintf('Final accuracy is %5.2f%%', acc*100));
      fflush(stdout);
      %end
    end % for subject
  end % for class pair
end
%---------------------------------------------
function [parameter, validationAcc] = variantPreprocessBin1worker(parameters, name, paramValues, 
  rawSignals, trainingIndex, testingIndex, trainingLabels, testingLabels,
  frqSampling, verbose)
% parameters : A struct containing fields 'firstMoment', 'lastMoment',
% 'filterOrder', 'lowFrq', 'highFrq'
% name : String, name of the variable parameter
% paramValues : Vector of possible values
% rawSignals : raw signals of a subject
% trainingIndex, testingIndex : indices and their counts
% trainingLabels : vector of labels of training trials
% testingLabels : vector of labels of testing trials
% verbose : boolean, controls verbosity
%
% Returns :
%   - The best value of the parameter
%   - Vector of accuracies for testing all possible values
  opts.numSelectedFilters = 3;
  opts.extensionModel = 'CNV.BIN';
  CCmodel.TYPE = 'FLDA';
  validationAcc = zeros(size(paramValues));
  if nproc('current')==1
    for k=1:numel(paramValues)
      % validation for this value of the parameter and default value for other parameters
      parameters = setfield(parameters,name,paramValues(k));
      validationAcc(k) = preprocessAndValidateBin1(rawSignals, parameters, 
      frqSampling, trainingIndex, testingIndex, trainingLabels, testingLabels, opts);
      if verbose
        disp(sprintf('Validation for %s=%d, acc=%5.2f%%', name, paramValues(k),
        validationAcc(k)*100));
        fflush(stdout);
      end
    end % for each value
  else % Run in parallel
    validationAcc = pararrayfun(nproc('current'), 
      @(value) variantPreprocessBin1workerParFunc(value, parameters, name, 
        rawSignals, frqSampling, trainingIndex, testingIndex, trainingLabels, 
        testingLabels, opts),
      paramValues, 'ChunksPerProc', 100, 'VerboseLevel', 1);
  end
  [acc,ix] = max(validationAcc);
  parameter = paramValues(ix);
end
%---------------------------------------------
function acc = variantPreprocessBin1workerParFunc(paramValue, parameters,
  paramName, rawSignals, frqs, trainingIndex, testingIndex, trainingLabels, testingLabels, 
        opts)
  % To be run in parallel
  parameters = setfield(parameters,paramName,paramValue);
  acc = preprocessAndValidateBin1(rawSignals, parameters, frqs,
      trainingIndex, testingIndex, trainingLabels, testingLabels, opts);
end  
%---------------------------------------------
%function variantPreprocessBin2(HDR, rawSignals, trueLabels, ppp, verbose)
  % Training and testing binary classifiers for classes 1,2 with preprocessing
  % optimized for one subject
%  C=60;		% Channels
  
%  countOfTestTrials=zeros(1,3);
%  opts.numSelectedFilters = 3;
%  opts.extensionModel = 'CNV.BIN';
%  CCmodel.TYPE = 'FLDA';
  
%  [trainingIndex, trainingTrials, testingIndex, testTrials] = calcIndices(HDR);
   
  % Limits of the frequency band
%  numIters = (20-5+1)*35-sum(5:20);
%  validationAcc = zeros(numIters,1);
%  paramValues = zeros(numIters, 2);
%  k=1;
%  for lowFrq = 5:20
%    paramValues(k,1) = lowFrq;
%    for highFrq = lowFrq+1:35
%      paramValues(k,2) = highFrq;
%      % validation for this value of firstMoment and default value for other parameters
%      validationAcc(k) = preprocessAndValidateBin1(HDR, rawSignals, ppp.firstMoment,
%      ppp.lastMoment, ppp.filterOrder, lowFrq, highFrq, trueLabels, opts,
%      trainingIndex, trainingTrials, testingIndex, testTrials);
%      if verbose
%        disp(sprintf('Validation for lowFrq=%d, highFrq=%d acc=%5.2f%%',lowFrq,
%        highFrq,validationAcc(k)*100));
%        fflush(stdout);
%      end
%      k=k+1;
%    end
%  end
  
%  [acc,ix] = max(validationAcc);
%  lowFrq = paramValues(ix,1);
%  highFrq = paramValues(ix,2);
  
%  if verbose
%    disp ' '
%    disp 'Best parameters for this subject:'
%    disp(sprintf('lowFrq=%d',lowFrq));
%    disp(sprintf('highFrq=%d',highFrq));
%    disp(sprintf('Final accuracy is %5.2f%%', acc*100));
%  end
  
%end
%---------------------------------------------
function acc = preprocessAndValidateBin1(rawSignals, parameters, frqs,
      trainingIndex, testingIndex, trainingLabels, testingLabels, opts)
        
    filterOptions.filterOrder = parameters.filterOrder;
    filterOptions.lowFrq = parameters.lowFrq;
    filterOptions.hiFrq = parameters.highFrq;
    signal2 = preprocessWithParamsForCSP(rawSignals, parameters.firstMoment,
      parameters.lastMoment, frqs, filterOptions);
   
    st = signal2(:,:,trainingIndex);
    cspinst = trainCSP(st, trainingLabels, '', opts.numSelectedFilters);
    CCmodel.TYPE = 'FLDA';  
    CC = train_sc(generatePatternsFromCSP(cspinst, st, trainingLabels),
      trainingLabels, CCmodel);
    
    R = testCSP(cspinst, signal2(:,:,testingIndex), CC, CCmodel, testingLabels);
   
    acc = R.ACC;
end
%---------------------------------------------
function simulationCSSSP(headers, signals, trueLabels, ppp, frqSampling, 
  subjectIndex, classPairs)
  % Run simulations for a single subject on a certain subset of class pairs  
	subjectName={'k3b','k6b','l1b'};
  for classPairIndex=classPairs
  for subject=subjectIndex
    thisppp = ppp{1};
    disp(['Validating paramters for subject ',subjectName{subject},' on the class pair ',num2str(classPairIndex)]);
    fflush(stdout);
    problemName = ['pair-',num2str(classPairIndex),'_',subjectName{subject}];
    
    thisppp.firstMoment = validateParameterForCSSSP(problemName, 
      headers{1}, signals{1}, trueLabels{1}, classPairIndex, 
      thisppp, 'firstMoment', 0:0.1:3.9, frqSampling);
      
    [thisppp.lastMoment,acc] = validateParameterForCSSSP(problemName, 
      headers{1}, signals{1}, trueLabels{1}, 
      classPairIndex, thisppp, 'lastMoment', thisppp.firstMoment+0.1:0.1:4, 
      frqSampling);
    
    [thisppp.filteOrder, acc] = validateParameterForCSSSP(problemName, 
    headers{1}, signals{1}, trueLabels{1}, 
    classPairIndex, thisppp, 'filteOrder', 4:4:96, frqSampling);
    
    disp(sprintf('Best accuracy found : %5.2f%%',acc*100));
    fflush(stdout);
  end
  end
end
%---------------------------------------------

function [inferredParameter,bestACC] = validateParameterForCSSSP(problemName, header, signal, trueLabels, classPairIndex, ppp, paramName, paramValues, frqSampling)
  % Takes data of a single subject and validates a parameter on a range of possible values
    disp(['Validating ',paramName,'...']);
    fflush(stdout);
    model.CCModel.TYPE='FLDA';
    model.CCModel.hyperparameter.gamma=0.075;
    model.SpectFltrLength = ppp.filterOrder;
    model.problemName = problemName;
    [trainingIndex, testingIndex, testingIndex2] = calcIndices(header,
      classPairIndex, trueLabels);
    restrictedTrainingLabels = getRestrictedLabels(header.Classlabel(trainingIndex), 
      classPairIndex);
    restrictedTesingLabels = getRestrictedLabels(trueLabels(testingIndex2), 
      classPairIndex);

    % accuracy values
    resultData = zeros(size(paramValues));
    
    cssspoptions.verbose=true;
    cssspoptions.parallel=false;
    cssspoptions.optimizationMethod='fminunc';
  
    for t=1:numel(paramValues)
      disp(sprintf('testing %s = %f',paramName,paramValues(t)));
      fflush(stdout);
      ppp = setfield(ppp, paramName, paramValues(t));
      disp 'Preprocessing...'
      signal2 = preprocessWithParamsForCSSSP(signal, ppp.firstMoment, ppp.lastMoment, frqSampling, false);
      % Channels and Time samples after the visual cue
      %[C,T,K] = size(signal2);
      fflush(stdout)  
      csssp = trainCSSSP(signal2(:,:,trainingIndex), restrictedTrainingLabels, 
        model, 3, cssspoptions);
      if (~isempty(csssp.cspinst.W))
        D = generatePatternsFromCSSSP(csssp, signal2(:,:,trainingIndex));
        CCmode.TYPE = 'FLDA';
        CC = train_sc(D, restrictedTrainingLabels, CCmode);
        D = generatePatternsFromCSSSP(csssp, signal2(:,:,testingIndex));
        R = test_sc(CC, D, CCmode, restrictedTesingLabels);
        resultData(t) = R.ACC;
      end
    end
    [bestACC,ix] = max(resultData);
    inferredParameter = paramValues(ix);
    save('-binary', ['../resultdata/BBCI-III-3a-CSSSP/validating_',problemName,'_',paramName,'.mat'], 'paramValues','resultData');
end
%---------------------------------------------
function demoBinCSSSP(headers, signals, trueLabels, ppp, frqSampling)
% headers : Cell array of three HDR structures
% signals : Cell array of three signal matrices
% trueLabels : Cell array of three vectors
% ppp : Cell array of three structures containing preprocessing parameters
  frqs=250;	% Hz
	C=60;		% Channels
	T=1000;		% Time samples after the visual cue
	subjectName={'k3b','k6b','l1b'};
  cssspoptions.verbose=true;
  cssspoptions.parallel=false;
  cssspoptions.optimizationMethod='fminunc';
  for subject=1:3
    disp(sprintf('Subject %d : %s',subject, subjectName{subject}));
    disp 'Preprocessing...'
    fflush(stdout);
    signal2 = preprocessWithParamsForCSSSP(signals{subject}, 
      ppp{subject}.firstMoment, ppp{subject}.lastMoment, frqSampling);
    pairNames={'LR','LF','LT','RF','RT','FT'};
    pairDesc=[1,2; 1,3; 1,4; 2,3; 2,4; 3,4];
    classPairIndex=1;
    for filterOrder = ppp{subject}.filterOrder;
      disp(sprintf('class pair %d, filter length %d',classPairIndex,filterOrder));
      disp 'Training CSSSP...'
      fflush(stdout);
      time1 = cputime;
      tic
      model.SpectFltrLength = filterOrder; %ppp{subject}.filterOrder;
      model.CCModel.TYPE='FLDA';
      model.CCModel.hyperparameter.gamma=0.075;
      model.problemName = ['BBCI-III-3a(',pairNames{classPairIndex},') :',subjectName{subject},' with averaging on non-NaN values'];
      [trainingIndex, testingIndex] = calcIndices(headers{subject}, 
        classPairIndex, trueLabels{subject});
      
      restrictedTrainingLabels = getRestrictedLabels(headers{subject}.Classlabel(trainingIndex),
        classPairIndex);
      
      trainCSSSP(signal2(:,:,trainingIndex), restrictedTrainingLabels, 
        model, 3, cssspoptions);
      
      time2 = cputime;
      toc
      %disp 'Testing...'
      %fflush(stdout);
      
      disp(sprintf('Total CPU time spent is %f seconds',time2-time1));
      %disp 'Confusion matrix:'
      %disp(R.H);
      %disp(sprintf('Accuracy is %5.2f%%',R.ACC*100));
    end % for filterOrder
  end % for each subject
end
%---------------------------------------------
function signal2 = preprocessWithParamsCommon(signal1, firstMoment, lastMoment, frqs)
  % signal1 : A 3D array with channels as its rows, time samples as its columns 
  % and trials as its pages
  % firstMoment : First moment of the interesting part of each signal in seconds
  % lastMoment : Last moment of the interesting part of each signal in seconds
  % frqs : Effective sampling frequency
  
  [C,T,K] = size(signal1);
  % Windowing
  firstSample = max(fix(frqs*firstMoment),1);
  lastSample = fix(frqs*lastMoment);
  T = lastSample-firstSample+1;
  if T<=1
    error 'Window length is insufficient'
  end
  window = hamming(T)'; % So it will be a row
  if isOctave
	  signal2 = signal1(:,firstSample:lastSample,:).*window;
  else
    signal2 = signal1(:,firstSample:lastSample,:).*repmat(window,[C,1,K]);
  end
  
	if all(signal2(:)==0)
		error "Signals after preprocessing are all zero"
	%else
	%	disp "Healthy preprocessed signal:"
	%	disp(signal(1:20,1:8))
  end  
end
%---------------------------------------------
function signal2 = centralize(signalTemp)
  % Centralizing (subtracting average of each row from that row)
  [C,T,K] = size(signalTemp);
  if isOctave
    signal2 = signalTemp - mean(signalTemp,2);
  else
    signal2 = signalTemp - repmat(mean(signalTemp,2),[C,1,K]);
  end
end
%---------------------------------------------
function signal2 = preprocessWithParamsForCSP(signal1, firstMoment, lastMoment, 
  frqs, filterOptions)
  % Windowing, band-passing ,centralizing and taking covariance matrix
  signalTemp = preprocessWithParamsCommon(signal1, firstMoment, lastMoment, frqs);
  winoptions.type=1;
	winoptions.alpha=0.5;
	h = wsibandpass(2*pi*filterOptions.lowFrq/frqs, 2*pi*filterOptions.hiFrq/frqs, 
    filterOptions.filterOrder, winoptions);
  signalTemp = centralize(filter(h,1,signalTemp,[],2));
  % covariance matrix for each trial
  [C,T,K] = size(signal1);
  signal2 = zeros(C,C,K);
  for k=1:K
    x = signalTemp(:,:,k);
    signal2(:,:,k) = x*x'/T;
  end
end  
%---------------------------------------------
function signal2 = preprocessWithParamsForCSSSP(signal1, firstMoment, lastMoment, frqs)
  % Windowing and centralizing
  signal2 = centralize(preprocessWithParamsCommon(signal1, firstMoment, 
  lastMoment, frqs));
end
%---------------------------------------------
function signal2 = reshapeAndWindow(signal1, trigger)
  C=60;		% Channels
  frqs=250;	% Hz
  T=1000;
  % Reshape it as a 3D array with each trial in one page
  signal2 = zeros(C,T,numel(trigger));
  for n=1:numel(trigger)
    signal2(:,:,n) = signal1(trigger(n):trigger(n)+T-1,:)';
  end
  % Missing values. MaxMin is better than interpolation here
  signal2 = replaceMissingValuesWithMaxMin(signal2); 
  
end
%---------------------------------------------
function [signalOut,frqs] = downSample(signalIn, downSampleRate)
  winoptions.type=1;
	winoptions.alpha=0.5;
  hlowpass = wsilowpass(pi/downSampleRate, 112, winoptions);
  signalOut=filter(hlowpass,1,signalIn,[],2);
  signalOut = signalOut(:,2:downSampleRate:end,:);
  %disp(['size of signals array after downsampling:',sprintf('%d,',size(signal2))]);
  %fflush(stdout);
  frqs = 250/downSampleRate;
end
%---------------------------------------------
function [signal2,frqs] = totalPreprocess(signal1, trigger)
  % or 3 or 4, so Fmax will be at least 125/4=31.25
  [signal2,frqs] = downSample(reshapeAndWindow(signal1, trigger), 2);
end
