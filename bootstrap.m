function bootstrap   
mypaths = {'/signals', '/CSP', '/cssp', '/csssp', '/eeg', '/linalg','/optim',...
  '/utils','/native', '/statistics'};
% Adds above paths as absolutle ones
currentDir = pwd();
for k=1:numel(mypaths)
  addpath([currentDir, mypaths{k}])
end

if isOctave
  pkg load signal
  pkg load optim
  graphics_toolkit('gnuplot');
end

end