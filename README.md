# Source code for a reasearch on BCI (Brain Computer Interface)

This is the source code implementing my thesis for MSc degree in Computer engineearing - Artificial intelligence. The subject of the research is about a proposing a method to predict movement imaginations based on EEG signals of a person.

## How to use this code
You need to download or clone this repo. You also need to download some data files from [bbci.de competitions](http://www.bbci.de/activities#competition) ,that are not included here. There must be a folder named *datasets* next to the root folder of this source code, with this structure:

```
datasets
└── bci
    └── bbci.de
        ├── BCI-comp-II
        │   ├── set1
        │   │   ├── ia-testdata.txt.gz
        │   │   ├── ia-traindata_0.txt.gz
        │   │   ├── ia-traindata_1.txt.gz
        │   │   ├── ib-testdata.txt.gz
        │   │   ├── ib-traindata_0.txt.gz
        │   │   ├── ib-traindata_1.txt.gz
        │   │   ├── true_labels_1a.txt
        │   │   └── true_labels_1b.txt
        │   ├── set2
        │   │   ├── a
        │   │   │   ├── albany_data_AA.zip
        │   │   │   ├── albany_data_BB.zip
        │   │   │   ├── albany_data_CC.zip
        │   │   │   └── albany_data_matlab.zip
        │   │   │   
        │   │   ├── b
        │   │   │   ├── data
        │   │   │   │   ├── AAS010R01.mat
        │   │   │   │   ├── AAS010R02.mat
        │   │   │   │   ├── AAS010R03.mat
        │   │   │   │   ├── AAS010R04.mat
        │   │   │   │   ├── AAS010R05.mat
        │   │   │   │   ├── AAS011R01.mat
        │   │   │   │   ├── AAS011R02.mat
        │   │   │   │   ├── AAS011R03.mat
        │   │   │   │   ├── AAS011R04.mat
        │   │   │   │   ├── AAS011R05.mat
        │   │   │   │   ├── AAS011R06.mat
        │   │   │   │   ├── AAS012R01.mat
        │   │   │   │   ├── AAS012R02.mat
        │   │   │   │   ├── AAS012R03.mat
        │   │   │   │   ├── AAS012R04.mat
        │   │   │   │   ├── AAS012R05.mat
        │   │   │   │   ├── AAS012R06.mat
        │   │   │   │   ├── AAS012R07.mat
        │   │   │   │   └── AAS012R08.mat
        │   │   │   └── true-labels.txt
        │   │   ├── test_labels_2a.zip
        │   │   └── test_labels_2b.txt
        │   ├── set3
        │   │   ├── dataset.mat
        │   │   └── test_labels.mat
        │   └── set4
        │       ├── iv-sp1s_aa_1000Hz.mat
        │       ├── iv-sp1s_aa.mat
        │       ├── sp1s_aa_test_1000Hz.txt.zip
        │       ├── sp1s_aa_test.txt.zip
        │       ├── sp1s_aa_train_1000Hz.txt.zip
        │       ├── sp1s_aa_train.txt.zip
        │       └── test_labels.txt
        ├── BCI-comp-III
        │   ├── 1
        │   │   ├── competition_test.mat
        │   │   ├── competition_train.mat
        │   │   └── true_labels.txt
        │   ├── 2
        │   │   ├── BCI_Comp_III_Wads_2004.zip
        │   │   ├── true_labels_a.txt
        │   │   └── true_labels_b.txt
        │   ├── 3
        │   │   ├── a
        │   │   │   ├── k3b.mat
        │   │   │   ├── k6b.mat
        │   │   │   ├── l1b.mat
        │   │   │   ├── true_labels_k3b.txt
        │   │   │   ├── true_labels_k6b.txt
        │   │   │   └── true_labels_l1b.txt
        │   │   └── b
        │   │       ├── O3VR.mat
        │   │       ├── S4b.mat
        │   │       ├── true_labels_O3VR.txt
        │   │       ├── true_labels_S4b.txt
        │   │       ├── true_labels_X11b.txt
        │   │       └── X11b.mat
        │   ├── 4
        │   │   ├── IVa
        │   │   │   ├── 100Hz
        │   │   │   │   ├── aa.mat
        │   │   │   │   ├── al.mat
        │   │   │   │   ├── av.mat
        │   │   │   │   ├── aw.mat
        │   │   │   │   └── ay.mat
        │   │   │   ├── true_labels_4a_aa.mat
        │   │   │   ├── true_labels_4a_al.mat
        │   │   │   ├── true_labels_4a_av.mat
        │   │   │   ├── true_labels_4a_aw.mat
        │   │   │   └── true_labels_4a_ay.mat
        │   │   ├── IVb-test
        │   │   │   └── 100Hz
        │   │   │       └── data_set_IVb_al_test.mat
        │   │   ├── IVb-train
        │   │   │   └── 100Hz
        │   │   │       └── data_set_IVb_al_train.mat
        │   │   ├── IVc
        │   │   │   ├── IVc-description.html
        │   │   │   └── IVc-test
        │   │   │       └── 100Hz
        │   │   │           └── data_set_IVc_al_test.mat
        │   │   ├── true_labels_4b.mat
        │   │   └── true_labels_4c.mat
        │   └── 5
        │       ├── labels8_subject1_raw.asc
        │       ├── labels8_subject2_raw.asc
        │       ├── labels8_subject3_raw.asc
        │       ├── preprocessed-features-mat.zip
        │       ├── subject1_mat.zip
        │       ├── subject2_mat.zip
        │       ├── subject3_mat.zip
        │       ├── true_labels_subject1_16Hz_psd.asc
        │       ├── true_labels_subject1_2Hz_psd.asc
        │       ├── true_labels_subject2_16Hz_psd.asc
        │       ├── true_labels_subject2_2Hz_psd.asc
        │       ├── true_labels_subject3_16Hz_psd.asc
        │       └── true_labels_subject3_2Hz_psd.asc
        └── BCI-comp-IV
            ├── 1
            ├── 2
            ├── 3
            │   └── subset_3_mat.zip
            └── 4
```

## Compatibility with Matlab
I tried my best to keep this code compatible with Matlab. But as I don't have access a Matlab version, I can't be sure whether it is really compatinle. For example there are calls to function `fflush`, what I'm not sure if Matlab does support.

## Extra resources
To get and use the latest version of Octave (5.1), see [https://copr.fedorainfracloud.org/coprs/g/scitech/octave5.1/](https://copr.fedorainfracloud.org/coprs/g/scitech/octave5.1/)

