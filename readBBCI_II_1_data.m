function [data, channels, samplePerChannel, frqs] = readBBCI_II_1_data(subset, class, kind, datasetRoot)
% [data, channels, samplePerChannel, frqs] = readBBCI_II_1_data(subset, class, kind, datasetRoot)
%
% Reads data from Set 1 BCI competition II
% subset  :  'a' or 'b'
% class   :  0 or 1
% kind    :  'train' or 'test'
% datasetRoot : Root path of data sets on the disk

%
% Returns 
% data      :  a matrix whose rows are trials; It the case of train data the first column is the class
% label, and other columns are samples of each trial, starting from channel 1 and
% ending with the last channel
% channels  :  Number of channels
% samplePerChannel : Number of samples for each channel in one trial
% frqs			   : Sampling frequency (always 256 Hz)

if length(subset)~=1 || (subset~='a' && subset~='b')
	error 'Invalid subset (Must be ''a'' or ''b'')'
end

if length(class)~=1 || class<0 || class>1
	error 'invalid class (Must be 0 or 1)'
end

if ~ischar(kind)
	error 'Invalid kind. It must be ''train'' or ''test'''
end

frqs = 256;
if isequal(kind,'train')
	name = sprintf('%s/bbci.de/BCI-comp-II/set1/i%c-traindata_%d.txt.gz',
	datasetRoot, subset, class);
else
	name = sprintf('%s/bbci.de/BCI-comp-II/set1/i%c-testdata.txt.gz',
	datasetRoot, subset);
end


# Now open the zipped text file and read its rows
if (subset=='a')
	channels = 6;
	samplePerChannel = 896;
	trialPerClass=[135, 133]; # for classes 0,1
	testTrials = 293;
else
	channels = 7;
	samplePerChannel = 1152;
	trialPerClass=[100, 100]; # for classes 0,1
	testTrials = 180;
end


[fid,message] = fopen(name, 'rbz');
if fid==-1
	disp(message)
	return
end

if isequal(kind,'train')	# Train data
	nrows=trialPerClass(class+1);
	[data,c,message] = fscanf(fid,'%f',[channels*samplePerChannel+1, nrows]);
	if length(message)>0
		disp (message)
	else
		data = data';
	end
else		# Test data
	nrows = testTrials;
	[data,c,message] = fscanf(fid,'%f',[channels*samplePerChannel, nrows]);
	if length(message)>0
		disp (message)
	else
		data=data';
	end
end

fclose(fid);


end
