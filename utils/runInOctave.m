function runInOctave(func)
  ignore_function_time_stamp="all";
  try
    func();
  catch err
    disp(err.message);
    for n=1:numel(err.stack)-1
      disp(sprintf("%s in %s at line %d column %d",
        err.stack(n).name, err.stack(n).file, err.stack(n).line, err.stack(n).column));
    end
  end_try_catch
  ignore_function_time_stamp="system";
endfunction

    