function drawAsImage(x,y,m)
% drawAsImage(x,y,m)
%
% Draws a real valued matrix as a gray image
% x : A vector containing values along X axis
% y : A vector containing values along Y axis
% m : A matrix with as many columns as elements of x and as many rows as elements of y

if ~isvector(x) || ~isvector(y)
	error 'x and y must be vectors'
end

if numel(x)~=size(m,2)
	error 'Vector x must have as many elements as columns of m'
end

if numel(y)~=size(m,1)
	error 'Vector y must have as many elements as rows of m'
end


if isOctave
	graphics_toolkit('gnuplot')
end

zmin=min(m(:));
zmax=max(m(:));

figure,hold on
colormap('gray')
set(gca(),'clim',[zmin,zmax])
image(x, y, (m-zmin)*64/(zmax-zmin));
colorbar
hold off
if isOctave
	graphics_toolkit('qt')
end

end