function horn(secs)
  t = 1:44100*secs;
  y = cos(100 + 0.0075*t./(1+sin(t/7500).^2));
  sound(y,44100);
end
