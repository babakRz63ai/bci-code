function signal2 = interpolateMissingValues(signal1,neighboursCount)
  % signal2 = interpolateMissingValues(signal1,neighboursCount)
  % Imputes some interploated values to missing values in each row of signal1
  %   signal1 : A 3D array with signals in its rows
  %   neighboursCount : Number of neighbour samples to be used to interpolate a missing value
  % Returns:
  %   An array with the same size as signal1 but without missing values
  % FIXME in this implementation we assumed missing values are separated (not 
  % consecutive) and have enough distance from each other.
  
  % FIXME transpose signal to find indices along its rows
  idx = permute(isnan(signal1), [2,1,3]);
  signal2 = signal1;
  if any(idx(:))
    % transpose signal to find indices along its rows
    [col,row,page] = ind2sub(size(idx), find(idx(:)));
    nCols = size(signal1, 2);
    t=1;
    disp(sprintf('interpolateMissingValues will replace %d NaN elements',
      numel(row)));
    fflush(stdout);  
    while t<=numel(row)
      % Finite elements before this NaN value or sequence of NaN values
      finiteBefore = find(isfinite(signal1(row(t), 1:col(t)-1, page(t))));
      % Finite elements after this NaN value or sequence of NaN values
      finiteAfter = find(isfinite(signal1(row(t), col(t)+1:end, page(t)))) + col(t);      
      % Handle each case separately
      if ~isempty(finiteBefore) && ~isempty(finiteAfter)
        % Count of consequent NaN values 
        nnv = finiteAfter(1)-finiteBefore(end)-1;
        % How many neighbours can we take from each side?
        numLeft = min(neighboursCount, finiteBefore(end));
        numRight = min(neighboursCount, nCols-finiteAfter(1)+1);
        % Interpolate
        replaced = interp1(signal1(row(t),
          finiteBefore(end)-numLeft+1:finiteAfter(1)+numRight-1, page(t)), 
          numLeft+1:numLeft+nnv, 'spline');
      elseif ~isempty(finiteBefore)
        % Count of consequent NaN values 
        nnv = nCols-finiteBefore(end);
        % How many neighbours can we take from the left side?
        numLeft = min(neighboursCount, finiteBefore(end));
        % Extrapolate
        replaced = interp1(signal1(row(t), finiteBefore(end)-numLeft+1:end, page(t)),
          numLeft+1:numLeft+nnv, 'spline', 'extrap');
      elseif ~isempty(finiteAfter)
        % Count of consequent NaN values 
        nnv = finiteAfter(1)-1;
        % How many neighbours can we take from the right side?
        numRight = min(neighboursCount, nCols-finiteAfter(1)+1);
        % Extrapolate
        replaced = interp1(signal1(row(t), 1:finiteAfter(1)+numRight-1, page(t)),
          1:finiteAfter(1)-1, 'spline', 'extrap');
      else
        error(sprintf('A whole row of signal in page %d, row %d is NaN',page(t),row(t)));
      end
      
      if ~isempty(finiteBefore)
        signal2(row(t), finiteBefore(end)+1:finiteBefore(end)+nnv, page(t)) = replaced;
      else
        signal2(row(t), 1:nnv, page(t)) = replaced;
      end
      
      t = t+nnv;
      
    end % while
  end % if any is nan
end