function signal2 = replaceMissingValuesWithMaxMin(signal)
% signal2 = replaceMissingValuesWithMaxMin(signal1)
% Replaces missing or NaN values in each row of signal1 by minimum or maximum of
% that row, depending on the immediately previous or next values of the missing
% ones.

signal2 = signal;
% Where is each missing value?
idx = isnan(signal);
[row,col,page] = ind2sub(size(signal),find(idx(:)));

if numel(row)>0
  disp(sprintf('replaceMissingValuesWithMaxMin replaces %d missing values',numel(row)));
  fflush(stdout);
end

% for each missing value, find the first non-missing value right before and after it
for t=1:numel(row)
  % Finite elements before this NaN value or sequence of NaN values
  finiteBefore = find(isfinite(signal(row(t), 1:col(t)-1, page(t))));
  % Finite elements after this NaN value or sequence of NaN values
  finiteAfter = find(isfinite(signal(row(t), col(t)+1:end, page(t)))) + col(t);
  rowAvg = mean(signal(row(t), [finiteBefore,finiteAfter], page(t)));
  rowMax = max(signal(row(t), [finiteBefore,finiteAfter], page(t)));
  rowMin = min(signal(row(t), [finiteBefore,finiteAfter], page(t)));
  if ~isempty(finiteAfter) && ~isempty(finiteBefore)
    if signal(row(t), finiteBefore(end), page(t))>rowAvg
      if signal(row(t), finiteAfter(1), page(t))>rowAvg
        signal2(row(t),col(t),page(t)) = rowMax;
      else
        signal2(row(t),col(t),page(t)) = rowAvg;
      end
    else
      if signal(row(t), finiteAfter(1), page(t))<=rowAvg
        signal2(row(t),col(t),page(t)) = rowMin;
      else
        signal2(row(t),col(t),page(t)) = rowAvg;
      end
    end
  elseif ~isempty(finiteAfter)
    if signal(row(t), finiteAfter(1), page(t))>rowAvg
      signal2(row(t),col(t),page(t)) = rowMax;
    else
      signal2(row(t),col(t),page(t)) = rowMin;
    end
  elseif ~isempty(finiteBefore)
    if signal(row(t), finiteBefore(1), page(t))>rowAvg
      signal2(row(t),col(t),page(t)) = rowMax;
    else
      signal2(row(t),col(t),page(t)) = rowMin;
    end
  else
    error('Could not find a finite value in row %d , page %d',row(t),page(t));
  end
end % for
end