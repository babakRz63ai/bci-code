function [acc,FCL,FCU] = testCSP_BBCI_II_1(silent)
% [acc,FCL,FCU] = testCSP_BBCI_II_1(silent)
%
% Tests 2 class CSP on BBCI-II-1 data



if nargin<1
	silent=false;
end

	if ~silent
		disp(['Loading training data for subset ''a'''])
		fflush(stdout);
	end
	subset='a';
	[data0, channels, samplePerChannel, frqs] = readBBCI_II_1_data(subset, 0, 'train', '../datasets/bci');
	[data1, channels, samplePerChannel, frqs] = readBBCI_II_1_data(subset, 1, 'train', '../datasets/bci');

	traindata = [data0;data1];
	clear data0
	clear data1
	
	
	%figure
	%subplot(2,1,1)
	%plot(traindata(1,2:samplePerChannel+1))
	%title('Original data of Trial 1 channel 1')
	
	% Some outliers in the trial 1 chennel 1
	%traindata(1,101:109)=[30 35 40 45 50 45 40 35 30];
	%subplot(2,1,2)
	%plot(traindata(1,2:samplePerChannel+1))
	%title('Trial 1 channel 1 with high freq. outlier')

	[testdata, channels, samplePerChannel, frqs] = readBBCI_II_1_data(subset, nan, 'test', '../datasets/bci');
	[fid,message] = fopen(sprintf('../datasets/bci/bbci.de/BCI-comp-II/set1/true_labels_1%c.txt',subset),'r');
	if fid==-1
		error(message)
	end
	[trueLabels,c,message]=fscanf(fid,'%d',[size(testdata,1),1]);
	fclose(fid);
	if length(message)>0
		disp (message)
	end
	
	% A band-pass FIR filter
	winoptions.type=1;
	winoptions.alpha=0.54;
	%winoptions.beta=4;
	FCL=2:2:18;
	FCU=20:2:40;
	order=4;
	acc=zeros(numel(FCL), numel(FCU));
	window = hamming(samplePerChannel);
	for fclindex=1:numel(FCL)
	fcl = FCL(fclindex)
	for fcuindex = 1:numel(FCU)
	fcu = FCU(fcuindex)
	fflush(stdout);
	filterLength = 112;
	b = wsibandpass(fcl*2*pi/frqs, fcu*2*pi/frqs, filterLength, winoptions);
	a=1;
	% h = firfs(firfsutil(frqs, [fcl,fcu]));
	% [b,a]=butter(order, [fcl*2/frqs, fcu*2/frqs]);
	
	
	if ~silent
		disp 'Training CSP...'
		fflush(stdout);
	end	
	csp = trainCSP(channels, samplePerChannel, 2,
		@(idx) trialGenerator(idx,traindata,'train', channels, samplePerChannel, a, b, 
			window),
		size(traindata,1), 'normal');
			
	% Saves the result
	% save ('-7' ,sprintf('../resultdata/testCSP1_%c.mat',subset), 'csp')
	
	if ~silent
		% Shows distribution of data
		plotCSPPatternDist(csp)
		title(sprintf('Distribution of mapped patterns by CSP on BBCI-II-1-%c with Butterworth of order %d and Hamming window',
	 	subset, order))
		xlabel('Projection space')
		ylabel('Relative frequency')
		
		disp 'Loading test data...'
		fflush(stdout);
	end
	% Apply on the test data and measure its accuracy
	
	acc(fclindex,fcuindex) = testCSP(csp,@(index) trialGenerator(index, testdata, 'test', channels, samplePerChannel, a, b, window),
		size(testdata,1), trueLabels+1);
	if ~silent
		disp(sprintf('Accuracy on the test data with F_cu %d is %5.2f%%',
			fcu,100*acc))
	end		
	end		% for fcu
	end		% for fcl
end

function [x,label] = trialGenerator(index, data, kind, C, T, a, b, win)
% [x,label] = trialGenerator(index, data, kind, C, T, a, b, window)
% This function is used for CSP algorithm
%
% index : 1,2,3...number of trials
% data  : augmented data whose rows are trials and the columns are samples and the
%   first column may be the label of classes.
% kind  : 'train' or 'test'
% C  	: Number of channels
% T		: Length of each trial
% a,b	: Parameters of the disired filter. a=1 means b is the impulse response
% window: A window of length T (A column vector)
%
% Returns :
%  x : A matrix of C rows (channels) and T columns (Time samples)
%  label : Class label

if isequal(kind,'train')
	x = filtfilt(b,a, repmat(win, 1, C).*reshape(data(index,2:end),T,C))';
	% x = filter(b,a, reshape(data(index,2:end),T,C))';
	label = data(index,1)+1;	% {0,1} -> {1,2}
else
	x = filtfilt(b,a, repmat(win, 1, C).*reshape(data(index,:),T,C))';
	% x = filter(b,a, reshape(data(index,:),T,C))';
end
end