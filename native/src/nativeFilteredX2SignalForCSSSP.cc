#include <octave/oct.h>
#include <octave/dMatrix.h>
#include <octave/dNDArray.h>
#include <cmath>	// for sqrt
#include <cstring>	// for memcpy
#include "openblas/cblas.h"

DEFUN_DLD(nativeFilteredX2SignalForCSSSP, args, nargout,
	"nativeFilteredX2SignalForCSSSP(b, signal)\n\n\
    Calculates Calculates autocorrelation of signals of trials from raw signals and using\n\
    a set of weights composing a CSSSP frequency fliter.\n\n\
    b : vector of weights\n\
    signal : A 3D array with size channels*T*nTrials\n\n\
    Returns :\n\
    An array of size channels times channels times number of trials")
{
	if (args.length() < 2)
		print_usage();
	ColumnVector b = args(0).column_vector_value("nativeFilteredX2SignalForCSSSP : Argument 'b' must be a column vector");
	NDArray signal = args(1).array_value("nativeFilteredX2SignalForCSSSP : signal must be an array");
	
	octave_idx_type channels = signal.rows();
	octave_idx_type T = signal.columns();
	octave_idx_type nTrials = signal.pages();
#ifdef DEBUG
	octave_stdout<<"channels="<<channels<<" , T="<<T<<" , nTrials="<<nTrials<<"\n";
#endif

	NDArray s2(dim_vector(channels, channels, nTrials),0.0);

	octave_idx_type S2PageSize = channels*channels;
	octave_idx_type signalPageSize = channels*T;
	double sqrt_T = std::sqrt(T);	// We divide the auxillary results by this value to get correct average in the final product
	double *signalPage = signal.fortran_vec();	
	for (octave_idx_type t=0;t<nTrials;t++)
	{
		// s = signal(:,:,t)/sqrt_T;
		Matrix s(signal.page(t));
#ifdef DEBUG
		octave_stdout << "Matrix S initially is\n"<<s<<"\n";
#endif

#ifdef OPENBLAS_CONFIG_H
		cblas_dscal(signalPageSize, 1.0/sqrt_T, s.fortran_vec(), 1);
#else
		double *sp0 = s.fortran_vec();
		for (int n=0;n<signalPageSize;n++,sp0++)
			*sp0 /= sqrt_T;
#endif
#ifdef DEBUG
		octave_stdout << "Matrix S at t="<<t+1<<" before tau loop is\n"<<s<<"\n";
#endif
		double *sp = s.fortran_vec();
		for (octave_idx_type tau=1;tau<b.numel();tau++)
		{
			//s(:,tau+1:end) += b(tau+1)*signals(:,1:end-tau,t)/sqrt_T;
			//double *sp = s.fortran_vec()+tau*channels;
			sp += channels;
			octave_idx_type count = channels*(T-tau);

#ifdef DEBUG
			octave_stdout << "Adding "<<b(tau)/sqrt_T<<" of s(:,"<<tau+1<<":"<<T<<")\n";
#endif

#ifdef OPENBLAS_CONFIG_H
			cblas_daxpy(count, b(tau)/sqrt_T, signalPage, 1, sp, 1);
#else
			double *signalPnt=signalPage+tau*channels;
			double bTau = b(tau)/sqrt_T;
			double *spTemp = sp;
			for (octave_idx_type n=0;n<count;n++,signalPnt++,spTemp++)
				*spTemp += bTau * (*signalPnt);
#endif
#ifdef DEBUG
		octave_stdout << "Matrix S at t="<<t+1<<" after addition is\n"<<s<<"\n";
#endif
		}	// for tau
#ifdef DEBUG
		octave_stdout << "Matrix S at t="<<t+1<<" after tau loop is\n"<<s<<"\n";
#endif
		// s2(:,:,t) = s*s'/T;
		Matrix temp = s*s.transpose();	// Divided by T already

#ifdef DEBUG
		octave_stdout << "Matrix temp is \n"<<temp<<"\n";
#endif

		// cblas_dcopy is faster than memcpy	
#ifdef OPENBLAS_CONFIG_H
		cblas_dcopy(S2PageSize, temp.data(), 1, s2.fortran_vec()+t*S2PageSize, 1);
#else
		std::memcpy(s2.fortran_vec()+t*S2PageSize, temp.data(), S2PageSize*sizeof(double));
#endif
		// Summations instead of multiplications
		signalPage += signalPageSize;
	}	// for t
	
	return octave_value(s2);		
}
