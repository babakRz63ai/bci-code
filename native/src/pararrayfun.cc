#include <octave/oct.h>
#include <octave/parse.h>
#include <octave/dColVector.h>
#include <pthread.h>
#include <cstring>
#include <vector>
#include "openblas/cblas.h"


typedef struct {
	octave_function *function;	// May be NULL, in such a case the name of the function (field below) will be used
	std::string funcName;
	// Eech value list is for one function call
	std::vector<octave_value_list> arguments;
	int nargout;
#ifdef DEBUG
	int pid;
#endif		
} FunctionHandleParams;

typedef std::vector<ColumnVector> ArrayOfOctaveColumnVectors;

/**
* Thrad function to evaluate an octave function over a set of arguments each time. 
* @param params points to a FunctionHandleParams structure.
* @return A vector of octave column vectors 
*/
void *threadFunction(void *params)
{
	FunctionHandleParams *fcnp = reinterpret_cast<FunctionHandleParams*>(params);
	// Initililizes enough output arguments. Address of this vector will be returned	
	ArrayOfOctaveColumnVectors *output = new ArrayOfOctaveColumnVectors(fcnp->nargout);
	// reserve enough space (as many as fcnp->arguments.size()) in each (*output)[j]
	for (int j=0;j<fcnp->nargout;j++) {
#ifdef DEBUG
		octave_stdout<<"Job "<<fcnp->pid<<" reserves space for storing "<<fcnp->arguments.size()<<" elements in (*output)["<<
		j<<"]\n";
#endif	
		(*output)[j].resize(fcnp->arguments.size());
	}
	octave_value_list retvals;
	int count = fcnp->arguments.size();
	for (int i=0;i<count;i++) 
	{
		bool retry = true;
		while (retry)
		try 
		{
			if (fcnp->function)
				retvals = octave::feval(fcnp->function, fcnp->arguments[i], fcnp->nargout);
			else
				retvals = octave::feval(fcnp->funcName, fcnp->arguments[i], fcnp->nargout);
		
			for (int j=0;j<fcnp->nargout;j++)
			{
				(*output)[j](i) = retvals(j).double_value();
			}
			retry = false;
		}
		catch (std::exception ex)
		{
#ifdef DEBUG
			octave_stdout<<"Exception in job "<<fcnp->pid<<" for argument set "<<i+1<<" : "<<ex.what()<<"\n";
#endif
		}
		catch (...)
		{
#ifdef DEBUG		
			octave_stdout<<"Unknown exception in job "<<fcnp->pid<<" for argument set "<<i+1<<"\n";
#endif			
		}
	}
#ifdef DEBUG
			octave_stdout<<"Job "<<fcnp->pid<<" returns\n";
#endif
	return output;
}

//---------------------------------------------

DEFUN_DLD(pararrayfun, args, nargout,
"[O1, O2, ...] = pararrayfun (NPROC, FUN, A1, A2, ...)\n\
Evaluates a function for corresponding elements of some arrays.\n\n\
Returns : O1,O2,,,O(n), arrays of outputs of each call of FUN")
{
	int nargin = args.length();
	if (nargin < 3)
		print_usage();
	
	int nprocs = args(0).int_value("Number of desired processes must be integer");
	if (nprocs<=1)
		error("Number of desired processes must be a positive integer greater than one");
	
	octave_function *fcn = NULL;
	std::string fcnname;

	if (args(1).is_function_handle () || args(0).is_inline_function ())
	{
		fcn = args(1).function_value();
	}
	else if (args(1).is_string ())
	{
		fcnname = args(1).string_value();
	}
	else
		error ("pararrayfun: FUN must be string, inline, or function handle");
	
	OCTAVE_LOCAL_BUFFER(ColumnVector ,orgArguments, nargin-2);
	for (int i=0;i<nargin-2;i++) 
	{	
		orgArguments[i] = args(i+2).column_vector_value("Only column vectors of arguments are supported");
		if (orgArguments[i].numel()==0)
			error("Argument vectors can't be empty. Make sure you have provided column vectors. You have provided arguments %d such as isnumeric=%d, is_double_type=%d, is_matrix_type=%d", 
	i+3, args(i+2).isnumeric(), args(i+2).is_double_type(), args(i+2).is_matrix_type());
		
		if (orgArguments[i].columns()>1)
			error("Only column vectors of arguments are supported, while argument %dth has %d columns",
			i+3, orgArguments[i].columns());
				
		if ((i>0) && (orgArguments[i].numel()!=orgArguments[0].numel()))
			error("All argument vectors must have the same size");

#ifdef DEBUG
		octave_stdout<<"Argument array "<<i+1<<" has "<<orgArguments[i].rows()<<" rows\n";
#endif
	}
	
	// Load balancing
	const int TotalCalls = orgArguments[0].numel();
	int argsPerProc = TotalCalls / nprocs;
	int reminder = TotalCalls % nprocs;
	// An array of int
	OCTAVE_LOCAL_BUFFER(int, load, nprocs);
	for (int i=0;i<nprocs;i++)
		load[i] = argsPerProc;
	// If there are	reminding jobs, distribute them as equally as possible
	for (int i=0;i<reminder;i++)
		load[i]++;
#ifdef DEBUG
	for (int i=0;i<nprocs;i++)
		octave_stdout<<"Load balancing assigned "<<load[i]<<" jobs to process "<<i+1<<"\n";
#endif
	
	
	// Arguments for each thread
	OCTAVE_LOCAL_BUFFER(FunctionHandleParams, fhparams, nprocs);

	// Array of threads
	OCTAVE_LOCAL_BUFFER(pthread_t, threads, nprocs);

	// There will be on output arguments at least
	if (nargout==0)
		nargout = 1; 
	
	// Counts each touple of original arguments
	int callCounter = 0;
	
	for (int i=0;i<nprocs;i++)
	{
		// Set arguments for each thread
		if (fcn)
			fhparams[i].function = fcn;
		else
		{
			fhparams[i].function = NULL;
			fhparams[i].funcName = fcnname;
		}
		fhparams[i].nargout = nargout;
#ifdef DEBUG
		fhparams[i].pid = i+1;
#endif		
		
		// Divide the original given arguments into some segments
		for (int j=0;j<load[i];j++)
		{			
			octave_value_list valueList(nargin-2);
			for (int k=0;k<nargin-2;k++)
				valueList(k) = orgArguments[k].elem(callCounter);
			callCounter++;
			fhparams[i].arguments.push_back(valueList);
		}
		
		// Create and run a thread for this set of function calls
		pthread_create(threads+i, NULL, &threadFunction, fhparams+i);
	}
	
	// The temporary results
	OCTAVE_LOCAL_BUFFER(double*, tempResults, nargout);
	for (int k=0;k<nargout;k++)
		tempResults[k] = new double[TotalCalls];
	

	// Wait for child threads
	callCounter = 0;
	for (int i=0;i<nprocs;i++)
	{
		ArrayOfOctaveColumnVectors *output = NULL;	// Will point to a vector of column vectors
#ifdef DEBUG
		octave_stdout<<"Waiting for job "<<i+1<<" to finish...\n";
#endif
		int errorCode = pthread_join(threads[i], (void**)&output);
		if (errorCode)
			error("Error %d happened when joining child thread %d",errorCode, i);
		//output = (ArrayOfOctaveColumnVectors*) threadFunction(fhparams+i);
#ifdef DEBUG
		octave_stdout<<"Job "<<i+1<<" returns "<<output->size()<<" argument vectors\n";
		for (size_t t=0;t<output->size();t++)
			octave_stdout<<"Vector "<<t+1<<" has "<<(*output)[t].rows()<<" rows and "<<(*output)[t].columns()<<" columns\n";
#endif
		// Copies the values from each vector of sub results to its corresponding main vector		
		for (int k=0;k<nargout;k++) {
#ifdef DEBUG
			octave_stdout<<"Dimensions of source of assignment are "<<(*output)[k].dims().str()<<"\n";
			
#endif
			//tempResults[k].assign(index, (*output)[k]);
#ifdef OPENBLAS_CONFIG_H
			cblas_dcopy(load[i], (*output)[k].data(), 1, tempResults[k]+callCounter, 1);
#else			
			std::memcpy(tempResults[k]+callCounter, (*output)[k].data(), load[i]*sizeof(double));
#endif			
			
		}
		
		delete output;
		callCounter += load[i];
	}
	
	// The result
	octave_value_list results(nargout);
	for (int k=0;k<nargout;k++) {
		ColumnVector column(TotalCalls);
#ifdef OPENBLAS_CONFIG_H
		cblas_dcopy(TotalCalls, tempResults[k], 1, column.fortran_vec(), 1);
#else		
		std::memcpy(column.fortran_vec(), tempResults[k], TotalCalls*sizeof(double));
#endif		
		results(k) = column;
	}	

	//delete[] fhparams;
	
	//delete[] threads;
	
	//delete[] load;
	
	for (int i=0;i<nargout;i++)
		delete[] tempResults[i]; 
	//delete[] tempResults;
	
	//delete[] orgArguments;
	
	return results;
}

/* Unit tests
%!error (pararrayfun())
%!error (pararrayfun(1))
%!error (pararrayfun(1,2))
%!error (pararrayfun(1,2,3))
%!error (pararrayfun(2,[],2))
%!error (pararrayfun(1,@sin,eye(2,2)))
%!error (pararrayfun(1,@sin,1:10))
%!assert (pararrayfun(2,@sin,eye(2,2)), sin([1;0;0;1]))
%!assert (pararrayfun(2,@sin,1:10), sin(1:10)')
%!assert (pararrayfun(2,@sin,(1:10000)'), sin((1:10000)'))
%!assert (pararrayfun(3,@sin,(1:10000)'), sin((1:10000)'))
%!assert (pararrayfun(4,@sin,(1:10000)'), sin((1:10000)'))
%!assert (pararrayfun(2,'sin',(1:10000)'), sin((1:10000)'))
%!assert (pararrayfun(3,'sin',(1:10000)'), sin((1:10000)'))
%!assert (pararrayfun(4,'sin',(1:10000)'), sin((1:10000)'))
*/

/*TODO Calling this function with a function handle which takes more than one argument can carsh Octave. There seems a bug in the code.
If someone can resolve this bug, then they can uncomment these unit tests too:
%assert (pararrayfun(2, @(x,y) sin(x)+sqrt(y), linspace(-4*pi,4*pi,10000)', linspace(1,333,10000)'), sin(linspace(-4*pi,4*pi,10000)')+sqrt(linspace(1,333,10000)'))
%assert (pararrayfun(3, @(x,y) sin(x)+sqrt(y), linspace(-4*pi,4*pi,10000)', linspace(1,333,10000)'), sin(linspace(-4*pi,4*pi,10000)')+sqrt(linspace(1,333,10000)'))
%assert (pararrayfun(4, @(x,y) sin(x)+sqrt(y), linspace(-4*pi,4*pi,10000)', linspace(1,333,10000)'), sin(linspace(-4*pi,4*pi,10000)')+sqrt(linspace(1,333,10000)'))
*/ 

