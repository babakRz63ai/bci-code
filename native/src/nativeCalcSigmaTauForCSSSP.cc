#include <octave/oct.h>
#include <octave/dMatrix.h>
#include <octave/dNDArray.h>
#include <algorithm>
#include "openblas/cblas.h"

class IsLabel
{
	private:
		octave_idx_type label;
	public:
		IsLabel(octave_idx_type aLebal) : label(aLebal) {}
		bool operator()(octave_idx_type value) const
		{return value==label;}
};

DEFUN_DLD(nativeCalcSigmaTauForCSSSP, args, nargout,
  "nativeCalcSigmaTauForCSSSP(signal, classLabels, SpectFltrLength)\n\n\
  Calculates all Sigma^Tau matrices for an iteration of training CSSSP.\n\n\
  signal : A 3D array with signal of each channel in each of its rows\n\
  classLabels : Column vector of class labels\n\
  SpectFltrLength : Length of disired spectral filter\n\n\
  Returns : A 4D array with size channels x channels x last Tau x number of classes")
{
#ifndef NOVALIDATION
	if (args.length() < 3)
         print_usage();
#endif
	NDArray signal = args(0).array_value("nativeCalcSigmaTauForCSSSP : signal must be an array");
	ColumnVector classLabels = args(1).column_vector_value("nativeCalcSigmaTauForCSSSP : classLabels must be a column vector");
	octave_idx_type SpectFltrLength = args(2).int_value("nativeCalcSigmaTauForCSSSP : SpectFltrLength must be an integer");
	octave_idx_type channels = signal.rows();
	octave_idx_type T = signal.columns();
	octave_idx_type nTrials = signal.pages();
	if (classLabels.numel()!=nTrials)
		print_usage();
	
	octave_idx_type lastTau = std::min(T-1, SpectFltrLength-1);
	
	// Class labels	
	double *dp = classLabels.fortran_vec();
	octave_idx_type *ilabels = new octave_idx_type[nTrials];
	octave_idx_type *iterator = ilabels;
	*iterator = static_cast<octave_idx_type>(*dp);
	octave_idx_type maxLabel = *iterator;
	dp++;
	iterator++;
	for (octave_idx_type t=1;t<nTrials;t++,iterator++,dp++) {
		*iterator = static_cast<octave_idx_type>(*dp);
		maxLabel = std::max(maxLabel, *iterator);
	}
	
	octave_idx_type nClasses = maxLabel;

	NDArray preCalculatedSigmaTau(dim_vector(channels,channels,lastTau+1,nClasses), 0.0);
	
	// Transposing each page of signal 
	Array<octave_idx_type> transposePages(dim_vector(1,3));
	transposePages(0) = 2-1;
	transposePages(1) = 1-1;
	transposePages(2) = 3-1;
	NDArray sigtrans(signal.permute(transposePages, false));
	
	
	// Index for the array above in each step
	//Array<idx_vector> indices(dim_vector(1,4));
	//indices(0) = idx_vector::colon;
	//indices(1) = idx_vector::colon;
	Array<idx_vector> directSIndex(dim_vector(1,3)); // in m-file  ":,1:end-tau,t"
	directSIndex(0) = idx_vector::colon;
	
	Array<idx_vector> transposedSIndex(dim_vector(1,3));	// in m-file : "tau+1:end,:,t"
	transposedSIndex(1) = idx_vector::colon;
	
	const int pageJump = preCalculatedSigmaTau.rows()*preCalculatedSigmaTau.columns();
	const int blockJump = pageJump*preCalculatedSigmaTau.pages();

#ifdef DEBUG
	octave_stdout<<"nativeCalcSigmaTauForCSSSP : Page size is "<<pageJump<<"\nBlock size is "<<blockJump<<"\n";
#endif

	idx_vector *preCreatedDirectSIndex = new idx_vector[lastTau+1]; 
	idx_vector *preCreatedTransposedSIndex = new idx_vector[lastTau+1];
	for (octave_idx_type tau=0; tau<=lastTau; tau++)
	{
		preCreatedDirectSIndex[tau] = idx_vector(0,T-tau);
		preCreatedTransposedSIndex[tau] = idx_vector(tau,T);
	}

	for (octave_idx_type t=0; t<nTrials; t++)
	{
		// A scalar as the forth index
		//indices(3) = idx_vector(ilabels[t]-1);
		// Also third index of these two
		directSIndex(2) = transposedSIndex(2) = idx_vector(t);
#ifdef DEBUG
		octave_stdout<<"directSIndex(2) = transposedSIndex(2) = "<<directSIndex(2)<<"\n";
#endif
		idx_vector *preCreatedDirectSIndexIterator = preCreatedDirectSIndex;
		idx_vector *preCreatedTransposedSIndexIterator = preCreatedTransposedSIndex;
		for (octave_idx_type tau=0; tau<=lastTau; tau++)
		{
			// A scalar (tau+1) for the third index
			//indices(2) = idx_vector(tau);
			directSIndex(1) = *preCreatedDirectSIndexIterator++;
			transposedSIndex(0) = *preCreatedTransposedSIndexIterator++;
#ifdef DEBUG
			octave_stdout<<"tau="<<tau<<"\ndirectSIndex(1) ="<<directSIndex(1)<<"\ntransposedSIndex(0) = "<<transposedSIndex(0)<<"\n";
#endif
			//preCalculatedSigmaTau.index(indices) += Matrix(signal.index(directSIndex)) * Matrix(signal.index(transposedSIndex));
			Matrix f1(signal.index(directSIndex));
			Matrix f2(sigtrans.index(transposedSIndex));
			Matrix C = f1*f2;
#ifdef DEBUG
			octave_stdout <<"f1:\n"<<f1<<"\nf2:\n"<<f2<<"\nC :\n"<< C<<"\nAdding C to page "<<tau<<" in block "<<ilabels[t]-1<<"\n";
#endif
			double *tp = preCalculatedSigmaTau.fortran_vec() + blockJump*(ilabels[t]-1) + pageJump*tau;
			const double *cp = C.data();
#ifdef OPENBLAS_CONFIG_H
			// Adds each *cp to the corresponding *tp 
			cblas_daxpy(pageJump, 1.0, cp, 1, tp, 1);
#else
			for (int i=0;i<pageJump;i++,tp++,cp++)
				*tp += *cp;
#endif
		}
	}

	

	
	// preCalculatedSigmaTau += permute(preCalculatedSigmaTau, [2,1,3,4]);
	Array<octave_idx_type> permitation(dim_vector(1,4));
	permitation(0)=2-1;
	permitation(1)=1-1;
	permitation(2)=3-1;
	permitation(3)=4-1;
	preCalculatedSigmaTau += NDArray(preCalculatedSigmaTau.permute(permitation,false));
	
	// Averaging, or dividing block of each class label by number of trials in that class
	double *tp = preCalculatedSigmaTau.fortran_vec(); 
	for (int y=1;y<=nClasses;y++)
	{
		double count = std::count_if(ilabels, ilabels+nTrials, IsLabel(y));
#ifdef DEBUG
		octave_stdout<<"nativeCalcSigmaTauForCSSSP : Count of trials with label "<<y<<" in "<<nTrials<<" trials is "<<count<<"\n";
#endif

#ifdef OPENBLAS_CONFIG_H
		cblas_dscal(blockJump, 1.0/count, tp, 1);
		tp += blockJump;
#else
		for (int n=0;n<blockJump;n++,tp++)
			*tp /= count;
#endif
	}
	
	delete[] ilabels;
	delete[] preCreatedDirectSIndex;
	delete[] preCreatedTransposedSIndex; 
	
#ifdef DEBUG	
	preCalculatedSigmaTau.print_info(octave_stdout, "nativeCalcSigmaTauForCSSSP : ");
#endif
	return octave_value(preCalculatedSigmaTau);
}
