#include <octave/oct.h>
#include <octave/dMatrix.h>
#include <octave/dNDArray.h>

DEFUN_DLD(multiplyAndSumSlices, args, nargout,
	"multiplyAndSumSlices(A,B)\n\n\
	Multiplies each planar slice of three-dimensional arrays A and B and summurizes all the products\n\
	Each slice of A (i.e each A(:,:,k))  must be multiplicable by each slice of B (i.e each B(:,:,k))\n\n\
	Returns : A matrix with size rows(A) by columns(B)")
{
#ifndef NOVALIDATION
	if (args.length() != 2)
         print_usage();
#endif
	NDArray A = args(0).array_value();
    NDArray B = args(1).array_value();
#ifndef NOVALIDATION
	if ((A.ndims()!=3) || (B.ndims()!=3))
	{
		octave_stdout << "Error : A and B must have 3 dimensions each\n";
		print_usage();
	}
	
	if (A.columns()!=B.rows())
	{
		octave_stdout << "Error : A has "<<A.columns()<<" columns while B has "<<B.rows()<<" rows\n";
		print_usage();
	}

	if (A.pages()!=B.pages())
	{
		octave_stdout << "Error : A has "<<A.pages()<<" pages while B has "<<B.pages()<<" pages\n";
		print_usage();
	}
#endif
	// A matrix with rows(A) by columns(B) elements
	Matrix C(dim_vector(A.rows(), B.columns()), 0.0);
	for (int k=0;k<A.pages();k++)
	{
		C += Matrix(A.page(k)) * Matrix(B.page(k));
	} // for page
	return octave_value(C);
}

/* Unit test
%!error(multiplyAndSumSlices(1))
%!error(multiplyAndSumSlices(1,1))
%!error(multiplyAndSumSlices([0 0],[0 0]))
%!error(multiplyAndSumSlices(ones(2),ones(2)))
%!assert(multiplyAndSumSlices(ones(2,2,3),ones(2,2,3)),[6 6;6 6]);
%!assert(multiplyAndSumSlices(repmat(eye(3,3),[1,2,4]), ones(6,3,4)),8*ones(3,3));
*/

