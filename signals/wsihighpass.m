function hp = wsihighpass(wc,M,winoptions)
% h = wsihighpass(wc,M,winoptions)
%
% Produces a high pass filter by windowing and shifting an ideal filter
%
% wc        :  Scalar, Cut frequency in rad/sec
% M         :  Scalar, Disired length of the filter
% winoptions:  Structure, Options to generate a window
%    This structure must have an integer type (1 for Hamming-Hann family and 
%    2 for Kaiser family), a real factor named alpha for Hamming-Hann windows or
%    beta for Kaiser windows. 
% 
if nargin<3
	error "Insufficient arguments given"
end

if numel(wc)>1 | numel(M)>1
	error "Illegal arguments"
end

if mod(M,2)~=0
	error "Length should be even"
end

if wc<=0 | pi<wc
	error "Cutting frequency is out of range"
end

n=-M/2 : M/2;

% Impuls response of the initial ideal filter
h = (n==0) - wc/pi*sinc(n*wc/pi);

hp = wsifreqpassworker(h, M, winoptions);

end