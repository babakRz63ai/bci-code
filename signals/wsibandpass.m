function hp = wsibandpass(wcl,wcu,M,winoptions)
% hp = wsibandpass(wcl,wcu,M,winoptions)
% Produces a low pass filter by windowing and shifting an ideal filter
%
% wcl,wcu   :  Scalar, Cut frequencies in rad/sec
% M         :  Scalar, Disired order of the filter
% winoptions:  Structure, Options to generate a window
%    This structure must have an integer type (1 for Hamming-Hann family and 
%    2 for Kaiser family), a real factor named alpha for Hamming-Hann windows or
%    beta for Kaiser windows. 
% 

if nargin<4
	error "Insufficient arguments given"
end

if numel(wcl)>1 | numel(wcu)>1 | numel(M)>1
	error "Illegal arguments"
end

if mod(M,2)~=0
	error "The order should be even"
end

if wcl<=0 | pi<wcl | wcu<=0 | pi<wcu
	error "Cutting frequencies are out of range"
end

n=-M/2 : M/2;
% Impuls response of the initial ideal filter
h=wcu/pi*sinc(n*wcu/pi) - wcl/pi*sinc(n*wcl/pi);

hp = wsifreqpassworker(h, M, winoptions);

end