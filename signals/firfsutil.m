function fsamples = firfsutil(fs, fc, kind, dlen)
% h = firfsutil(fs, fc, 'low')
% h = firfsutil(fs, fc, 'high')
% h = firfsutil(fs, [fcl, fcu])
% h = firfsutil(..., N)
%
% Utility to disign FIR filters from frequency samples.
% 
% fs       : Frequency in Hz in which the original signal has been sampled
% fc       : The cut off frequency for low-pass or high pass filters
% fcl, fcu : The cut off frequencies for a band pass filter
% N        : The desired length of vector of samples
%
% Returns  : Samples of the frequency response

if nargin<2
	error "Too few arguments"
end

if numel(fs)>1 | numel(fc)>2
	error "Illegal arguments"
end

if fs<2*fc
	error "The sampling frequency is not big enough"
end

if numel(fc)==1 & nargin<3
	error "Kind of the filter must be given"
end

if numel(fc)==2
	d = gcd(fc(1),fc(2));
	dp = gcd(fc(1), fs);
	l = lcm(fc(1)/d, fc(1)/dp);
	if nargin<=3
		Nmin=5;
	else
		Nmin = max(dlen, 5);	
	end
	
	t = max(ceil(3/l), ceil(Nmin*fc(1)/(l*fs)));
	k1 = t*l;
	k2 = k1*fc(2)/fc(1);
	N = k1*fs/fc(1);
	if k1<3 | k2>N-2
		error "Insufficient length of the filter"
	end
	fsamples = [zeros(1,k1-2), 0.4, ones(1, k2-k1+1), 0.4, zeros(1,N-k2-1)];
else
	if nargin==3
		Nmin = 3;
	else
		Nmin = max(dlen, 3);
	end
	if isequal(kind,'low')
		kmin=1;
	else
		kmin=3;
	end
	d=gcd(fc,fs);
	t = max(ceil(d*kmin/fc), ceil(d*Nmin/fs));
	k = t*fc/d;
	N = k*fs/fc;
	if isequal(kind,'low')
		fsamples = [ones(1,k), zeros(1,N-k)];
		if k<N
			fsamples(k+1) = 0.4;
		end
	elseif isequal(kind,'high')
		fsamples = [zeros(1,k-2), 0.4, ones(1,N-k+1)];
	end
end

if nargin>=4 && dlen<N
	hprime = zeros(1,dlen);
	if mod(N,dlen)>0
		fixed=fix(N/dlen) - 1;	% At least 0
		fract1=relative(0,1);
		addfraction = relative(mod(N,dlen),dlen);
		fract2=addfraction;
		
		firstpos=1;
		for j=1:dlen
			hprime(j) = ((1-realValue(fract1))*fsamples(firstpos) + 
				sum(fsamples(firstpos+1:firstpos+fixed)) +
				realValue(fract2)*fsamples(firstpos+fixed+1))*dlen/N;
			firstpos = firstpos+fixed+1;
			fract1 = addRelatives(fract1,addfraction);
			fract2 = addRelatives(fract2,addfraction);
			if fract1.nom>=fract1.denom
				fract1=addRelatives(fract1, relative(-1,1));
				fract2=addRelatives(fract2, relative(-1,1));
				firstpos=firstpos+1;
			end
		end % for j
	else	% N is a factor of dlen
		d=fix(N/dlen);
		for j=1:dlen
			hprime(j) = mean(fsamples(1+(j-1)*d:j*d));
		end
	end	
	fsamples = hprime;
end	 % Down-sampling
end

% Creares a relative number
function r = relative(nom,denom)
	if denom==0
		error 'Denominator can''t be zero'
	end
	if nom!=0
		d=gcd(nom,denom);
		r.nom = nom/d;
		r.denom=denom/d;
	else
		r.nom=0;
		r.denom=1;
	end
end

% Sum of two relative numbers
function c=addRelatives(a,b)
	c = relative(a.nom*b.denom + a.denom*b.nom, a.denom*b.denom);
end

% Real value of a relative number
function rv = realValue(r)
	rv = r.nom/r.denom;
end