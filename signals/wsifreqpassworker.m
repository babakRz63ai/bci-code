function hp = wsifreqpassworker(h,M,winoptions)

if ~isstruct(winoptions) | ~isfield(winoptions,'type')
	error "Illegal window options"
end

n=-M/2 : M/2;
% The window
if winoptions.type==1
	w = winoptions.alpha + (1-winoptions.alpha)*cos(2*pi*n/M);
else
	w = kaiser(M+1, winoptions.beta)'; % We want a row vector
end

% Windowing
hp = h.*w;

end