function h = firfs(samples)
% Usage : h = firfs(samples)
% where samples - is a row vector of M equi-spaced, real values
% of the freq. response magnitude.
% The samples are interpreted as being equally spaced around
% the top half of the unit circle at normalized (in terms of
% the Nyquist frequency f_N) frequencies from
% 0 to 2(M-1)/(2M-1) x f_N,
% or at frequencies 2k/(2N-1)xf_N for k = 0...M-1
% Note: Because the length is odd, the frequency response
% is not specified at f_N.
% h - is the output impulse response of length 2M-1 (odd).
% The filter h is real, and has linear phase, i.e. has symmetric
% coefficients obeying h(k) = h(2M+1-k), k = 1,2,...,M+1.

M = length(samples);
N = 2*M -1;
H_d = zeros(1,N);
%
% We want a causal filter, so the resulting impulse response will be shifted
% (N-1)/2 to the right.
% Move the samples into the upper and lower halves of H_d and add the
% linear phase shift term to each sample.
%
%Phi = pi*(N-1)/N;
%H_d(1) = samples(1);
%for k = 2:N/2-1
%	Phase = exp(-i*(k-1)*Phi);
%	H_d(k) = samples(k)*Phase;
%	H_d(N+2-k) = samples(k)*conj(Phase);
%end
K=0:M-1;
H_d(K+1) = samples(K+1).*exp(-j*pi*K*(N-1)/N);
K=1:M-1;
H_d(N-K+1) = conj(H_d(K+1));

%
% Use the inverse DFT to define the impulse response.
%
h = real(ifft(H_d));
end
