function R = testCSSP(cssp,orgTrialGenerator,nTrials,CC,CCMode,extra,trueLabels)
% R = testCSSP(C,T,trialGenerator,nTrials,CC,CCMode,extra=[],trueLabels=[])
%
% Tests a CSP on some trials and returns its accuracy
%
% csp : Trained CSP
% nTrials  : Number of all trials
% trilaGenerator : A function handle which accepts a single index and returns a 
% trial (a matrix with C rows and T columns)
% trialGenerator must be defined as follows:
% 
% function x = trialGenerator(index) ...
% 
% CC : A single classifier in case of binary problems or SIM extension, or a 
%     cell array of binary classifiers for OVR models; as returned by  train_sc 
%     from Biosig package
% CCmode : Classifier model, as used in train_sc
% extra : Extra info. from training
% trueLabels : A vector of true labels , one of {1,2,3...n}
%
% Retuens : A struct containing these fields:
%   R.output     	output: "signed" distance for each class. 
%		  This represents the distances between sample D and the separating hyperplane
%		  The "signed distance" is possitive if it matches the target class, and 
%		  and negative if it lays on the opposite side of the separating hyperplane. 
%   R.classlabel 	class for output data
% Also with this fields if true labels were given:
%       R.kappa 	Cohen's kappa coefficient
%       R.ACC   	Classification accuracy 
%       R.H     	Confusion matrix  

if nargin<5
  error 'Insufficient arguments'
end

if nargin<6
  extra=[];
end

if nargin<7
  trueLabels=[];
end
% We use C/2 because channels has been duplicated during training CSSP
s2 = getCSSPSecondMoments(cssp.tau, cssp.C/2, cssp.T, [1:nTrials], orgTrialGenerator); 

R = testCSP(cssp, @(index) simpleTrialGenerator(index, [1:nTrials], s2, []),
	nTrials, CC, CCMode, extra, trueLabels); 

end
