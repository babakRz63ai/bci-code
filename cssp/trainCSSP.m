function csp = trainCSSP(C,T,nclasses,orgTrialGenerator, 
  nTrials, model, CCModel, numSelected, options)
% csp = trainCSSP(C,T,trialGenerator, nTrials, 
%       model='OVR.CSP3', CCModel=struct(...), numSelected=2, options=[])
%
% Executes all steps for training a CSSP from trials
%
% C : Number of channles
% T : Length of each trial in time steps
% nclasses : Number of different classes
% model    : CSP model (used for multi-class cases)
% CCModel  : Classifier model. This struct by default has fields TYPE='FLDA' and
%   hyperparameter.gamma = 0.1;
% nTrials  : Number of all trials
% orgTrialGenerator : A function handle which accepts a single index and 
% returns the filtred signal of a trial (a matrix with C rows and T 
% columns) and its class (1,2,3,...nclasses) 
% orgTrialGenerator must be defined as follows:
% 
% function [x,label] = trialGenerator(index) ...
%
% distAssumption : Assumption about distribution of data. It can only be 'normal' 
% for now.
% 2/3 of the training data is used for actual training and 1/3 of it is used for 
% validation in each iteration of the calibration
%
% options can be a struct containing boolean verbose and scalar tauStep

if nargin<5
	error 'Insufficient input arguments, at least six arguments are needed'
end

if nargin<6
	model = 'OVR.CSP3';
end

if nargin<7
  CCModel.TYPE = 'FLDA';
  CCModel.hyperparameter.gamma = 0.1;
end

if nargin<8
  numSelected = 2;
end

if nargin<9 || ~isstruct(options)
  options.verbose = false;
  options.tauStep = 1;
  options.checkNaNs = false;
else
  if ~isfield(options,'verbose')
    options.verbose = false;
  end
  if ~isfield(options,'tauStep')
    options.tauStep=1;
  end
  if ~isfield(options,'checkNaNs')
    options.checkNaNs = false;
  end
  if ~isfield(options,'firstTau')
    options.firstTau=1;
  end
  if ~isfield(options,'lastTau')
    options.lastTau=Inf;
  end
  
end


crossValidationRatio=3;
shuffler = 1:nTrials;  %permutation(nTrials);
sizeOfValidation = fix(nTrials/crossValidationRatio);
sizeOfTrianing = nTrials-sizeOfValidation;

% Selects the best tau
%bestTau = optSimAnnealing(@(tau) objectiveTauFunc(tau, crossValidationRatio,C,T,
%    nclasses, sizeOfValidation,shuffler,orgTrialGenerator,sizeOfTrianing, model, 
%    numSelected, CCModel, verbose),
%  fix(rand*T/2), 1, fix(T/2), 1);

% using tau causes to create a new signal from existing one with twice its channels
% so we should get raw original signals,duplicate them and take their second moments 
bestTau = optFullyExplore(@(tau) objectiveTauFunc(tau, crossValidationRatio,C,T,
    nclasses, sizeOfValidation,shuffler,orgTrialGenerator,sizeOfTrianing, model, 
    numSelected, CCModel, options),
    options.firstTau, min(options.lastTau, fix(T/2)), options.tauStep, 
    options.verbose);

    %acc = objectiveTauFunc(bestTau, crossValidationRatio,C,T,
%    nclasses, sizeOfValidation,shuffler,orgTrialGenerator,sizeOfTrianing, model, 
%    numSelected, CCModel, verbose);
if options.verbose
  disp(sprintf('bestTau=%d',bestTau));
  fflush(stdout);
end


% Train a CSP on the whole training data and return it
[s2,allClassLabels] = getCSSPSecondMoments(bestTau, C, T, 1:nTrials, orgTrialGenerator);
csp = trainCSP(2*C, T, nclasses,
	@(index) simpleTrialGenerator(index, 1:nTrials, s2, allClassLabels),
	nTrials, model, numSelected, options.verbose);
csp.tau = bestTau;	
end

%--------------------------

function thiskappa = objectiveTauFunc(tau,crossValidationRatio,C,T,nclasses,
  sizeOfValidation,shuffler,orgTrialGenerator,sizeOfTrianing,CSPModel,numSelected,
  CCModel, options)
  
	% Calculate mean accuracy of all iterations and put it in allacc(tau)
  if options.verbose
    tic;
  end
  
	sumkappa=0;
  time_trainCSP=0;
  time_generatePatternsFromCSP=0;
  time_train_sc=0;
  time_testCSP=0;
  moment=cputime;
  [s2,allClassLabels] = getCSSPSecondMoments(tau, C, T, shuffler, orgTrialGenerator, options.checkNaNs);
  time_generate2ndMoments = cputime-moment;
	for iter=1:crossValidationRatio
		% Train a CSP with partial training data
    % TODO trace from here. test size of below arrays for different iterations
		if iter==1
      rangeIdx = (sizeOfValidation+1:sizeOfValidation+sizeOfTrianing)';
		elseif iter<crossValidationRatio
      rangeIdx = [1:(iter-1)*sizeOfValidation, iter*sizeOfValidation+1:numel(shuffler)]';
		else
      rangeIdx = (1:sizeOfTrianing)';
		end
    moment=cputime;
    cspinst=trainCSP(2*C,T,nclasses,
			@(index) simpleTrialGenerator(index, rangeIdx, s2, allClassLabels),
			sizeOfTrianing,	CSPModel, numSelected);
    time_trainCSP = time_trainCSP+cputime-moment;
    % Trains the classifier(s)
    moment=cputime;
    D = generatePatternsFromCSP(cspinst, 
      @(index) simpleTrialGenerator(index, rangeIdx, s2, []),
      sizeOfTrianing, 'training');
    time_generatePatternsFromCSP = time_generatePatternsFromCSP+cputime-moment;
    moment=cputime;
    CSPModelExtension = CSPModel(6:end);
    if nclasses==2 || strncmpi(CSPModelExtension,'CNV',3)
      CC = train_sc(D, allClassLabels(rangeIdx), CCModel);
      extra=[];
    elseif strncmpi(CSPModelExtension,'OVR',3)
      [CC,extra] = trainOVRClassifiers(D, allClassLabels(rangeIdx), CCModel);
    else
      error 'Other extension models are not implemented'
    end
    time_train_sc = time_train_sc+cputime-moment;
    moment=cputime;
		% Test it on the remaining part of the training data and add the
		% accuracy to sumacc
    rangeIdx = 1+(iter-1)*sizeOfValidation:iter*sizeOfValidation;
		R = testCSP(cspinst, @(index) simpleTrialGenerator(index, rangeIdx, s2, []), 
      sizeOfValidation, CC, CCModel, extra, allClassLabels(rangeIdx));
    time_testCSP = time_testCSP+cputime-moment;
    sumkappa = sumkappa + R.kappa;  % Size of the validation set is always the same
	end
  
  thiskappa = sumkappa/crossValidationRatio;
  
  if options.verbose
    toc
    disp(sprintf('objectiveTauFunc at tau=%d is %f',tau,thiskappa));
    %disp(sprintf('Time for generating second moments=%f',time_generate2ndMoments));
    %disp(sprintf('Time of training CSP=%f',time_trainCSP));
    %disp(sprintf('Time of generating patterns=%f',time_generatePatternsFromCSP));
    %disp(sprintf('Time of training classifiers=%f',time_train_sc));
    %disp(sprintf('Time of testing CSP=%f',time_testCSP));
	  fflush(stdout);
  end
end
%-------------------------

% Generates a random permutation of 1,2,3...n
function s=permutation(n)
s0=fix(rand(1,n)*n)+1;
s=zeros(1,n);
flags=false(1,n);
for k=1:n
	if ~flags(s0(k))
		s(k)=s0(k);
	else
		index=find(~flags);
		s(k)=index(1);
	end
	flags(s(k))=true;
end
end
%-------------------------



