function q = diagonalQuality(A,method)
% q = diagonalQuality(A,method='a')
% A : A two or three dimensional array
% method : Type of averaging; 'a' for ordinary, 'g' for geometric or 'h' for
% harmonic

if nargin<2
  method = 'a';
end

if numel(size(A))==2
  q = diagonalQuality2(A,method);
else
  s=zeros(1,size(A,3));
  for k=1:size(A,3)
    s(k) = diagonalQuality2(A(:,:,k),method);
  end
  q = mean(s,method);
end

end

function q = diagonalQuality2(A,method)
  % Quality of a square matrix
  n = size(A,1);
  A = abs(A);
  r1 = A(1,1)/max(max(A(1,2:n)), max(A(2:n,1)));
  rn = A(n,n)/max(max(A(n,1:n-1)) , max(A(1:n-1,n)));
  s = zeros(1,n);
  s(1)=r1;
  s(n)=rn;
  for k=2:n-1
    s(k) = A(k,k)/max(max(A(k,[1:k-1,k+1:n])), max(A([1:k-1,k+1:n],k)));
  end
  q = mean(s,method);
end