function V = simdiag_lsm(COV, epsillon, theta, verbose)
%  V = simdiag(COV, epsillon, theta=0.95, verbose=false)
% Simultaneous diagonalization of a set of matriices
% Retuens matrix V  so that V*COV(:,:,k)*V' will be almost diagonal for each k
  if nargin<3
    verbose=false;
  end
  
  if nargin<3
    theta = 0.95;
  end
    
  N = size(COV,1);
  V = eye(N);
  W = zeros(N,N);
  n=0;
  COVn = COV;
  maxIterations=1000;
  measuretrace=nan(1,maxIterations);
  % TODO debug from here
  do
    
    zii=zeros(N,1);
    for k=1:size(COV,3)
      zii += diag(COVn(:,:,k)).^2;
    end
    
    
    for i=1:N-1
      for j=i+1:N
        zij = sum(COVn(i,i,:).*COVn(j,j,:));
        yij = sum(COVn(j,j,:).*(COVn(i,j,:)+COVn(j,i,:)))/2;
        yji = sum(COVn(i,i,:).*(COVn(i,j,:)+COVn(j,i,:)))/2;
        dz=zii(i)*zii(j)-zij^2;
        if abs(dz)<1e-30
          error(sprintf('Determinent of z is zero. n=%d, i=%d, j=%d, zii=%g, zjj=%g, zij=%g, dz=%g', 
          n,i,j,zii,zjj,zij,dz));
        end
        W(i,j) = (zij*yji-zii(i)*yij)/dz;
        W(j,i) = (zij*yij-zii(j)*yji)/dz;
      end
    end
    
    %for i=1:N
    %  for j=[1:i-1,i+1:N]
    %    Wnext(i,j) = sum(COVn(j,i,:).*(COVn(i,i,:)-COVn(j,j,:))) / sum((COVn(i,i,:)-COVn(j,j,:)).^2);
    %  end
    %end
    
      normW = sqrt(sum(W(:).^2));
     
      if normW>theta
        W = theta*W/normW;
      end
      
      if verbose
        disp(sprintf('||W(n)||=%f', normW));
        fflush(stdout);
      end
    
    % Update rule
    IW = eye(N)+W;
    V = IW*V;
    for k=1:size(COV,3)
      COVn(:,:,k) = IW*COVn(:,:,k)*IW';
    end
    %if verbose
    %  disp 'C(n+1),k=1:'
    %  disp(COVn(:,:,1))
    %  fflush(stdout);
    %end
    n=n+1;
    
    % We want to diagonalize planes of COV, so a convergence measure is sum of 
    % squared non-diagonal elements of COV
    sumdiags = 0;
    for k=1:size(COVn,3)
      sumdiags = sumdiags + sum(diag(COVn(:,:,k)).^2);
    end
    measure = sum(COVn(:).^2) - sumdiags;
    measuretrace(n)=measure;
    if verbose
      disp(sprintf('Simultaneous diagonalization iteration %d : %f',n,measure));
      fflush(stdout);
    end
    % May need to add this clause : || n>1 && measuretrace(n)>1.1*measuretrace(n-1)
  until(measure<epsillon || n>maxIterations); %*sum(COVn(:).^2)
  
  if verbose
    semilogy(measuretrace(1:n))
  end
end