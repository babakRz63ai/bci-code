function Q = simdiag_jac(A, epsillon, verbose)
% Q = simdiag_jac(A, epsillon=1e-6, verbose=false)
% Simultaneously diagonalize a set of pair wise commuting matrices using Jacobi angles
%
% A : An N by N by K matrix whose plans are pair wise commuting 
% epsillon : sets accuracy of answers
% verbose : boolean, sets verbosity
% 
% Returns: An N by N matrix Q s.t. Q*A(:,:,k)*Q' is mostly diagonal for each k.

if nargin<2
  epsillon=1e-6;
end

if nargin<3
  verbose=false;
end

N = size(A,1);
K = size(A,3);

% Tests whether given matrices are commuting
if verbose
  disp 'simdiag_jac : Checking conditions of the given matrices...'
  fflush(stdout);
end

for k=1:K
  M1 = A(:,:,k)*A(:,:,k)';
  D = M1-M1';
  if sum(abs(D(:)))>epsillon*N*N
    error(sprintf('Matrix A(%d) is not normal', k));
  end
end

for k1=1:K-1
  for k2=k1+1:K
    D = A(:,:,k1)*A(:,:,k2) - A(:,:,k2)*A(:,:,k1);
    if sum(abs(D(:)))>epsillon*N*N
      warning(sprintf('Matrices A(%d) and A(%d) are not commuting (mean difference=%f)', 
        k1,k2,sum(abs(D(:)))/(N*N)));
    end
  end
end

Q = eye(N);
offd = offdiagonal(A)
% This won't change because of unitary transformations
fnorm = FrobeniusNorm(A); 
errorRate = offd/fnorm;
if verbose
    disp(sprintf('simdiag_jac : off(A)=%f, Fnorm(A)=%f, Error rate=%g',offd, fnorm, errorRate));
    fflush(stdout);
end

offdweights = ones(1,K);

while errorRate>epsillon
  temp_length_log = 0;
  anyapplied = false;
  prevErrorRate = errorRate;
  for i=1:N-1
    for j=i+1:N
      [c,s] = minimizers_app1(A,i,j,offdweights,epsillon,verbose);
      R = createR(i,j,c,s,N);
      % Checks if this transform really reduces the error rate
      B = A;
      for k=1:K
        B(:,:,k) = R'*B(:,:,k)*R;
      end
      offd = offdiagonal(B);
      %fnorm = FrobeniusNorm(B);
      if offd/fnorm<=errorRate
        %if verbose
        %  disp(sprintf('Applies R(i=%d, j=%d, c=%f, s=%f)',i,j,c,s));
        %  fflush(stdout);
        %end
        anyapplied = true;
        errorRate = offd/fnorm;
        Q = Q*R;
        A=B;
      end
    end % for j
  end % for i
  offdweights = ones(1,K);
  temp_half=false;
  if ~anyapplied || (prevErrorRate-errorRate)/prevErrorRate<0.01
    index = fix(rand*K)+1;
    offdweights(1:index-1, index+1:end) = 2;
    temp_half = true;
  end
  if ~anyapplied && verbose
    disp('No transformation applied in this iteration');
    fflush(stdout);
  end
  
  offd = offdiagonal(A);
  %fnorm = FrobeniusNorm(A);
  errorRate = offd/fnorm;
  if verbose
    disp(sprintf('simdiag_jac : off(A)=%f, Error rate=%f',offd, errorRate));
    if temp_half
      disp(sprintf('Temporarily uses half of A(%d)',index));
    end
    fflush(stdout);
  end
end % while
end
%-------------------------------
function f = FrobeniusNorm(A)
% Sum of Frobenius norms of all planes of A
  f = 0;
  N2 = size(A,1)^2;
  for k=1:size(A,3)
    f = f + sqrt(sum(A((k-1)*N2+[1:N2]).^2));
  end
end
%-------------------------------
function off = offdiagonal(A,w)
  % Weighted sum of squared off-diagonal elements of all planes of A
  off=0;
  if nargin<2
    w = ones(1,size(A,3));
  end
  
  N2 = size(A,1)^2;
  for k=1:size(A,3)
    off = off + w(k)*(sum(A((k-1)*N2+[1:N2]).^2) - sum(diag(A(:,:,k)).^2));
  end
  
end

%-------------------------------
function [c,s] = minimizers_app1(A, i, j, offdweights, epsillon, verbose)
% Uses generic optimization methods to find a value for c, then sets s=sqrt(1-c^2)
  K = size(A,3);
  ck = nan(1,K);
  sk = nan(1,K);
  %options = optimset('GradObj','on');
  for k=1:K
    [ck(k),s(k)] = myfminfinder(A(i,i,k), A(j,j,k), A(i,j,k), A(j,i,k), epsillon, verbose);
  end % for k
  % Now see which pair minimizes a total measure
  radical2 = sqrt(2);
  aux=zeros(2*K, 3);
  m=1:K;
  aux(2*(m-1)+1,:) = reshape([A(i,j,:), (A(i,i,:)-A(j,j,:))/radical2, -A(j,i,:)], [3,K])';
  aux(2*m,:) = reshape([A(j,i,:), (A(i,i,:)-A(j,j,:))/radical2, -A(i,j,:)], [3,K])';
  
  f_ij_cs = sum((aux*[ck.^2; radical2*ck.*sk; sk.^2]).^2);
  [mf,k] = min(f_ij_cs.*offdweights);
  c=ck(k);
  s=sk(k);
  if numel(c)>1 || numel(s)>1
    disp(f_ij_cs.*offdweights)
    error 'More than one scalar'
  end 
end
%---------------------
function [c,s] = myfminfinder(aii, ajj, aij, aji, epsillon, verbose)
  [c1,s1] = myfminfinder_worker(1, aii, ajj, aij, aji, epsillon, verbose);
  [c2,s2] = myfminfinder_worker(-1, aii, ajj, aij, aji, epsillon, verbose);
  f1 = funcToMinimize(c1, s1, aii, ajj, aij, aji);
  f2 = funcToMinimize(c2, s2, aii, ajj, aij, aji);
  if f1<f2
    c=c1;
    s=s1;
  else
    c=c2;
    s=s2;
  end
end

%---------------------
function [c,s] = myfminfinder_worker(signflag, aii, ajj, aij, aji, epsillon, verbose)
  % divides [0,1] to some intervals so that the first derivative have a zero in
  % some of them
  points=linspace(0,1,5);
  spoints = signflag*sqrt(1-points.^2);
  
  
   
   % Now decides which of the roots or interval ends yield an smaller value
   if counter>0
    cvals = [roots, -1, 1];
    fvals = funcToMinimize(cvals, signflag*sqrt(1-cvals.^2), aii, ajj, aij, aji);
    [f,ix] = min(fvals);
    c = cvals(ix);
    s = signflag*sqrt(1-c*c);
  % Reaching here means it couldn't find a proper interval
   elseif verbose
    points2=linspace(0,1,101);
    fps = zeros(1,101);
    for t=1:101
      fps(t) = funcToMinimize(points2(t),spoints(t), aii, ajj, aij, aji);
    end
    figure
    plot(points2, fps)
    xlabel('c')
    ylabel("f'(c)")
    hold on
    for t=1:numel(points)
      line([points(t),points(t)],[0,funcToMinimize(points(t),spoints(t), aii, ajj, aij, aji)],'color',[1,0,0]);
    end 
    
    title('Could not find a root for this function')
    error 'Could not find a root for this function'
  end
  
end
%---------------------
function roots = findAllRoots(a, b, f, epsillon)
  % May return an empty vector
  points=linspace(a,b,5);
  roots = zeros(1,4);
  fpa = f(points(1));
  counter=0;
  while abs(fpa)<epsillon
    counter=counter+1;
    roots(counter) = points(counter);
    fpa = f(points(counter+1));
  end   % while
  %TODO
  for t=2:numel(points)
     fpb = funcToMinimize(points(t), spoints(t), aii, ajj, aij, aji);
     if abs(fpb)<epsillon
       c = points(t);
       s = spoints(t);
       return;
     end
     
     if fpa*fpb<0
       counter=counter+1;
       roots(counter) = myfzerofinder(points(t-1),points(t), 
        @(c) funcToMinimize(c, signflag*sqrt(1-c*c), aii, ajj, aij, aji), 
        epsillon);
     end
     fpa = fpb;
   end  % for t
end
%---------------------
function c = falsePosition(a, b, f, epsillon)
  fa = f(a);
  fb = f(b);
  do
    c = (a*fb-b*fa)/(fb-fa);
    fc = f(c);
    if abs(fc)<=epsillon
      return
    elseif fa*fc<0
      b=c;
      fb=fc;
    elseif fb*fc<0
     a=c;
     fa=fc;
    else
     error(sprintf('myfzerofinder can not find a root in interval [%f,%f], f(a)=%f, f(b)=%f, f(c=%f)=%f',
      a,b,fa,fb,c,fc));
    end
    until (abs(fc)<=epsillon)
end

%---------------------
function f = funcToMinimize(c, s, aii, ajj, aij, aji)
  % c and c can be scalars or vecotrs
  term1 = c.*s*(aii-ajj);
  term2 = c.*c*(aij+aji);
  f = (term1+term2-aji).^2 + (term1+term2-aij).^2;
end

% These two functions are used for root finding
function fp = fprime1(c, s, aii, ajj, aij, aji)
  if abs(c)==1
    fp = 1;
  else
    fp = (aii-ajj)*(s-c^2/s)+2*c*(aij+aji);
  end
end
%---------------------
function fp = fprime2(c, s, aii, ajj, aij, aji)
  if abs(c)==1
    fp = 1;
  else
    fp = 2*c*s*(aii-ajj)+(2*c^2-1)*(aij+aji);
  end
end
%---------------------
function R = createR(i,j,c,s,N)
  R = eye(N);
  R([i,j],[i,j]) = [c, s; -s, c];
end
