function [V,measuretrace] = simdiag_lsmopt(COV, epsillon, theta, verbose)
%  V = simdiag(COV, epsillon, theta=0.95, verbose=false)
% Simultaneous diagonalization of a set of matriices
% Retuens matrix V  so that V*COV(:,:,k)*V' will be almost diagonal for each k
  if nargin<3
    verbose=false;
  end
  
  if nargin<3
    theta = 0.95;
  end
    
  N = size(COV,1);
  K = size(COV,3);
  V = eye(N);
  W = zeros(N,N);
  n=0;
  COVn = COV;
  maxIterations=1000;
  measuretrace=nan(1,maxIterations);
  alldiags = zeros(N,size(COV,3));
  diagonalIndex = 1:N+1:N*N;
  do
    
    for k=1:K
      alldiags(:,k) = diag(COVn(:,:,k));
    end
    zii=sum(alldiags.^2,2);   % Column
    zij = alldiags*alldiags'; % square
    dz=zii*zii'-zij.^2;       % square
    %idxzero = find(abs(dz)<1e-30);
    %  if numel(idxzero)>N
    %    jix = find(idxzero~=i);
    %    j = idxzero(jix(1));
    %    error(sprintf('Determinent of z is zero. n=%d, i=%d, j=%d, zii=%g, zjj=%g, zij=%g, dz=%g', 
    %      n,i,j,zii(i),zii(j),zij(i,j),dz(j)));
    %  end
    for i=1:N
      temp = (squeeze(COVn(i,:,:))+squeeze(COVn(:,i,:)))/2; % N by K
      yij = sum(alldiags.*temp, 2);   % column
      yji = sum(repmat(alldiags(i,:),[N,1]).*temp, 2);  %column
      W(i,:) = (zij(i,:).*yji'-zii(i)*yij')./dz(i,:);
    end
    W(diagonalIndex)=0;
    
    %for i=1:N
    %  for j=[1:i-1,i+1:N]
    %    Wnext(i,j) = sum(COVn(j,i,:).*(COVn(i,i,:)-COVn(j,j,:))) / sum((COVn(i,i,:)-COVn(j,j,:)).^2);
    %  end
    %end
    
      normW = sqrt(sum(W(:).^2));
     
      if normW>theta
        W = theta*W/normW;
      end
      
      if verbose
        disp(sprintf('||W(n)||=%f', normW));
        fflush(stdout);
      end
    
    % Update rule
    IW = eye(N)+W;
    V = IW*V;
    for k=1:size(COV,3)
      COVn(:,:,k) = IW*COVn(:,:,k)*IW';
    end
    %if verbose
    %  disp 'C(n+1),k=1:'
    %  disp(COVn(:,:,1))
    %  fflush(stdout);
    %end
    n=n+1;
    
    % We want to diagonalize planes of COV, so a convergence measure is sum of 
    % squared non-diagonal elements of COV
    sumdiags = 0;
    for k=1:size(COVn,3)
      sumdiags = sumdiags + sum(diag(COVn(:,:,k)).^2);
    end
    measure = sum(COVn(:).^2) - sumdiags;
    measuretrace(n)=measure;
    if verbose
      disp(sprintf('Simultaneous diagonalization iteration %d : %f',n,measure));
      fflush(stdout);
    end
    % may need to add this clause : || n>1 && measuretrace(n)>1.5*measuretrace(n-1)
  until(measure<epsillon || n>maxIterations); %*sum(COVn(:).^2)
end