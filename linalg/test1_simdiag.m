function test1_simdiag
%rand("seed",2.4392e-310)
% K = 15 diagonal matrices D k of size 5 × 5, where the elements on the diagonal 
% have been drawn from a uniform distribution in the range [−1 , 1]

K=15;
N=50;
scale=20;
D=zeros(N,N,K);
for k=1:K
  D(:,:,k) = diag(1-2*rand(1,N));
end

% A random orthogonal matrix
theta = rand;
A=[cos(theta), -sin(theta); sin(theta), cos(theta)];
for n=2:N-1
  v=rand(n+1,1);
  P=eye(n+1)-2*v*v'/(v'*v);
  A = P*[A, zeros(n,1); zeros(1,n), 1];
end

% A must be orthogonal
sdiff=eye(N)-A*A';
assert(sum(abs(sdiff(:)))<=N*N*eps);

% Mixing
for k=1:K
  D(:,:,k) = A*D(:,:,k)*A'*rand*scale;
end

% Normalizing
for k=1:K
  C=D(:,:,k);
  D(:,:,k) = C/sqrt(sum(C(:).^2));
end
firstMoment = cputime;
% Actual test
V = simdiag_hyb(D, 1e-6, 0.95, true);
secondMoment = cputime;
disp 'These matrices Should be diagonal:'
for k=1:4:15
disp(V*D(:,:,k)*V');
disp '-------------------------------------------------------------------------'
end
disp(sprintf('Passed CPU time: %f',secondMoment-firstMoment));
fflush(stdout);
end

