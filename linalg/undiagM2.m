function measure = undiagM2(C)
  % We want to diagonalize planes of COV, so a convergence measure is sum of 
  % squared non-diagonal elements of COV
  sumdiags = 0;
  for k=1:size(C,3)
      sumdiags = sumdiags + sum(diag(C(:,:,k)).^2);
  end
  measure = sum(C(:).^2) - sumdiags;
end