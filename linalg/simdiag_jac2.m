function Q = simdiag_jac2(A, epsillon, verbose)
% Q = simdiag_jac(A, mode='comulative', epsillon=1e-6, verbose=false)
% Simultaneously block diagonalize a set of pair wise commuting matrices using 
% Jacobi angles
%
% A : An N by N by K matrix whose plans are pair wise commuting 
% mode : A string, one of 'comulative' or 'oneshot'
% epsillon : sets accuracy of answers
% verbose : boolean, sets verbosity
% 
% Returns: An N by N matrix Q s.t. Q*A(:,:,k)*Q' is mostly diagonal for each k.

if nargin<2
  epsillon=1e-6;
end

if nargin<3
  verbose=false;
end

N = size(A,1);
K = size(A,3);

% checkNormality(A)

if verbose
  disp 'simdiag_jac : Checking conditions of the given matrices...'
  fflush(stdout);
end


% TODO if N is odd, add zero pads to the end of planes of A
if mod(N,2)==1
  A2 = zeros(N+1,N+1,K);
  A2(1:N,1:N,:) = A;
else
  A2 = A;
end

Q = comulative(A2,epsillon,verbose)';

if mod(N,2)==1
  Q = Q(1:N, 1:N);
end

end
%-------------------------------
function checkNormality(A)
  % Tests whether given matrices are commuting
  K = size(A,3);
  for k=1:K
  M1 = A(:,:,k)*A(:,:,k)';
  D = M1-M1';
  if sum(abs(D(:)))>epsillon*N*N
    error(sprintf('Matrix A(%d) is not normal', k));
  end
end

for k1=1:K-1
  for k2=k1+1:K
    D = A(:,:,k1)*A(:,:,k2) - A(:,:,k2)*A(:,:,k1);
    if sum(abs(D(:)))>epsillon*N*N
      warning(sprintf('Matrices A(%d) and A(%d) are not commuting (mean difference=%f)', 
        k1,k2,sum(abs(D(:)))/(N*N)));
    end
  end
end

end

%-------------------------------
function Q = comulative(A,epsillon,verbose)
N = size(A,1);
K = size(A,3);
Q = eye(N);
offd = offdiagonal(A)
% This won't change because of unitary transformations
fnorm = FrobeniusNorm(A); 
errorRate = offd/fnorm;
if verbose
    disp(sprintf('simdiag_jac : off(A)=%f, Fnorm(A)=%f, Error rate=%g',offd, fnorm, errorRate));
    fflush(stdout);
end

offdweights = ones(1,K);

while errorRate>epsillon
  temp_length_log = 0;
  anyapplied = false;
  prevErrorRate = errorRate;
  for i=permutation(N/2)
    if verbose
      disp(sprintf('i=%d',i));
      fflush(stdout);
    end
    
    for j=i+1:N/2
      Z = blockrotation(i,j,A);
      % Checks if this transform really reduces the error rate
      B = A;
      for k=1:K
        B(:,:,k) = Z'*B(:,:,k)*Z;
      end
      offd = offdiagonal(B);
      %fnorm = FrobeniusNorm(B);
      if offd/fnorm<=errorRate
        %if verbose
        %  disp(sprintf('Applies R(i=%d, j=%d)',i,j));
        %  fflush(stdout);
        %end
        anyapplied = true;
        errorRate = offd/fnorm;
        Q = Q*Z;
        A=B;
      end
    end % for j
  end % for i
  %offdweights = ones(1,K);
  %temp_half=false;
  %if ~anyapplied || (prevErrorRate-errorRate)/prevErrorRate<0.01
  %  index = fix(rand*K)+1;
  %  offdweights(1:index-1, index+1:end) = 2;
  %  temp_half = true;
  %end
  if ~anyapplied && verbose
    disp('No transformation applied in this iteration');
    fflush(stdout);
  end
  
  offd = offdiagonal(A);
  %fnorm = FrobeniusNorm(A);
  errorRate = offd/fnorm;
  if verbose
    disp(sprintf('simdiag_jac : off(A)=%f, Error rate=%f',offd, errorRate));
    %if temp_half
    %  disp(sprintf('Temporarily uses half of A(%d)',index));
    %end
    fflush(stdout);
  end
end % while
end
%-------------------------------
function p = permutation(n)
% generates a permutation of 1,2,...,n
p=zeros(1,n);
x=fix(n*rand(1,2*n))+1;
taken = false(1,n);
count=0;
for t=1:n
  if ~taken(x(t))
    count = count+1;
    p(count)=x(t);
    taken(x(t))=true;
  end
end
% reminders
for t=1:n
  if ~taken(t)
    count=count+1;
    p(count)=t;
  end
end

end
%-------------------------------
function Z = blockrotation(i,j,A)
  % Optimizes a function with four parameters t1,t2,t3 and t4
  T = fminunc(@(T) objective(T,i,j,A), [0;0;0;0], optimset('TolFun',1e-3));
  c = cos(T);
  s = sin(T);
  U = R(2,4,c(4),s(4)) * R(1,4,c(3),s(3)) * R(2,3,c(2),s(2)) * R(1,3,c(1),s(1));
  N = size(A,1);
  Z = eye(N,N);
  iidx = [2*i-1,2*i];
  jidx = [2*j-1,2*j];
  Z(iidx,iidx) = U([1,2],[1,2]);
  Z(iidx,jidx) = U([1,2],[3,4]);
  Z(jidx,iidx) = U([3,4],[1,2]);
  Z(jidx,jidx) = U([3,4],[3,4]);
end
%-------------------------------
function y = objective(T,i,j,A)
  c = cos(T);
  s = sin(T);
  U = R(2,4,c(4),s(4)) * R(1,4,c(3),s(3)) * R(2,3,c(2),s(2)) * R(1,3,c(1),s(1));
  U11 = U([1,2],[1,2]);
  U12 = U([1,2],[3,4]);
  U21 = U([3,4],[1,2]);
  U22 = U([3,4],[3,4]);
  % Avoid outer rotations
  if norm(U11,'fro')<norm(U12,'fro')
    y = Inf;
  else
    y=0;
    %N=size(A,1);
    %half = N/2;
    iidx = [2*i-1,2*i];
    jidx = [2*j-1,2*j];
    for k=1:size(A,3)
      Aii = A(iidx,iidx,k);
      Aij = A(iidx,jidx,k);
      Aji = A(jidx,iidx,k);
      Ajj = A(jidx,jidx,k);
      y = y + f2norm((U12'*Aii + U22'*Aji)*U11 + (U12'*Aij + U22'*Ajj)*U21) + f2norm((U11'*Aii + U21'*Aji)*U12 + (U11'*Aij + U21'*Ajj)*U22);
    end
  end
end

%-------------------------------
function f = FrobeniusNorm(A)
% Sum of Frobenius norms of all planes of A
  f = 0;
  for k=1:size(A,3)
    f = f + norm(A(:,:,k),'fro');
  end
end
%-------------------------------
function f2 = f2norm(X)
  f2 = sum(X(:).^2);
end
%-------------------------------
function off = offdiagonal(A,w)
  % Weighted sum of squared block off-diagonal elements of all planes of A
  off=0;
  if nargin<2
    w = ones(1,size(A,3));
  end
  
  A2=A.^2;
  N=size(A,1);
  for k=1:size(A,3)
    for d=[-N+1:-2,2:N-1]
      off=off+sum(diag(A2(:,:,k),d));
    end
    planeHead = (k-1)*N*N;
    % interleaved reminders
    off = off + sum(A2(planeHead+2*N+2:2*N+2:planeHead+(N+1)*(N-2))) + sum(A2(planeHead+N+3:2*N+2:planeHead+N*(N-3)+N-1));
  end
end
%-------------------------------
function r = R(i,j,c,s)
  r = eye(4,4);
  r([i,j],[i,j]) = [c, -s; s, c];
  %r(i,i)=c;
  %r(i,j)=-s;
  %r(j,i)=s;
  %r(j,j)=c;
end
