function V = simdiag_hyb(COV, epsillon, theta, verbose)
%  V = simdiag(COV, epsillon, theta=0.95, TolErrRateJac=1, verbose=false)
% Simultaneous diagonalization of a set of matriices
% Retuens matrix V  so that V*COV(:,:,k)*V' will be almost diagonal for each k
  if nargin<4
    verbose=false;
  end
  
  if nargin<3
    theta = 0.95;
  end
    
  N = size(COV,1);
  K = size(COV,3);
  V = eye(N);
  W = zeros(N,N);
  n=0;
  COVn = COV;
  
  norms=zeros(1,K);
  for k=1:K
    norms(k) = norm(COV(:,:,k));
  end
  COVn = COV/max(norms);
  V = V/sqrt(max(norms));
  
  maxIterations=50;
  measuretrace=nan(1,maxIterations);
  increasingCounter=0; 
  do
    % Get the first possible update
    U = lsmw(COVn,theta);
    COVnext = updateRule(COVn,U);
    % Does it reduce the undiagonality measure?
    measure = undiagM2(COVnext);
    %if n==0 || measure<measuretrace(n)
      COVn = COVnext;
    %  increasingCounter=0;
    %else
    %  if verbose
    %    disp 'Random step...'
    %    fflush(stdout);
    %  end
      
    %  while measure>=measuretrace(n)
    %    U = randomTrans(N);
    %    COVnext = updateRule(COVn,U);
    %    measure = undiagM2(COVnext);
    %  end
      
        %increasingCounter=0;
      %else  % It is acceptable
      %end
    % Update rule
    V = U*V;
    n=n+1;
    
    measuretrace(n)=measure;
    if verbose
      disp(sprintf('Simultaneous diagonalization iteration %d : %f',n,measure));
      fflush(stdout);
    end
  until(measure<epsillon || n>maxIterations ); %*sum(COVn(:).^2)
  % || n>1 && measuretrace(n)>1.1*measuretrace(n-1)
  
  if verbose
    figure
    semilogy(measuretrace(1:n),'b')
    title('Simultaneous diagonalization')
    xlabel('Iterations')
    ylabel('Diagonalization error')
  end
end

function U = lsmw(COV, theta)
  % Uses method by Andreas Ziehe and Pavel Laskov
  N = size(COV,1);
  K = size(COV,3);
  alldiags = zeros(N,1,K);
  diagonalIndex = 1:N+1:N*N;
  temp = zeros(N, N, K);
  for k=1:K
      alldiags(:,1,k) = diag(COV(:,:,k));
      temp(:,:,k) = (COV(:,:,k)+COV(:,:,k)')/2;
    end
    
    zii=sum(alldiags.^2,3);   % Column
    zij = squeeze(alldiags)*squeeze(alldiags)'; % square
    dz=zii*zii'-zij.^2;       % square
    %if any(abs(dz)<1e-15)
    
    yij = sum(repmat(alldiags,[1,N]).*temp, 3); % square
    W = (zij.*yij - repmat(zii,[1,N]).*yij')./dz;
    W(diagonalIndex)=0;
    if any(isnan(W(:)))
      error 'NaN values in primary W'
    end
    
    normW = norm(W,'fro');
    if normW>theta
        W = theta*W/normW;
    end
    U = eye(N)+W;
end
%-------------------------
function U = randomPermutation(N)
  U=eye(N,N);
  i=fix(rand*(N-1))+1;
  j=i+1+fix(rand*(N-i));
  U([i,j],[i,j]) = [0 1; 1 0];
end
%-------------------------
function U = randomScale(n)
  U = eye(n,n);
  i=fix(rand*(n-1))+1;
  U(i,i) = rand;
end
%-------------------------
function U = randomTrans(n)
  N = randn(n,n)/500;
  N(1:n+1:n*n) = 0;
  U = eye(n,n) + N;
end
%-------------------------
function COVout = updateRule(COV,U)
  % Applies U on each plane of COV
  COVout = zeros(size(COV));
  for k=1:size(COV,3)
      COVout(:,:,k) = U*COV(:,:,k)*U';
  end
end
%------------------------ 