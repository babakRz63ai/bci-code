function testCSSP_BBCI_III_3a
  frqs=250;	% Hz
	C=60;		% Channels
	T=1000;		% Time samples after the visual cue
	subject={'k3b','k6b','l1b'};
	winoptions.type=1;
	winoptions.alpha=0.5;
	h = wsibandpass(7*2*pi/frqs, 30*2*pi/frqs, 32, winoptions);
	window = hamming(T);
  headers = cell(1,3);
  signals = cell(1,3);
  trueLabels = cell(1,3);
  for k=1
	% Will load HDR and s for training and testing for each subject
  disp(sprintf('Loading data for subject %d : %s',k,subject{k}));
  fflush(stdout);
	load(sprintf('../datasets/bci/bbci.de/BCI-comp-III/3/a/%s.mat', subject{k}))
  
	
  % True labels
	[fid,message] = fopen(sprintf('../datasets/bci/bbci.de/BCI-comp-III/3/a/true_labels_%s.txt',subject{k}),'r');
	if fid==-1
		error(message)
	end
  testTrials = sum(isnan(HDR.Classlabel));
	[trueLabels{k},c,message]=fscanf(fid,'%d',[testTrials,1]);
	fclose(fid);
	if length(message)>0
		disp (message)
	end
  
  disp 'Preprocessing signal...'
  fflush(stdout);
	signals{k} = preprocess(s, window, h, C, HDR.TRIG);
	clear s
  headers{k}=HDR;
end   % for k
  
  demoRun(signals,headers,trueLabels);
  
end
%------------------------------------
function demoRun(signals,headers,trueLabels)
  C=60;		% Channels
	T=1000;		% Time samples after the visual cue
  CSPModel = 'OVR.CSP3';
  selectedFilters=3;
  CCModel.TYPE = 'FLDA';
  CCModel.hyperparameter.gamma=0.1;
  sumAcc=0;
  sumKappa=0;
  sumTimes=0;
  for k=1:3
    trainingIndex = find(~isnan(headers{k}.Classlabel) & ~headers{k}.ArtifactSelection);
    testingIndex = find(isnan(headers{k}.Classlabel));
	  trainingTrials = numel(trainingIndex);
	  testTrials = numel(testingIndex);
    disp 'Training CSP...'
    fflush(stdout);
    moment=cputime;
    cssp = trainCSSP(C,T,4, @(idx) mytrialGenerator(idx, signals{k}, 
      trainingIndex, headers{k}.TRIG, headers{k}.Classlabel), trainingTrials,
      CSPModel, CCModel, selectedFilters, true);
    sumTimes = sumTimes+cputime-moment;
    disp 'Training classifiers...'
    fflush(stdout);  
    D = generatePatternsFromCSP(cssp,@(index) CSSPTrialGenerator(cssp.tau, 
      [1:trainingTrials], index, @(idx) mytrialGenerator(idx, signals{k}, 
      trainingIndex, headers{k}.TRIG, headers{k}.Classlabel)), trainingTrials, 
      'training');
    [CC,extra] = trainOVRClassifiers(D, headers{k}.Classlabel(trainingIndex),
      CCModel);
    
    disp 'Testing...'
    fflush(stdout);  
    R = testCSSP(cssp, @(idx) mytrialGenerator(idx, signals{k}, 
      testingIndex, headers{k}.TRIG, headers{k}.Classlabel), testTrials, CC, 
      CCModel, extra, trueLabels{k});
    disp('Confusion matrix is');
    disp(R.H);
    disp(sprintf('Accuracy=%5.2f%% kappa=%f',R.ACC*100, R.kappa));
    fflush(stdout);
    sumAcc=sumAcc+R.ACC;
    sumKappa=sumKappa+R.kappa;
  end % for k
  disp(sprintf('Average accuracy=%5.2f%% , kappa=%f time=%f',100*sumAcc/3, 
   R.kappa/3, sumTimes/3));
end
%------------------------------------
function signal = preprocess(signal1, window, h, C, trigger)
	T = numel(window);
	window = repmat(window(:), 1, C);
	signal = zeros(size(signal1));
	for startRow = trigger'
		%disp(sprintf('Updates rows %d to %d\n',startRow,startRow+T-1))
		signal(startRow:startRow+T-1,:) = filter(h, 1, signal1(startRow:startRow+T-1,:).*window);
	end
	if all(signal(:)==0)
		error "Signals after preprocessing are all zero"
	%else
	%	disp "Healthy preprocessed signal:"
	%	disp(signal(1:20,1:8))
	end
end
%------------------------------------
function [trial,label] = mytrialGenerator(idx, s, trialsIndex, triggerStart, classlabels)
	trial = s(triggerStart(trialsIndex(idx)):triggerStart(trialsIndex(idx))+999, :)';
	%if all(abs(trial(:))==0)
	%	disp(sprintf('idx=%d , trialsIndex(idx)=%d , triggerStart(trialsIndex(idx))=%d', 
	%	idx, trialsIndex(idx), triggerStart(trialsIndex(idx))))
	%	error "Extracted trial is all zeror"
	%end
  if nargin>=5
	  label = classlabels(trialsIndex(idx));
  else
    label=NaN;
  end
end
