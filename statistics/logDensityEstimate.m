function logp = logDensityEstimate(x, samples)
  % p = logDensityEstimate(x, samples)
  % Estimates logarithm of density of a given point x using a set of samples using k-nearest 
  % neighbours method
  % x  :  A column vector with d elements
  % samples : A matrix with d rows and n columns
  % Retuens : An estimate of probablity density at point x
  [d,n] = size(samples);
  if numel(x)~=d
    error 'Point x must be in the same space as given samples'
  end
  k = fix(sqrt(n));
  % Distance of x to each sample
  if isOctave
    distance = sqrt(sum(x-samples).^2);
  else
    distamce = sqrt(sum(repmat(x,[1,n])-samples).^2);
  end
  % Where are the k-nearest neighbours?
  S = sort(distance);
  % Distance of the k-th point to x defines radius of a sphere around x. We need
  % to calculate volume of this sphere.
  %v = 2 * S(k)^d * pi^(d/2) * gamma(d/2)/d;
  logv = log(2)+d*log(S(k)) + d/2*log(pi) + gammaln(d/2)-log(d);
  %p = k/(n*v);
  logp = log(k)-log(n)-logv;
end
