function p = probablityEstimate2(x,halfdelta,samples)
  % p = probablityEstimate2(x,halfdelta,samples)
  % Estimates probablity mass of a box around a point by counting samples that
  % fall in the box
  % 
  % x : A point in the d-dimensional space
  % halfdelta : vector of half of sides of a box around x
  % samples : Matrix of data samples
  [d,n] = size(samples);
  
  % k=0;
  %for t=1:n
  %  if all(x-halfdelta<=samples(:,t) && samples(:,t)<=x+halfdelta)
  %    k = k+1;
  %  end
  %end
  
  if isOctave
    k = nnz(all(x-halfdelta<=samples & samples<=x+halfdelta));
  else
    k = numel(find(all(repmat(x-halfdelta,[1,n])<=samples & samples<=repmat(x+halfdelta,[1,n]))));
  end
  
  p = k/n;
end
