function entropy = entropyOfSignal(signal,nBins)
  % entropy = entropyOfSignal(signal, nBins=10)
  % Retuens entropy of signals of some trials
  % signal : A 3D array. Each sample is taken as a column of this array
  % nBins : Number of bins along each dimension
  
  if nargin<2
    nBins = 10;
  end
  [C,T,K] = size(signal);
  if C>6
    warning 'Using many channels will cause this algorithm to take too long'
  end
  
  entropy = 0;
  
  samples = reshape(signal,[C,T*K]);
  % Devides the domain along each dimension
  allMin = min(samples,[],2);
  delta = (max(samples,[],2) - allMin)/nBins;
  halfdelta = delta/2;
  dv = prod(delta);
  x0 = allMin+delta/2;
  % Generates all representative points 
  current = zeros(C,1);
  counter=1;
  total = nBins^C;
  format = sprintf('%%%dd of %d',C,total);
  numchars = fprintf(stdout,format,0);
  backspace = char(8*ones(1,numchars));
  fflush(stdout);

  while ~isempty(current)
    fprintf(stdout,backspace);
    fprintf(stdout,format,counter);
    fflush(stdout);
    x = x0 + current.*delta;
    p = probablityEstimate2(x, halfdelta, samples);
    if p>0
      entropy = entropy - p*log(p);
      % disp([p, log(p), entropy])
      % fflush(stdout);
    end
    current = pointGenerator(current, nBins-1);
    counter = counter+1;
  end %while
  fprintf(stdout,backspace);
end
%--------------
function next = pointGenerator(current, bound)
  % Takes a vector of indices and generates the next vector
  next = current;
  if next(end)<bound
    next(end) = next(end)+1;
    return
  else
    i = numel(next);
    while i>0 && next(i)==bound
      next(i)=0;
      i = i-1;
    end %while
    if i==0
      next = [];
    else
      next(i) = next(i)+1;
    end %if
  end %if
end
