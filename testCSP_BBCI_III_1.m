function testCSP_BBCI_III_1
	% Training : 278 trials, 64 channels and 3000 samples in 3 seconds
	% Testing  : 
	frqs=1000;
	C=64;
	T=3000;
	trainingTrials = 278;
	testTrials = 100;
	
  disp 'Loading training data...'
  fflush(stdout);
  
	% Will load X and Y for training
	load '../datasets/bci/bbci.de/BCI-comp-III/1/competition_train.mat'
	
  disp 'Preprocessing training data...'
  fflush(stdout);
	Xtrain = preprocess(X);
	% Converts -1,1 to 1,2
	Ytrain = (3+Y)/2;
	
	% Will load X for testing
  disp 'Loading testing data...'
  fflush(stdout);
	load '../datasets/bci/bbci.de/BCI-comp-III/1/competition_test.mat'
  disp 'Preprocessing testing data...'
  fflush(stdout);
	X = preprocess(X);
	
	% True labels
	[fid,message] = fopen('../datasets/bci/bbci.de/BCI-comp-III/1/true_labels.txt','r');
	if fid==-1
		error(message)
	end
	[trueLabels,c,message]=fscanf(fid,'%d',[size(Y,1),1]);
	fclose(fid);
	if length(message)>0
		disp (message)
	end
	% Converts -1,1 to 1,2
	trueLabels = (3+trueLabels)/2;
	
  disp 'Training CSP...'
  fflush(stdout);
	moment = cputime;
	csp = trainCSP(C,2,@(idx) mytrialGenerator(idx, Xtrain, Ytrain), trainingTrials, 
    '', 3, true);
  
  disp 'Training classifier...'
  fflush(stdout);
  D = generatePatternsFromCSP(csp, Xtrain, trainingTrials);
  model.TYPE = 'FLDA';
  model.hyperparameter.gamma=0.075;
  CC = train_sc(D, Ytrain, model);
  passedTime = cputime-moment;
	
  clear Xtrain
	clear Ytrain
  clear D
  
  disp 'Testing...'
  fflush(stdout);
	R = testCSP(csp, X, CC, model, trueLabels);
	disp 'Confusion matrix is:'
  disp(R.H);
  disp(sprintf('Accuracy=%f, time=%fs',100*R.ACC, passedTime))
  
end

function xpp = preprocess(X)
	% X : A three dimensional matrix with L rows, C columns and T planes
	% lowPass : A vector of factors of a low pass filter
	% downSamplingRate : scaler
	% window : A vector with T/10 elements
	% bandPass : A vector of factors of a band pass filter
	% down sampling
  frqs=1000;
	C=64;
	T=3000;
  winoptions.type=1;
	winoptions.alpha=0.5;
	filterLength = 112;
  % Down-sampling the signal
  downSamplingRate = 10;
	
	hlow = wsilowpass(pi/downSamplingRate, filterLength, winoptions);

	% Windowing and band-pass filtering will be done on the down sapled signal
	frqs = frqs/downSamplingRate;
	
	bandPass = wsibandpass(6*2*pi/frqs, 34*2*pi/frqs, filterLength, winoptions);
	
	X = filter(hlow, 1, permute(X,[2,3,1]), [], 2);
  [C,T,K] = size(X);
	X = X(:, downSamplingRate/2:downSamplingRate:T, :);
	T = size(X,2);
  % Windowing
  window = hamming(T)';
	X = X.*repmat(window, [C,1,K]);
	% band pass filtering
  % filters along the second dimension
	X = filter(bandPass, 1, X, [], 2);
  % centering and taking the second moment
  xpp = zeros(C, C, K);
  central=eye(T)-ones(T)/T;
  for trial=1:K
    xtrial=X(:,:,trial)*central;
    xpp(:,:,trial) = xtrial*xtrial'/T;
  end
end


function [trial,label] = mytrialGenerator(idx,x,y)
trial = x(:,:,idx);
if ~isnan(y)
	label = y(idx);
else
  label=[];
end
end