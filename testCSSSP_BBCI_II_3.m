function testCSSSP_BBCI_II_3

	% Loading data
  disp 'Loading data...'
  fflush(stdout);
	load '../datasets/bci/bbci.de/BCI-comp-II/set3/dataset.mat'
	% True labels as y_test vector
	load '../datasets/bci/bbci.de/BCI-comp-II/set3/test_labels.mat'
	
  demo(x_train, y_train, x_test, y_test); % simulation1
	
end

function demo(x_train, y_train, x_test, y_test)
  disp 'Preprocessing...'
  fflush(stdout);
  frqs=128;
	winoptions.type=1;
	winoptions.alpha=0.5;
	C=3;
	T=1152;
  x_train = preprocess1WithParams(x_train);
  x_test = preprocess1WithParams(x_test);
  
	% Timing starts from here
	%timedt=cputime;
  tic
	disp 'Training CSP'
  fflush(stdout);
  model.SpectFltrLength = 5;
  model.CCModel='FLDA';
  options.optimizationMethod='fminunc';
  options.verbose = true;
  options.parallel=false;
	csssp = trainCSSSP(x_train, y_train, model, 3, options);
  
  disp 'Training classifier...'
  fflush(stdout);
  D = generatePatternsFromCSSSP(csssp, x_train);
  mode.TYPE = 'FLDA';
  CC = train_sc(D, y_train, mode);
  toc
	% end of timing
  %timedt = cputime-timedt;
  
	% Testing CSP
  disp 'Testing CSP and classifier...'
  fflush(stdout);
	D = generatePatternsFromCSSSP(csssp, x_test);
  R = test_sc(CC, D, mode, y_test);
	disp(sprintf("Accuracy = %5.2f%%, duration=nan",R.ACC*100));
  disp 'Confusion matrix:'
  disp(R.H);
end
%-----------------
function simulation1(x_train, y_train, x_test, y_test)
  frqs=128;
  C=3;
  % Default values
  params.lastMoment = 9;
  params.SpectFltrLength = 10;

  params.firstMoment = findBestParameterValue(x_train, y_train, x_test, y_test,
    params, 'firstMoment', 0:0.1:4.5, true);

  params.lastMoment = findBestParameterValue(x_train, y_train, x_test, y_test,
    params, 'lastMoment', params.firstMoment+0.1:0.1:9, true);
    
  params.SpectFltrLength = findBestParameterValue(x_train, y_train, x_test, y_test,
    params, 'SpectFltrLength', 1:1:40, false);
end
%-----------------
function bestValue = findBestParameterValue(x_train_org, y_train, x_test_org, 
  y_test, params, name, paramValues, shouldPreprocessEachTime)
  
  frqs=128;
  C=3;
  
  if ~shouldPreprocessEachTime
    x_train = preprocess1WithParams(x_train_org, params);
    x_test = preprocess1WithParams(x_test_org, params);
  end

  accracy = zeros(size(paramValues));
  model.CCModel = 'FLDA';
  cssspoptions.optimizationMethod = 'fminunc';
  cssspoptions.parallel = true;
  cssspoptions.verbose = false;
  for k=1:numel(paramValues)
    params = setfield(params, name, paramValues(k));
    if shouldPreprocessEachTime
      x_train = preprocess1WithParams(x_train_org, params);
      x_test = preprocess1WithParams(x_test_org, params);
    end
    % training CSSSP
    model.SpectFltrLength = params.SpectFltrLength;
    csssp = trainCSSSP(x_train, y_train, model, 3, cssspoptions);
    D = generatePatternsFromCSSSP(csssp, x_train);
    CCmode.TYPE = 'FLDA';
    CC = train_sc(D, y_train, CCmode);
	  % Testing CSP
	  D = generatePatternsFromCSSSP(csssp, x_test);
    R = test_sc(CC, D, CCmode, y_test);
    accracy(k) = R.ACC;
    disp(sprintf('%s=%f will result in accuracy=%5.2f%%', name, paramValues(k),
      100*R.ACC));
    fflush(stdout);
  end
  [v,ix] = max(accracy);
  bestValue = paramValues(ix);
end  
%-----------------
function xpp = preprocess1WithParams(x, params)
% params should be a structure with fields: 'firstMoment' and 'lastMoment'
  C=3;
  frqs=128;
  if nargin<2
     params.firstMoment = 0;
     params.lastMoment = 9;
  end
   
  % Window
  offset1 = max(1,fix(frqs*params.firstMoment));
  offset2 = fix(frqs*params.lastMoment);
  T = offset2-offset1+1;
  window = hamming(T)';
  repwin = repmat(window, C, 1);
  %central=eye(T)-ones(T)/T;
  xpp = zeros(C,T,size(x,3));
  for idx = 1:size(x,3)
    xpp(:,:,idx) = repwin.*x(offset1:offset2,:,idx)'; % C by T
  end
end

