function testCSSP_BBCI_II_3

	% Loading data
	load '../datasets/bci/bbci.de/BCI-comp-II/set3/dataset.mat'
	
	C=3;
	T=1152;
	
	% True labels
	load '../datasets/bci/bbci.de/BCI-comp-II/set3/test_labels.mat'
	
	disp 'Preprocessing...'
	fflush(stdout)
	x_train = preprocess(x_train);
	x_test = preprocess(x_test);
  y_train=y_train(:);
  y_test=y_test(:);
	simulation2(x_train,y_train,x_test,y_test);
end

function demoRun(x_train, y_train, x_test, y_test, numSelectedFilters, aGamma)
  sumtimes = 0;
	sumacc=0;
	iterations=1;
  C=3;
	T=1152;
  for iter=1:iterations
	% Timing starts from here
	timedt=cputime;
	CCModel.TYPE = 'FLDA';
  CCModel.hyperparameter.gamma = aGamma;
  options.verbose=true;
  options.tauStep = 3;
	cssp = trainCSSP(C, T, 2,
		@(idx) mytrialGenerator(idx, x_train, y_train), 140,'CSP0.CNV',CCModel,
    numSelectedFilters, options);
	% end of timing
	timedt = cputime-timedt;
	sumtimes = sumtimes+timedt;
  
  s2 = getCSSPSecondMoments(cssp.tau, C, T, [1:140],
    @(idx) mytrialGenerator(idx, x_train, NaN));
  
  D = generatePatternsFromCSP(cssp, @(index) simpleTrialGenerator(index, [1:140], 
    s2, []), 140, 'training');
  
  CC = train_sc(D, y_train, CCModel);  
  
	% Testing CSP
	R = testCSSP(cssp, @(idx) mytrialGenerator(idx, x_test, NaN),
		numel(y_test),CC,CCModel,[], y_test);
	sumacc=sumacc + R.ACC;
  disp 'Confusion matrix is'
  disp(R.H);
  disp(sprintf('Accuracy in this iteration is %f%%',R.ACC*100));
  fflush(stdout);
	
	end		% iterations
	
  horn(5);
  
  disp(sprintf('Accuracy is %f, average time is %fs',100*sumacc/iterations,sumtimes/iterations))
	
end

function simulation1(x_train, y_train, x_test, y_test)
  C=3;
	T=1152;
 	iterations=1;
  CCModel.TYPE = 'FLDA';
  CSPModels={'CSP0.CNV','CSP3.CNV','CSP4.CNV'};
  results=cell(1,numel(CSPModels)*4);
  counter=0;
  options.verbose=false;
  options.tauStep = 3;
  for CSPModelIndex=1:numel(CSPModels)
    for numSelectedFilters=1:4
      disp(sprintf('CSP Model is %s, number of selected filters is %d',
        CSPModels{CSPModelIndex},numSelectedFilters));
      sumacc=0;
      for iter=1:iterations
        disp(sprintf('Iteration:%d',iter));
        fflush(stdout);
        cssp = trainCSSP(C, T, 2,
		      @(idx) mytrialGenerator(idx, x_train, y_train), 140,
          CSPModels{CSPModelIndex}, CCModel, numSelectedFilters,options);
        s2 = getCSSPSecondMoments(cssp.tau, C, T, [1:140],
          @(idx) mytrialGenerator(idx, x_train, NaN));
        D = generatePatternsFromCSP(cssp, @(index) simpleTrialGenerator(index, [1:140], 
          s2, []), 140, 'training');
        CC = train_sc(D, y_train, CCModel);
        R = testCSSP(cssp, @(idx) mytrialGenerator(idx, x_test, NaN),
		      numel(y_test), CC, CCModel, [], y_test);
	      sumacc=sumacc + R.ACC;
      end % Iterations for each configuration
      counter=counter+1;
      r.acc = sumacc/iterations;
      r.cspmodel=CSPModels{CSPModelIndex};
      r.numSelectedFilters = numSelectedFilters;
      results{counter} = r;
    end
  end

  disp('Model     Filters   Accuracy');
  for n=1:numel(results)
    disp(sprintf('%s      %d      %5.2f%%',results{n}.cspmodel, 
      results{n}.numSelectedFilters, results{n}.acc*100));
  end
  
  horn(5);
end

function simulation2(x_train, y_train, x_test, y_test)
  C=3;
	T=1152;
 	iterations=1;
  CCModel.TYPE = 'FLDA';
  CSPModel='CSP3.CNV';
  allGamma=0.05:0.01:0.1;
  accPerGamma=zeros(size(allGamma));
  options.verbose=true;
  options.tauStep = 3;
  numSelectedFilters=2
      disp(sprintf('CSP Model is %s, number of selected filters is %d',
        CSPModel,numSelectedFilters));
      for iter=1:iterations
        disp(sprintf('Iteration:%d',iter));
        fflush(stdout);
        CCModel.hyperparameter.gamma=0.08;
        cssp = trainCSSP(C, T, 2,
		      @(idx) mytrialGenerator(idx, x_train, y_train), 140,
          CSPModel, CCModel, numSelectedFilters, options);
        s2 = getCSSPSecondMoments(cssp.tau, C, T, [1:140],
          @(idx) mytrialGenerator(idx, x_train, NaN));
        D = generatePatternsFromCSP(cssp, @(index) simpleTrialGenerator(index, [1:140], 
          s2, []), 140, 'training');
        tg=1;  
        for gamma=allGamma
          disp(sprintf('gamma=%f',gamma));
          fflush(stdout);
          CCModel.hyperparameter.gamma=gamma;
          CC = train_sc(D, y_train, CCModel);
          R = testCSSP(cssp, @(idx) mytrialGenerator(idx, x_test, NaN),
		        numel(y_test), CC, CCModel, [], y_test);
	        accPerGamma(tg)=accPerGamma(tg) + R.ACC;
          tg=tg+1;
        end
        
      end % Iterations for each configuration

  disp('Gamma     Accuracy');
  for n=1:numel(allGamma)
    disp(sprintf('%f      %5.2f%%',allGamma(n), 100*accPerGamma(n)/iterations));
  end
  
  horn(5);
end
% Returns a matrix with C rows, C columns and N planes
function xpp = preprocess(x)
	frqs=128;
	C=3;
	T=1152;
	winoptions.type=1;
	winoptions.alpha=0.5;
	h = wsibandpass(6*2*pi/frqs, 34*2*pi/frqs, 112, winoptions);
	window = hamming(T);
	windowRep = repmat(window(:), 1, size(x,2));
	xpp = zeros(C,T,size(x,3));
	for z=1:size(x,3)
		xpp(:,:,z) = filtfilt(h, 1, x(:,:,z).*windowRep)';
	end
end

function [trial,label] = mytrialGenerator(idx, x, y)
% idx : index of a trial
% x   : a three dimensional array with T times C times L elements
% y   : a vector with L elements (one for each trial)
trial = x(:,:,idx);
if ~isnan(y)
	label = y(idx);
else
  label=NaN;
end
end
%-------------------------