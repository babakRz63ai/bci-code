function csssp = trainCSSSP(signal, allClassLabels, model, numSelectedFilters, options)
  % csssp = trainCSSSP(signal, allClassLabels, model, numSelectedFilters, options)
  %
  % signal : 3D Array of raw signal with channels in its rows, time samples in its columns and trials in its planes.
  % The signal should have been centralized and be without NaN values
  % allClassLabels : Vector of class labels for training trials
  % model : Struct with fields SpectFltrLength and CCModel (and optionally 
  % CSPModel and problemName)
  % numSelectedFilters : number of selected spectro-spatial filters from each
  % end of spectrum of eigne values
  %
  % options is a struct with following fields:
  % options.verbose : Controls verbosity
  % options.parallel : whether calculations should be performed in parallel
  % options.optimizationMethod : A string, name of the used method. trainCSSSP accepts
  % one of 'fminunc', 'fminsearch', 'nonlin_min', 'mdsmax', 'SimAnnealing' or
  % 'HillClimbing' 
  %
  % Retuens a struct with fields:
  %   cspinst : CSP instance
  %   b       : vector of weights forming the spectral filter
  %   controlFactor : a factor controlling the sparcity of b elements
  
  if nargin<4
    error 'Insufficient arguments'
  end
    
  if nargin<5
    options.verbose=false;
    options.parallel=false;
    options.optimizationMethod = 'fminunc';
  elseif ~isfield(options,'verbose') || ~isfield(options,'parallel') || ~isfield(options,'optimizationMethod')
    error 'Set fields varbose, parallel and optimizationMethod in the options struct'
  end
  
  withNaNs = false; %any(isnan(signal(:)));
  
  nClasses = numel(unique(allClassLabels));
  
  if nClasses>2
    %csssp.cspinst = cspn(channels, nClasses, model.CSPModel);
    error 'CSSSP has not been implemented for more than two classes'
  end
  
  if ~isfield(model,'SpectFltrLength') || ~isfield(model,'CCModel') %~isfield(model,'CSPModel') || 
    error 'model must have fields SpectFltrLength and CCModel'
  end
  
  [r,c] = size(allClassLabels);
  if r>1 && c>1
    error 'allTrainingClassLabels must be a vector';
  end
  
  [channels,T, nTrials] = size(signal);
  
  if numel(allClassLabels)~=nTrials
    error('Number of trials and class labels must match, but now there is %d trials and %d class labels',
    nTrials, numel(allClassLabels));
  end
    
  backSpaces=char([8,8,8,8,8,8,8]);
  controlFactors = 0:0.1:2;
  numSelectedFilters = min(numSelectedFilters*2, channels);
  if options.verbose
    fprintf(stdout,'CSSSP Validating controlFactor 1.23...');
  end
  
  optimizationMethods={'fminunc', 'nonlin_min', 'mdsmax', 'fminsearch', 'SimAnnealing','HillClimbing'};
  found=false;
  for t=1:numel(optimizationMethods)
    if isequal(options.optimizationMethod, optimizationMethods{t})
      options.optimizationMethod = t;
      found=true;
      break;
    end
  end
  
  if ~found
    error('Unknown or unsupported method : %s',options.optimizationMethod);
  end
  
  if nproc('current')==1 || ~options.parallel % Runs sequentially
    accuracy = zeros(size(controlFactors));
    if isOctave
      for k=1:numel(controlFactors)
        if options.verbose
          fprintf(stdout,'%s%4.2f...',backSpaces,controlFactors(k));
          fflush(stdout);
        end
     
        accuracy(k) = validateControlFactorInOctave(controlFactors(k), model, 
          nClasses, signal, allClassLabels, numSelectedFilters, 
          options, withNaNs);
      end % for
    else
      for k=1:numel(controlFactors)
        if options.verbose
          fprintf(stdout,'%s%4.2f...',backSpaces,controlFactors(k));
          fflush(stdout);
        end  
        accuracy(k) = validateControlFactor(controlFactors(k), model, 
          nClasses, signal, allClassLabels, numSelectedFilters, 
          options, withNaNs);
      end  
    end % for
  else  % Runs in parallel
      % Elapsed time is 2493.78 seconds for ChunksPerProc=fix(numel(controlFactors)/nproc('current'))
      % Elapsed time is 2455.17 seconds for ChunksPerProc=100
      % Elapsed time is 2475.34 seconds for ChunksPerProc=50
      if  isOctave
        accuracy = pararrayfun(nproc('current'), @(c) validateControlFactorInOctave(c, model, 
        nClasses, signal, allClassLabels, numSelectedFilters, 
        options, withNaNs), controlFactors, 
        'ChunksPerProc', 100, 'VerboseLevel', options.verbose+1);
      else
        accuracy = pararrayfun(nproc('current'), @(c) validateControlFactor(c, model, 
        nClasses, signal, allClassLabels, numSelectedFilters, 
        options, withNaNs), controlFactors,
        'ChunksPerProc', 100, 'VerboseLevel', options.verbose+1);
      end
  end
  if false %verbose
    disp ' '
    figure
    plot(controlFactors, accuracy)
    xlabel('Controlling factor')
    ylabel('Accuracy')
    if ~isfield(model, 'problemName')
      model.problemName = 'Unnamed problem';
    end
    title(sprintf('CSSSP demo run on %s with %d factors in the spectral filter',
      model.problemName, model.SpectFltrLength));
    disp(sprintf('Max accuracy = %f',max(accuracy)));
    disp(sprintf('Min accuracy = %f',min(accuracy)));
    m=mean(accuracy);
    s=std(accuracy);
    disp(sprintf('Normal range : %f±%f or (%f,%f)',m,s,m-s,m+s));
    fflush(stdout);
  end
   
  [v,ix] = max(accuracy);
  csssp.controlFactor = controlFactors(ix);
  if options.verbose
    disp(sprintf('Selected control factor is %f at %d', controlFactors(ix),
    ix));
  end
  
  % optimize optimizedFunc once for this controlFactor on all training data,
  % then solve the corresponding binary CSP
  csssp.cspinst = csp2(channels);
  if isOctave
    %if withNaNs
    %  preCalculatedSigmaTau = calcTrainingItemsWithNaNsOctave(signal, allClassLabels, 
    %  nClasses, model.SpectFltrLength);
    %else  
      preCalculatedSigmaTau = nativeCalcSigmaTauForCSSSP(signal, allClassLabels, model.SpectFltrLength);
    %end    
  %elseif withNaNs
  %  preCalculatedSigmaTau = calcTrainingItemsWithNaNs(signal, allClassLabels, nClasses, 
  %    model.SpectFltrLength);
  else  
    preCalculatedSigmaTau = calcTrainingItems(signal, allClassLabels, nClasses, 
      model.SpectFltrLength);
  end
  
  OPTIONS.TolFun = 0.001;
  bTail = maximizeMyFunction(preCalculatedSigmaTau, numSelectedFilters, 
  csssp.controlFactor, zeros(min(model.SpectFltrLength-1,T-1), 1), options);
  csssp.b = [1; bTail];
  sigmaPrime = calcSigmaPrime(bTail, preCalculatedSigmaTau);
  csssp.cspinst.selectedFilters = numSelectedFilters;
  csssp.cspinst.W = solveBinaryCSP(sigmaPrime(:,:,1), sigmaPrime(:,:,2),
    numSelectedFilters);
end
%--------------------------------
function bTail = maximizeMyFunction(preCalculatedSigmaTau, numSelectedFilters, 
  controlFactor, initialPoint, options)
  
  % use any of optimization methods based on the given options
  switch options.optimizationMethod
    case 1 %'fminunc'
      OPTIONS.TolFun = 0.001;
      % optimizedFunc itself represents a maximization problem, so must be negated here
      [bTail, objf, INFO, output] = fminunc(@(bTail) -optimizedFunc(bTail, preCalculatedSigmaTau, 
        numSelectedFilters, controlFactor), initialPoint, OPTIONS);
      objf = -objf;
      numCalls = output.funcCount;
      if options.verbose
        switch(INFO)
          case 1
            description = 'Converged to a solution point.  Relative gradient error is less than specified by TolFun';
          case 2
            description = 'Last relative step size was less than ''TolX''';
          case 3
            description = 'Last relative change in function value was less than ''TolFun''';
          case 0
            description = 'Iteration limit exceeded';
          case -1
            description = 'Algorithm terminated by ''OutputFcn''';
          case -3
            description = 'The trust region radius became excessively small';
          otherwise
            description = sprintf('Unknown status returned : %d',INFO);
        end
      end
    
    case 2 %'nonlin_min'
      % optimizedFunc itself represents a maximization problem, so must be negated here
      [bTail, objf, cvg, outp] = nonlin_min(@(bTail) -optimizedFunc(bTail, preCalculatedSigmaTau, 
        numSelectedFilters, controlFactor), initialPoint);
      objf = -objf; % It was maximized
      numCalls = outp.nobjf;
      if options.verbose
        switch (cvg)
          case 0
            description = 'Maximum number of iterations exceeded';
          case 1
            description = 'Success without further specification of criteria';
          case 2
            description = 'Parameter change less than specified precision in two consecutive iterations';
          case 3
            description = 'Improvement in objective function less than specified';
          case -1
            description = 'Algorithm aborted by a user function';
          case -4
            description = 'Algorithm got stuck';
          otherwise
            if cvg>0
              description=sprintf('Another reason to succeed (%d)',cvg);
            else
              description=sprintf('Another reason to fail (%d)',cvg);
            end
        end % switch
      end % if verbose
    
    case 3 %'mdsmax'
      [bTail, objf, numCalls] = mdsmax(@(bTail) optimizedFunc(bTail, preCalculatedSigmaTau, 
        numSelectedFilters, controlFactor), initialPoint, 
        [1e-3, inf, inf, 0, false]);
      description = 'Default constraints for convergence';
      
    case 4 %'fminsearch'
      OPTIONS.TolFun = 0.001;
      % NOTE : I have changed code of fminsearch in octave m-files path so it 
      % returns number of objective function calls
      [bTail, objf, numCalls] = fminsearch(@(bTail) -optimizedFunc(bTail, preCalculatedSigmaTau, 
        numSelectedFilters, controlFactor), initialPoint, OPTIONS);
      objf = -objf;
      description = 'Default constraints for convergence';
    
    case 5 %'SimAnnealing'
      OPTIONS.numIterations = 10000;
      OPTIONS.verbose = false; %options.verbose;
      [bTail, objf, numCalls] = optSimAnnealingND(@(bTail) optimizedFunc(bTail, 
        preCalculatedSigmaTau, numSelectedFilters, controlFactor), 
        repmat([-1,1],[numel(initialPoint),1]), initialPoint, 0.002, OPTIONS);
      description = 'Maximum number of iterations reached';
    
    case 6 %'HillClimbing'
      OPTIONS.successorsStrategy = 'steepest';
      OPTIONS.sidewaysLimit=20;
      OPTIONS.restartsLimit=5;
      OPTIONS.verbose = options.verbose;
      [bTail, objf, numCalls] = optHillClimbing(@(bTail) optimizedFunc(bTail, 
        preCalculatedSigmaTau, numSelectedFilters, controlFactor),
        repmat([-1,1],[numel(initialPoint),1]), 0.005, OPTIONS);
      description = 'Maximum number of iterations reached';
    end % switch optimizationMethod
    if options.verbose
      disp(sprintf('Objective function calls and value : %d , %f',numCalls, objf));
      %disp(description);
      fflush(stdout);
    end
end  
%--------------------------------
function acc = validateControlFactorInOctave(controlFactor, model, 
  nClasses, signal, allClassLabels, numSelectedFilters, 
  options, withNaNs)
  
  if nargin~=8
    error 'Incorrect number of arguments'
  end
    
  % Find vector b for which sum of variances of spatially mapped frequency filterd
  % signals becomes maximum.
  [channels, T, nTrials] = size(signal);
  validationRepeats = 4;
   
  testSetSize = fix(nTrials/validationRepeats);
  sumAccuracy = 0;
  
  lastTau = min(model.SpectFltrLength-1, T-1);
  
  for repeat=1:validationRepeats
    % divide given data into training and validation subsets
    testingIndex = (repeat-1)*testSetSize+1:repeat*testSetSize;
    trainingIndex = [1:(repeat-1)*testSetSize, repeat*testSetSize+1:nTrials];
    % correlations between the signal and the by-τ-timepoints-delayed signal
      %if withNaNs
      %  preCalculatedSigmaTau = calcTrainingItemsWithNaNsOctave(signal(:,:,trainingIndex),
      %    allClassLabels(trainingIndex), nClasses, model.SpectFltrLength);
      %else
      if isOctave
        preCalculatedSigmaTau = nativeCalcSigmaTauForCSSSP(signal(:,:,trainingIndex),
          allClassLabels(trainingIndex), model.SpectFltrLength);
      else
        preCalculatedSigmaTau = calcTrainingItems(signal(:,:,trainingIndex),
          allClassLabels(trainingIndex),nClasses, model.SpectFltrLength);
      end
      %end  
   
    
    % Optimizing
   
    bTail = maximizeMyFunction(preCalculatedSigmaTau, numSelectedFilters, 
      controlFactor, zeros(lastTau, 1), options);
  
    % Solve the corresponding CSP
    sigmaPrime = calcSigmaPrime(bTail, preCalculatedSigmaTau);
    W = solveBinaryCSP(sigmaPrime(:,:,1), sigmaPrime(:,:,2), numSelectedFilters);
    clear preCalculatedSigmaTau
    if (~isempty(W))
    % validate this CSP on the validation data
      cspinst = csp2(channels);
      cspinst.W = W;
      cspinst.selectedFilters = numSelectedFilters;
    
    % Auto correlation of signal of each trial
    % process this raw signal by a frequency filter implemented by b=[1,bTail]

      s2 = calcFilteredX2SignalsForCSSSP([1;bTail], signal, withNaNs);
      
      %D = generatePatternsFromCSP(cspinst, @(idx) simpleTrialGenerator(idx,
      %  trainingIndex, s2, allClassLabels), numel(trainingIndex), 'training');
      D = generatePatternsFromCSP(cspinst, s2(:,:,trainingIndex), allClassLabels(trainingIndex));
      
      CC = train_sc(D, allClassLabels(trainingIndex), model.CCModel);
    
      % use testCSP for this CSP instance on the test subset    
      % We don't need kappa and confusion matrix here
      R = testCSP(cspinst, s2(:,:,testingIndex), CC, model.CCModel);
      sumAccuracy += nnz(allClassLabels(testingIndex)==R.classlabel(:));
    end % if W is not empty
  end % for repeat
  
  acc = sumAccuracy/(validationRepeats*testSetSize);
end

%--------------------------------
function preCalculatedSigmaTau = calcTrainingItems(signal, classLabel, nClasses, SpectFltrLength)
% preCalculatedSigmaTau : A matrix with channels times channels times 
% SpectFltrLength times nClasses elements, containing correlations between the 
% signal and the by-τ-timepoints-delayed signal
  [channels, T, nTrials] = size(signal);  
  lastTau = min(SpectFltrLength-1, T-1);
  
  preCalculatedSigmaTau = zeros(channels, channels, lastTau+1, nClasses);
  for t=1:nTrials
    for tau=0:lastTau
      %s_tau = [zeros(channels, tau), s(:,1:end-tau)]';
      sstau = signal(:, tau+1:end, t)*signal(:, 1:end-tau, t)';  %s*s_tau';
      preCalculatedSigmaTau(:,:,tau+1,classLabel(t)) = preCalculatedSigmaTau(:,:,tau+1,classLabel(t)) + sstau + sstau';
    end % for tau
  end % for each trial
  % Averaging
  for y=1:nClasses
    preCalculatedSigmaTau(:,:,:,y) = preCalculatedSigmaTau(:,:,:,y)/sum(classLabel==y);
  end
  
  %if any(isnan(preCalculatedSigmaTau(:)))
  %  error 'Result contains NaNs'
  %end
end
%--------------------------
%function preCalculatedSigmaTau = calcTrainingItemsWithNaNsOctave(signal, classLabel, nClasses, SpectFltrLength)
% preCalculatedSigmaTau : A matrix with channels times channels times 
% SpectFltrLength times nClasses elements, containing correlations between the 
% signal and the by-τ-timepoints-delayed signal
%  [channels, T, nTrials] = size(signal);  
%  lastTau = min(SpectFltrLength-1, T-1);
%  preCalculatedSigmaTau = zeros(channels, channels, lastTau+1, nClasses);
%  factors = zeros(channels, channels, lastTau+1, nClasses);
%  for t=1:nTrials
%    for tau=0:lastTau
%      %s_tau = [zeros(channels, tau), s(:,1:end-tau)];
%      sstau = signal(:, tau+1:end, t)*signal(:, 1:end-tau, t)';  %s*s_tau';
%      idx = isnan(sstau);
%      sstau(idx) = 0;
%      factors(:,:,tau+1,classLabel(t)) += (~idx) + (~idx');
%      preCalculatedSigmaTau(:,:,tau+1,classLabel(t)) += sstau + sstau';
%    end % for tau
%  end % for each trial
  % Averaging on non-NaN elements
%  isPositive = factors~=0;
%  preCalculatedSigmaTau(isPositive) ./= factors(isPositive);
%end

%function preCalculatedSigmaTau = calcTrainingItemsWithNaNs(signal, classLabel, nClasses, SpectFltrLength)
% preCalculatedSigmaTau : A matrix with channels times channels times 
% SpectFltrLength times nClasses elements, containing correlations between the 
% signal and the by-τ-timepoints-delayed signal
    
%  [channels, T, nTrials] = size(signal);
%  lastTau = min(SpectFltrLength-1, T-1);
%  preCalculatedSigmaTau = zeros(channels, channels, SpectFltrLength, nClasses);
%  factors = zeros(channels, channels, SpectFltrLength, nClasses);
%  for t=1:nTrials
%    for tau=0:SpectFltrLength-1
%      %s_tau = [zeros(channels, tau), s(:,1:end-tau)];
%      sstau = signal(:, tau+1:end, t)*signal(:, 1:end-tau, t)';  %s*s_tau';
%      idx = isnan(sstau);
%      sstau(idx) = 0;
%      factors(:,:,tau+1,classLabel(t)) = factors(:,:,tau+1,classLabel(t)) + (~idx) + (~idx');
%      preCalculatedSigmaTau(:,:,tau+1,classLabel(t)) = preCalculatedSigmaTau(:,:,tau+1,classLabel(t)) + sstau + sstau';
%    end % for tau
%  end % for each trial
%  % Averaging on non-NaN elements
%  isPositive = factors~=0;
%  preCalculatedSigmaTau(isPositive) = preCalculatedSigmaTau(isPositive)./factors(isPositive);
%end
%--------------------------
function fcn = optimizedFunc(bTail, preCalculatedSigmaTau, numSelectedFilters,
  controlFactor)
% Function to be maximized for spectral filter weigths b = [1; bTail]
% numSelectedFilters : Number of selected spatail filters in a CSP
% preCalculatedSigmaTau : A matrix with channels times channels times numel(b) 
% times nClasses elements, containing correlations between the signal and the by- 
% τ-timepoints-delayed signal
  %disp(bTail);
  sigmaPrime = calcSigmaPrime(bTail, preCalculatedSigmaTau);
  % now sigmaPrime contains some covariance matrices what CSP can act upon.
  % Calculate W then return W(:,1)'*sigmaPrime(:,:,1)*W(:,1)-C||b||/T when we have two 
  % classes.
  W = solveBinaryCSP(sigmaPrime(:,:,1), sigmaPrime(:,:,2), numSelectedFilters,
    false);
  if isempty(W)
    fcn = 0;
  else     
    b = [1;bTail];
    fcn = W(:,1)'*sigmaPrime(:,:,1)*W(:,1) - controlFactor*sum(abs(b))/numel(b);
  end
    
end
%----------------------
function sigmaPrime = calcSigmaPrime(bTail, preCalculatedSigmaTau)
  b = [1;bTail];
  channels = size(preCalculatedSigmaTau,1);
  nClasses = size(preCalculatedSigmaTau,4);
  sigmaPrime = zeros(channels,channels,nClasses);
  T = numel(b);
  bb = zeros(1,1,T);
  B = b*b';
  for tau=0:T-1
    bb(tau+1) = sum(diag(B,tau));
  end
  if isOctave
    % This part uses broadcasting
    for k=1:nClasses
      sigmaPrime(:,:,k) = sum(bb.*preCalculatedSigmaTau(:,:,:,k), 3);
    end
  else 
    for k=1:nClasses
      sigmaPrime(:,:,k) = sum(repmat(bb,[channels,channels]).*preCalculatedSigmaTau(:,:,:,k), 3);
    end
  end
  
  % The metod above is better for a bigger number of channels, but maybe this method
  % below was better for a few channels.
  %for k=1:nClasses
  %  for tau=0:T-1
  %    sigmaPrime(:,:,k) = sigmaPrime(:,:,k) + bb(tau+1)*preCalculatedSigmaTau(:,:,tau+1,k);
  %  end 
  %end
end
