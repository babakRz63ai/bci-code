function D = generatePatternsFromCSSSP(csssp, signal, extra)
% D = generatePatternsFromCSSSP(csssp, signal, extra=[])
%
% csssp : A trained CSSSP instance
% signal : A 3D array of all signals
% extra : Extra options. It can contain withNaNs
    
  
  if nargin<3 || ~isfield(extra,'withNaNs')
    extra.withNaNs = false;
  end
  
  D = generatePatternsFromCSP(csssp.cspinst, calcFilteredX2SignalsForCSSSP(csssp.b, signal, extra.withNaNs)); 
end
