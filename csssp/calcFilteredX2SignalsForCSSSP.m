function s2 = calcFilteredX2SignalsForCSSSP(b, signals, withNaNs)
  % s2 = calcFilteredX2SignalsForCSSSP(b, signals, withNaNs)
  %
  % Calculates autocorrelation of signals of trials from raw signals and using
  % a set of weights composing a CSSSP frequency fliter.
  %
  % b : vector of weights
  % signals : A 3D array with size channels*T*nTrials
  % withNaNs : Should it consider NaN values
  %
  % retuns : An array of size channels times channels times number of trials
  
  
  if size(signals,2)<numel(b)
    error('Insufficient length of an input signal; It is %d while must be at least %d',
    size(signals,2), numel(b))
  end
  
  channels = size(signals,1);
  
  if isOctave
    if withNaNs
      s2 = calcInOctaveWithNaNs(channels, b, signals);
    else
      s2 = nativeFilteredX2SignalForCSSSP(b, signals);
    end
elseif withNaNs
    s2 = calcInMatlabWithNaNs(channels, b, signals);
  else
    s2 = calcInMatlab(channels, b, signals);
  end
end
%----------------------
function s2 = calcInOctaveWithNaNs(channels, b, signals)
  nTrials = size(signals,3);
  s2 = zeros(channels, channels, nTrials);
  for t=1:nTrials
    s = signals(:,:,t);
    for tau=1:numel(b)-1
      s += b(tau+1)*[zeros(channels, tau), signals(:,1:end-tau,t)];
    end
    s2(:,:,t) = covarianceWithNaN(s);
  end
end
%----------------------
% It is deprecated. Use nativeFilteredX2SignalForCSSSP instead
%function s2 = calcInOctave(channels, b, signals)
%  s2 = zeros(channels, channels, size(signals,3));
%  sqrt_T = sqrt(columns(signals));
%  for t=1:size(signals,3)
%    s = signals(:,:,t)/sqrt_T;
%    for tau=1:numel(b)-1
      %s += b(tau+1)*[zeros(channels, tau), signals(:,1:end-tau,t)]/sqrt_T;
%      s(:,tau+1:end) += b(tau+1)*signals(:,1:end-tau,t)/sqrt_T;
%    end
    % The averaging is neccessary because covarianceWithNaN averages too
%    s2(:,:,t) = s*s';
%  end
%end
%----------------------
function s2 = calcInMatlabWithNaNs(channels, b, signals)
  s2 = zeros(channels, channels, size(signals,3));
  for t=1:size(signals,3)
    s = signals(:,:,t);
    for tau=1:numel(b)-1
      s = s + b(tau+1)*[zeros(channels, tau), signals(:,1:end-tau,t)];
    end
    s2(:,:,t) = covarianceWithNaN(s);
  end
end

%----------------------
function s2 = calcInMatlab(channels, b, signals)
  s2 = zeros(channels, channels, size(signals,3));
  for t=1:size(signals,3)
    s = signals(:,:,t);
    for tau=1:numel(b)-1
      s = s + b(tau+1)*[zeros(channels, tau), signals(:,1:end-tau,t)];
    end
    % The averaging is neccessary because covarianceWithNaN averages too
    s2(:,:,t) = s*s'/size(signals,2);
  end
end